import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SocketService } from './service/socket.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'pantul-client';

    constructor(
        private socket: SocketService,
        private titleService: Title,
    ) {
    }
    
    ngOnInit() {
        this.titleService.setTitle('MyGame');

        this.socket.on('disconnect', () => {
            alert('Disconnected! Please refresh the browser.');
        });

        window.onbeforeunload = () => {
            this.socket.close();
        };
    }
}
