import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppComponent } from './app.component';
import { LoginComponent } from './ui/login/login.component';
import { FormsModule } from '@angular/forms';
import { LobbyComponent } from './ui/lobby/lobby.component';
import { MainComponent } from './ui/main/main.component';
import { RoomComponent } from './ui/room/room.component';
import { RoomTileComponent } from './ui/room-tile/room-tile.component';
import { GameComponent } from './ui/game/game.component';
import { PlayerViewComponent } from './ui/game/player-view/player-view.component';
import { MenuControlComponent } from './ui/menu-control/menu-control.component';
import { GameRuleInputComponent } from './ui/room/game-rule-input/game-rule-input.component';
import { IntegerInputComponent } from './ui/room/game-rule-input/integer-input/integer-input.component';
import { VarDirective } from './ui/ngVarDirective';
import { ChoiceInputComponent } from './ui/room/game-rule-input/choice-input/choice-input.component';
import { SideMenuComponent } from './ui/side-menu/side-menu.component';
import { IconButtonComponent } from './ui/element/icon-button/icon-button.component';
import { ToggleTextComponent } from './ui/element/toggle-text/toggle-text.component';
import { ChatComponent } from './ui/chat/chat.component';
import { CreditsComponent } from './ui/credits/credits.component';
import { CreateRoomComponent } from './ui/create-room/create-room.component';
import { PreloaderComponent } from './ui/preloader/preloader.component';
import { GameResultComponent } from './ui/room/game-result/game-result.component';
import { RoomSettingsComponent } from './ui/room/room-settings/room-settings.component';
import { CoinsComponent } from './ui/element/coins/coins.component';
import { RoomItemComponent } from './ui/lobby/room-item/room-item.component';
import { LobbyBtnComponent } from './ui/lobby/lobby-btn/lobby-btn.component';
import { LeaderboardComponent } from './ui/leaderboard/leaderboard.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    LobbyComponent,
    RoomComponent,
    RoomTileComponent,
    GameComponent,
    PlayerViewComponent,
    MenuControlComponent,
    GameRuleInputComponent,
    IntegerInputComponent,
    VarDirective,
    ChoiceInputComponent,
    SideMenuComponent,
    IconButtonComponent,
    ToggleTextComponent,
    ChatComponent,
    CreditsComponent,
    CreateRoomComponent,
    PreloaderComponent,
    GameResultComponent,
    RoomSettingsComponent,
    CoinsComponent,
    RoomItemComponent,
    LobbyBtnComponent,
    LeaderboardComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
