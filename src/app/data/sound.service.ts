import { Injectable } from '@angular/core';
import { Howl } from 'howler';

@Injectable({
    providedIn: 'root'
})
export class SoundService {

    enabled = true;
    maps: Map<string, {howls: Howl[], variations: number}>[] = [];

    constructor() {
        let list: {name: string, variations?: number}[] = [
            { name: 'harddrop' },
            { name: 'clear' },
            { name: 'clearmany' },
            { name: 'clearb2b' },
            { name: 'immobile' },
            { name: 'tspin' },
            { name: 'tspinmini' },
            { name: 'rotate', variations: 2 },
            { name: 'rotkick' },
            { name: 'stuck' },
            { name: 'receiveone' },
            { name: 'receivetwo' },
            { name: 'receivemany' },
            { name: 'receivespike' },
            { name: 'die' },
            { name: 'combo', variations: 7 },
            { name: 'combotimerstart' },
            { name: 'hold' },
            { name: 'colorclear' },
            { name: 'perfectclear' },
        ];

        for (let channel = 0; channel < 2; channel++) {
            let map = new Map<string, {howls: Howl[], variations: number}>();
            for (let item of list) {
                let entry: any = {
                    howls: [],
                    variations: item.variations? item.variations : 1,
                };
                
                for (let i = 0; i < entry.variations; i++) {
                    entry.howls.push(new Howl({
                        src: 'assets/snd/' + [item.name] + (i+1) + '.mp3',
                    }));
                }
    
                map.set(item.name, entry);
            }
            this.maps.push(map);
        }

        this.setVolume(1, 0.1);
    }

    play(name: string, channel: number, variation?: number) {
        if (!this.enabled) return;

        let entry = this.maps[channel].get(name);

        if (variation == null) {
            variation = Math.floor(Math.random() * entry.variations);
        }

        entry.howls[variation].play();
    }

    toggle() {
        this.enabled = !this.enabled;
    }

    setVolume(channel: number, volume: number) {
        this.maps[channel].forEach((v, k) => {
            v.howls.forEach(h => h.volume(volume));
        });
    }
}
