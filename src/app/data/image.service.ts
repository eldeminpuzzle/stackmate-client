import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ImageService {

    public map = new Map<string, string>();
    public imgMap = new Map<string, HTMLImageElement>();

    toLoadcount = 0;
    loadedCount = 0;
    onLoad = () => {};

    public load() {
        for (let name of ['tiles', 'btn_left', 'btn_right']) {
            this.toDataURL('assets/img/default/' + name + '.png', (data: string) => { 
                this.map.set(name, data);
                
                let img = new Image();
                img.src = data;
                this.imgMap.set(name, img);

                this.loadedCount++;
                if (this.loadedCount == this.toLoadcount) {
                    this.onLoad();
                }
            });
        }
    }
    
    public setOnLoad(f: () => any) {
        this.onLoad = f;
    }

    public get(key: string) {
        return this.map.get(key);
    }

    public getImg(key: string) {
        return this.imgMap.get(key);
    }

    toDataURL(url: string, callback: any) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', url);
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var fr = new FileReader();

            fr.onload = function () {
                callback(this.result);
            };

            fr.readAsDataURL(xhr.response); // async call
        };

        xhr.send();

        this.toLoadcount++;
    }
}
