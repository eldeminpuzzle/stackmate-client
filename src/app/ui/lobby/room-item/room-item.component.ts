import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { RoomInfo } from 'src/app/shared/intf';

@Component({
    selector: 'app-room-item',
    templateUrl: './room-item.component.html',
    styleUrls: ['./room-item.component.scss']
})
export class RoomItemComponent implements OnInit {

    @Output() join = new EventEmitter();
    @Output() spectate = new EventEmitter();

    @Input() roomInfo: RoomInfo;

    playerCount = 0;
    slotCount: number = null;
    spectatorCount = 0;

    constructor() { }

    ngOnInit() {
        this.playerCount = this.roomInfo.players.filter(p => p != null).length;
        this.slotCount = this.roomInfo.autoJoin ? null : this.roomInfo.players.length;
        this.spectatorCount = this.roomInfo.spectators.length;
    }

    onSpectateClicked(e: MouseEvent) {
        e.stopPropagation();
        this.spectate.emit();
    }
}
