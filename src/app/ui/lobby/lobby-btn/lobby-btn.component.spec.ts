import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyBtnComponent } from './lobby-btn.component';

describe('LobbyBtnComponent', () => {
  let component: LobbyBtnComponent;
  let fixture: ComponentFixture<LobbyBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
