import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-lobby-btn',
    templateUrl: './lobby-btn.component.html',
    styleUrls: ['./lobby-btn.component.scss']
})
export class LobbyBtnComponent implements OnInit {

    @Input() text: string;
    @Input() highlight: boolean;

    constructor() { }

    ngOnInit() {
    }

}
