import { ChangeDetectorRef, Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';
import { LobbyService } from 'src/app/endpoint/lobby-service';
import { Session } from 'src/app/endpoint/session';
import { SocketService } from 'src/app/service/socket.service';
import { PresetInfo, RoomInfo } from 'src/app/shared/intf';
import { Util } from 'src/app/shared/util';
import { CreateRoomUiCmd } from '../create-room/create-room.component';

interface RoomCategory {
    name: string;
    rooms: RoomInfo[];
}

@Component({
    selector: 'app-lobby',
    templateUrl: './lobby.component.html',
    styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

    @Output() openLeaderboard = new EventEmitter();

    @Input() presetInfos: PresetInfo[];

    createRoomForm: any = {};
    roomCategories: RoomCategory[];
    // rooms: RoomInfo[] = [];

    hint: string;

    constructor(
        private lobbyService: LobbyService,
        private session: Session,
        private ngZone: NgZone,
        private socket: SocketService,
    ) { }

    ngOnInit() {
        this.showRandomHint();
        Util.autoBind(this, this.socket);
    }

    enterLobby() {
        if (!this.createRoomForm.name) {
            this.createRoomForm.name = this.session.account.name + '\'s Room'
        }
        this.refreshRoomList();
        this.showRandomHint();
    }

    onSubmitCreateRoom(cmd: CreateRoomUiCmd) {
        this.lobbyService.createRoom(this.createRoomForm.name, cmd.custom, cmd.presetId);
    }

    refreshRoomList() {
        this.lobbyService.getRoomInfos();
    }

    on_updateRoomInfos(rooms: RoomInfo[]) {
        this.ngZone.run(() => {
            this.roomCategories = [{
                name: 'Server Rooms',
                rooms: rooms.filter(r => r.serverRoom),
            }, {
                name: 'Player Rooms',
                rooms: rooms.filter(r => !r.serverRoom),
            }];
        });
    }

    onJoinRoomClicked(room: RoomInfo, spectateMode: boolean) {
        this.lobbyService.joinRoom(room, spectateMode);
    }

    showRandomHint() {
        const hints = [
            'You can change your local control settings (keyboard binding and tuning) using the keyboard icon on the top right.',
            'Players can move to another slots or become spectators by clicking on one of the slots in the Room Panel on the top left.',
        ];
        this.hint = hints[Math.floor(Math.random() * hints.length)];
    }
}
