import { PlayerEngine } from "src/app/shared/playerEngine";

export class GravityHandler {

    // game
    nextGravityUpdate: number;
    lockDelayLeft: number;
    lockDelayLimitLeft: number;

    gravityTimeout: any;

    constructor(
        private playerEngine: PlayerEngine,
        private changeCallback: any,
    ) {
        if (this.playerEngine.isLocal) {
            this.playerEngine.listenersOnTurnStarted.add( this.startGravity.bind(this));
            this.playerEngine.listenersOnPieceMoved.add(this.onPieceMoved.bind(this));
            this.playerEngine.listenersOnTurnEnded.add( this.stopGravity.bind(this));
            this.playerEngine.listenersOnLockOut.add( this.stopGravity.bind(this));
        }
    }

    onDestroy() {
        this.stopGravity();
    }

    private onPieceMoved() {
        if (this.playerEngine.sideRule.gravity == -1) {
            this.playerEngine.sonicDrop();
        }
    }

    private startGravity() {
        console.log('start g2');
        if (this.playerEngine.sideRule.gravity == -1) {
            setTimeout(this.playerEngine.sonicDrop.bind(this));
        } else if (this.playerEngine.sideRule.gravity > 0) {
            this.nextGravityUpdate = Date.now();
            this.gravityTimeout = setTimeout(this.tickGravity.bind(this), 1000 / this.playerEngine.sideRule.gravity);
        }
    }

    private tickGravity() {
        if (this.playerEngine.attemptMove(1, 0, 0)) {
            this.changeCallback();
        }
        this.nextGravityUpdate += 1000 / this.playerEngine.sideRule.gravity;
        this.gravityTimeout = setTimeout(this.tickGravity.bind(this), this.nextGravityUpdate - Date.now());
    }

    private stopGravity() {
        if (this.gravityTimeout != null) {
            clearTimeout(this.gravityTimeout);
        }
    }
}
