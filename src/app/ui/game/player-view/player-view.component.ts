import { Component, ElementRef, Input, NgZone, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ImageService } from 'src/app/data/image.service';
import { Piece } from 'src/app/shared/gameData';
import { ComboMode, SpinBonusMode } from 'src/app/shared/gameRule';
import { PlayerInfo } from 'src/app/shared/intf';
import { MatUtil } from 'src/app/shared/matUtil';
import { MoveInfo, MoveResult, PlayerEngine } from 'src/app/shared/playerEngine';
import { TestConfig } from 'src/app/testConfig';
import { PIECES } from 'src/app/shared/pieceGen';
import { SocketService } from 'src/app/service/socket.service';
import { Control } from '../../control';
import { SoundService } from 'src/app/data/sound.service';
import { PlayerSound } from './playerSound';
import { SubMoveTimer } from './subMoveTimer';

interface Rect {x: number, y: number, scale: number, opacity: number, width?: number, height?: number };

@Component({
    selector: 'app-player-view',
    templateUrl: './player-view.component.html',
    styleUrls: ['./player-view.component.scss']
})
export class PlayerViewComponent implements OnInit {

    @ViewChild('uiContainer', {static: true}) uiContainer:ElementRef<HTMLDivElement>;

    mockTileSize = 40 * (TestConfig.renderSmall ? 0.5 : 1); // used to set layout sizes - but actual tile size is dynamically determined from GameRule
    pieceSlotWidth = this.mockTileSize * 5;
    pieceSlotHeight = this.mockTileSize * 2.5;
    boardWidth = this.mockTileSize * 10;
    boardHeight = this.mockTileSize * 20;
    garbageIndicatorWidth = this.mockTileSize * 0.5;

    sourceTileSize = 30;
    sourceTileSpacing = 1;

    // ui helper
    uiHelperPrevewNrs: any[] = [0];
    ComboMode = ComboMode;

    @Input("playerEngine") playerEngine: PlayerEngine;
    @Input("playerInfo") playerInfo: PlayerInfo;

    @ViewChild("uiGarbageIndicator", {static: true}) uiGarbageIndicator: ElementRef<HTMLCanvasElement>;
    @ViewChild("uiBoard", {static: true}) uiBoard: ElementRef<HTMLCanvasElement>;
    g: CanvasRenderingContext2D;

    @ViewChild("uiHold", {static: true}) uiHold: ElementRef<HTMLCanvasElement>;
    @ViewChildren("uiPreview") uiPreview: QueryList<ElementRef<HTMLCanvasElement>>;
    @ViewChild("uiComboTimerCanvas", {static: false}) uiComboTimerCanvas: ElementRef<HTMLCanvasElement>;

    tilePositioning: { tileSize: number; offsetX: number; offsetY: number; offsetYGrid: number; offsetWidth: number; offsetHeight: number; };

    pos: Rect = { x: 0, y: 0, scale: 0.01, opacity: 1 };
    prevPos: Rect = { x: 0, y: 0, scale: 0.01, opacity: 1  };
    targetPos: Rect = { x: 0, y: 0, scale: 1, opacity: 1 };
    prevPosSince = Date.now();
    posAnimMs = 250;
    posAnimDone = false;
    garbageAnimOffsetY = 0;
    lineClearAnimOffsetY: number[] = [];

    // ui fields
    overlayText: string = null;
    clockDisplay: string = '0:00';
    lastMoveResult: MoveResult;
    spinDisplay: string;
    clearDisplay: string;
    comboDisplay: string;
    comboTimerDisplay: string;
    b2bDisplay: string;
    comboTimerSeconds: number;
    comboTimerSubSecond: number;

    clearNames = ['', 'Single', 'Double', 'Triple', 'Quadruple', 'Impossible'];
    comboTimerColors = ['#f00', '#ff0', '#0f0', '#0ff', '#00f', '#f0f'];

    constructor(
        private img: ImageService,
        private socket: SocketService,
        private control: Control,
        private soundService: SoundService,
        private ngZone: NgZone,
    ) {}

    ngOnInit() {
        this.g = this.uiBoard.nativeElement.getContext('2d');

        // init animation data
        for (let i = 0; i < this.playerEngine.sideRule.height; i++) {
            this.lineClearAnimOffsetY.push(0);
        }

        this.playerEngine.listenersOnCommandExecuted.add(this.render.bind(this));
        this.playerEngine.listenersOnLockDown.add(this.onLockDown.bind(this));
        this.playerEngine.listenersOnTimeChanged.add(this.updateClockDisplay.bind(this));
        this.playerEngine.listenersOnLockOut.add(() => { 
            if (this.playerEngine.isLocal) {
                this.socket.send('die');
            }
            
            if (!this.playerEngine.isLocal) {
                this.setPosition({
                    x: this.pos.x,
                    y: this.pos.y - 100,
                    scale: this.pos.scale,
                    opacity: 0,
                }, true);
                this.posAnimMs = 2000;
            }
        });

        this.playerEngine.listenersOnGarbageEntered.add((numLines, isFixRate) => {
            // shift line clear animation
            for (let i = 0; i < this.lineClearAnimOffsetY.length - numLines; i++) {
                this.lineClearAnimOffsetY[i] = this.lineClearAnimOffsetY[i+numLines];
            }
            for (let i = this.lineClearAnimOffsetY.length - numLines; i < this.lineClearAnimOffsetY.length; i++) {
                this.lineClearAnimOffsetY[i] = 0;
            }

            // garbage entry animation
            if (!isFixRate) {
                this.garbageAnimOffsetY += numLines;
            }
        });

        // line clear animation TODO: if sideRule.lineClearAnimationSpeed
        this.playerEngine.listenersOnLinesCleared.add(clearInfo => {
            const lines = clearInfo.linesCleared;

            // shift line clear animations
            let count = 1;
            let lcCount = 1;
            for (let i = lines[lines.length - 1] - 1; i >= 0; i--) {
                if (count < lines.length && lines[lines.length - 1 - count] == i) {
                    count++;
                    lcCount++;
                } else {
                    this.lineClearAnimOffsetY[i+count] += this.lineClearAnimOffsetY[i];
                    this.lineClearAnimOffsetY[i] = 0;
                    if (lcCount > 0) {
                        this.lineClearAnimOffsetY[i+count] += lcCount;
                        lcCount = 0;
                    }
                }
            }

        });

        if (this.playerEngine.isLocal) {
            // this.control.bind(this, this.playerEngine);
            this.control.unpressKeys();
        }

        let sideRule = this.playerEngine.sideRule;
        // TODO: if too many players, only local?
        new PlayerSound(this.playerEngine, this.soundService, sideRule);

        if (this.playerEngine.isLocal) {
            new SubMoveTimer(this.playerEngine, this.socket);
        }

        let visibleHeight = sideRule.playHeight - 1;
        let tileSize = Math.min(this.boardWidth / sideRule.width, this.boardHeight / visibleHeight);
        this.tilePositioning = {
            tileSize,
            offsetX: (this.boardWidth - sideRule.width * tileSize) / 2,
            offsetY: (this.boardHeight - (visibleHeight * tileSize)) / 2,
            offsetYGrid: (this.boardHeight - (visibleHeight * tileSize)) / 2,
            offsetWidth: sideRule.width * tileSize,
            offsetHeight: visibleHeight * tileSize,
        };

        this.updateClockDisplay();
    }

    onLockDown(moveInfo: MoveInfo, moveResult: MoveResult) {
        if (this.playerEngine.isLocal) {
            this.socket.send('submitMove', moveInfo);
            // this.socket.send('debugData', this.playerEngine.sideData);
        }


        // update display move result
        this.lastMoveResult = moveResult;
        let r = this.playerEngine.sideRule;
        let m = this.lastMoveResult;
        let p = PIECES[m.pieceSize - 1][m.pieceId];
        this.spinDisplay = 
            (r.spinBonusMode == SpinBonusMode.NO_BONUS || m == null) ? ''
            : (r.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && m.isMiniTSpin)
                ? 'T-Spin Mini'

            : (r.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && m.isThreeCornersSpin)  
                ? p.name + '-Spin'

            : (r.spinBonusMode == SpinBonusMode.IMMOBILE && m.isImmobileSpin)
                ? p.name + '-Spin'

            : '';
            
        this.clearDisplay = 
            m == null ? ''
            : (r.perfectClearBonus > 0 && m.isPerfectClear) ? 'Perfect Clear!'
            : (r.colorClearBonus > 0 && m.isColorClear) ? 'Color Clear!'
            : '';
        
        this.comboDisplay =
            m == null ? ''
            : (r.comboMode != ComboMode.NONE && m.combo > 1) ? 'Combo x' + (m.combo-1)
            : '';

        this.b2bDisplay = 
            m == null ? ''
            : (m.backToBack != null && m.backToBack > 1) ? 'Back-to-Back x' + (m.backToBack-1)
            : '';
    }

    render() {
        let ct = Date.now();
        if (!this.posAnimDone) {
            this.ngZone.run(() => {
                let x = Math.min(1, (ct - this.prevPosSince) / this.posAnimMs);
                let p = Math.pow(1 - x, 5);
                let t = 1 - p;
                this.pos.x = this.prevPos.x * p + this.targetPos.x * t;
                this.pos.y = this.prevPos.y * p + this.targetPos.y * t;
                this.pos.scale = this.prevPos.scale * p + this.targetPos.scale * t;
                this.pos.opacity = this.prevPos.opacity * p + this.targetPos.opacity * t;
    
                this.posAnimDone = t==1;
            });
        }

        this.renderComboTimer();

        // render local player
        if (this.tilePositioning == null) return;
        let dyFixRate = (-this.playerEngine.getProgressToNextFixRateGarbage(ct)) * this.tilePositioning.tileSize;
        let dyGarbageOnLockDown = this.garbageAnimOffsetY * this.tilePositioning.tileSize;
        this.tilePositioning.offsetYGrid = (this.boardHeight - ((this.playerEngine.sideRule.playHeight-1) * this.tilePositioning.tileSize)) / 2 + dyFixRate + dyGarbageOnLockDown;

        this.renderGarbageIndicator();
        this.renderBackground();
        this.renderGrid();
        this.renderBoard(ct);
        this.renderGhost();
        this.renderActivePiece();
        this.renderPieceSlots();

        if (!this.playerEngine.sideData.isAlive) {
            this.overlayText = 'R.I.P';
        }
    }

    tick(dt: number) {
        if (this.playerEngine.shouldClockRun()) {
            this.playerEngine.addTime(-dt);
            this.updateClockDisplay;
        }

        // line clear animation
        for (let i = 0; i < this.playerEngine.sideRule.height; i++) {
            this.lineClearAnimOffsetY[i] = Math.max(0, this.lineClearAnimOffsetY[i] - dt*this.playerEngine.sideRule.lineClearAnimationRate);
        }
        
        // garbage entry animation
        this.garbageAnimOffsetY = Math.max(0, this.garbageAnimOffsetY - dt*this.playerEngine.sideRule.lineClearAnimationRate);
    }

    updateClockDisplay() {
        this.clockDisplay = this.formatTime(this.playerEngine.sideData.time);
    }
    
    formatTime(s: number) {
        let th = Math.floor(s/3600);
        let tm = Math.floor((s%3600)/60);
        let ts = Math.floor(s%60);
        let tds = Math.floor((s%1)*10);

        let strH = th > 0 ? (th+':') : '';
        let strM = s < 10 ? '' : ((th > 0 ? pad2(tm) : tm) + ':');
        let strS = (tm > 0 ? pad2(ts) : ts);
        let strDs = s < 10 ? ('.'+tds) : '';

        return strH + strM + strS + strDs;
    }
    
    triggerAction(proc: any): boolean {
        if (this.playerEngine.isRunning() && this.playerEngine.sideData.activePiece != null) {
            return this.ngZone.runOutsideAngular(() => {
                let procResult: boolean = proc();
                this.render();
                return procResult;
            });
        };
        return false;
    }

    renderComboTimer() {
        if (this.playerEngine.sideRule.comboMode == ComboMode.TIMER) {
            let isComboActive = this.playerEngine.comboEnd != null && Date.now() <= this.playerEngine.comboEnd;

            let s = Math.max(0, (this.playerEngine.comboEnd - Date.now()) / 1000);
            this.comboTimerSeconds = Math.floor(s);
            this.comboTimerSubSecond = s - Math.floor(s);
            this.comboTimerDisplay = isComboActive ? this.playerEngine.sideData.combo.toString() : '';

            let c = this.uiComboTimerCanvas.nativeElement;
            let g = c.getContext('2d');
            g.clearRect(0, 0, c.width, c.height);

            if (isComboActive) {
                // render sub second
                g.beginPath();
                g.arc(c.width/2, c.height/2, c.height/2, Math.PI * 1.5, (-0.5 + this.comboTimerSubSecond*2) * Math.PI);
                g.lineTo(c.width/2, c.height/2);
                g.closePath();
                g.fillStyle = this.comboTimerColors[this.comboTimerSeconds % this.comboTimerColors.length];
                g.fill();

                // render rest second
                if (this.comboTimerSeconds > 0) {
                    g.beginPath();
                    g.arc(c.width/2, c.height/2, c.height/2, (-0.5 + this.comboTimerSubSecond*2) * Math.PI, Math.PI * 1.5);
                    g.lineTo(c.width/2, c.height/2);
                    g.closePath();
                    g.fillStyle = this.comboTimerColors[(this.comboTimerSeconds - 1) % this.comboTimerColors.length];
                    g.fill();
                }
            }
        }
    }
    
    renderGarbageIndicator() {
        const delayStyles = ['#f00', '#900'];
        const readyStyles = ['#ff0', '#ff0'];
        const totalStyles = ['#ccc', '#999'];

        let canvas = this.uiGarbageIndicator.nativeElement;
        let g = canvas.getContext('2d');
        g.clearRect(0, 0, canvas.width, canvas.height);

        let tileSize = this.tilePositioning.tileSize;
        let offsetY = this.boardHeight;
        let styleNr = 0;
        let numLines = 0;
        this.playerEngine.sideData.garbageQueue.forEach(g => numLines += g.heightLeft);

        let now = Date.now();
        offsetY = this.boardHeight;
        this.playerEngine.sideData.garbageQueue.forEach(gb => {
            let p = Math.min(1, (now - gb.queuedAt) / (gb.delayTime*1000));
            let th = p * gb.heightLeft * tileSize;
            
            g.fillStyle = (p == 1 ? readyStyles : delayStyles)[styleNr % delayStyles.length];
            g.fillRect(0, offsetY - th, this.garbageIndicatorWidth, th);

            offsetY -= th;
            styleNr++;
        });

        
        offsetY = this.boardHeight;
        let indicatorInterval = this.playerEngine.sideRule.garbageSpawnCap > 0 ? this.playerEngine.sideRule.garbageSpawnCap : 4;
        while (numLines > 0) {
            let th = Math.min(numLines, indicatorInterval) * tileSize;
            numLines -= indicatorInterval;

            g.fillStyle = totalStyles[styleNr % totalStyles.length];
            g.fillRect(0, offsetY - th, this.garbageIndicatorWidth / 3, th);

            if (offsetY < 0) {
                break;
            } else {
                offsetY -= th;
            }
            styleNr++;
        }

    }

    renderBackground() {
        let t = this.tilePositioning;

        // dead space
        this.g.fillStyle = '#000';

        // left dead space
        this.g.fillRect(0, 0, t.offsetX, this.boardHeight);

        // right dead space
        this.g.fillRect(t.offsetX + t.offsetWidth, 0, t.offsetX, this.boardHeight);
        
        // top dead space
        this.g.fillRect(0, 0, this.boardWidth, t.offsetY);

        // bottom dead space
        this.g.fillRect(0, t.offsetY + t.offsetHeight, this.boardWidth, t.offsetY);

        // live space
        this.g.fillStyle = '#111';
        this.g.fillRect(t.offsetX, t.offsetY, t.offsetWidth, t.offsetHeight);
    }

    renderGrid() {
        let gridThickness = 2;
        
        // shortcuts
        let sideRule = this.playerEngine.sideRule;
        let t = this.tilePositioning;

        // horizontal lines
        let offsetYFromLineClearAnim = 0;
        let invisibleHeight = sideRule.height - sideRule.playHeight;
        for (let i = sideRule.playHeight - 1; i >= 0; i--) {
            offsetYFromLineClearAnim += this.lineClearAnimOffsetY[i+invisibleHeight];
            // TODO: stop when out of bounds due to line clear animation
            
            this.g.fillStyle = '#fff1';
            this.g.fillRect(
                t.offsetX,
                t.offsetYGrid + (i+1-offsetYFromLineClearAnim)*t.tileSize - gridThickness/2,
                t.offsetWidth,
                gridThickness);
        }

        // vertical lines
        for (let j = 1; j < sideRule.width; j++) {
            this.g.fillRect(t.offsetX + j*t.tileSize - gridThickness/2, t.offsetY, gridThickness, t.offsetHeight);
        }
    }

    renderBoard(ct: number) {
        // shortcuts
        let sideData = this.playerEngine.sideData;
        let sideRule = this.playerEngine.sideRule;
        let t = this.tilePositioning;

        let invisibleHeight = sideRule.height - sideRule.playHeight;

        let offsetYFromLineClearAnim = 0;
        for (let i = sideRule.playHeight - 1; i >= 0; i--) {
            let lca = this.lineClearAnimOffsetY[i+invisibleHeight];
            offsetYFromLineClearAnim += lca;
            
            if (lca > 0) {
                // this.g.fillStyle = 'rgba(255,255,255,' + Math.min(1, Math.abs(Math.cos(lca*Math.PI/2))) + ')';
                this.g.fillStyle = 'rgba(255,255,255,' + Math.min(1, lca/1.5) + ')';
                this.g.fillRect(
                    t.offsetX,
                    t.offsetYGrid + (i+1-lca)*t.tileSize,
                    t.tileSize * sideRule.width,
                    t.tileSize * lca,
                );
            }

            let partial = i == sideRule.playHeight-1 ? this.playerEngine.getProgressToNextFixRateGarbage(ct) : 1;
            for (let j = 0; j < sideRule.width; j++) {
                let tile = sideData.board[i + invisibleHeight][j];
                if (tile != null) {
                    if (tile.isGarbage) {
                        this.renderTile(i-offsetYFromLineClearAnim, j, tile.colorId, partial);
                    } else {
                        this.renderTile(i-offsetYFromLineClearAnim, j, tile.colorId, partial);
                    }
                }
            }
        }
    }

    renderActivePiece() {
        let activePiece = this.playerEngine.sideData.activePiece;
        let sideRule = this.playerEngine.sideRule;
        let invisibleHeight = sideRule.height - sideRule.playHeight;

        if (activePiece != null) {
            let t = this.tilePositioning;
    
            let offsetX = t.offsetX + activePiece.x * t.tileSize;
            let offsetY = t.offsetYGrid + (activePiece.y - invisibleHeight) * t.tileSize;
            
            this.renderPiece(this.uiBoard.nativeElement, activePiece.piece, offsetX, offsetY, t.tileSize, activePiece.y);
        }
    }

    renderTile(i: number, j: number, colorId: number, partial=1) {
        let t = this.tilePositioning;

        this.g.drawImage(this.img.getImg('tiles'),
            colorId*(this.sourceTileSize + this.sourceTileSpacing),
            0,
            this.sourceTileSize,
            this.sourceTileSize * partial,
            t.offsetX + j*t.tileSize,
            t.offsetYGrid + i*t.tileSize,
            t.tileSize,
            t.tileSize * partial,
        );
    }

    renderPieceSlots() {
        let sideData = this.playerEngine.sideData;

        this.renderPieceSlot(this.uiHold.nativeElement, sideData.hold);

        if (this.uiHelperPrevewNrs.length != sideData.previews.length) {
            this.ngZone.run(() => {
                this.uiHelperPrevewNrs.length = sideData.previews.length;
            });
        }

        this.uiPreview.forEach((canvas, index) => {
            this.renderPieceSlot(canvas.nativeElement, sideData.previews[index]);
        });
    }

    renderPieceSlot(canvas: HTMLCanvasElement, piece: Piece) {
        let g = canvas.getContext('2d');
        g.clearRect(0, 0, canvas.width, canvas.height);

        if (piece != null) {
            // figure out tile size
            let tileSize = this.tilePositioning.tileSize * 4 / Math.max(4, piece.size);

            let offsetX = (canvas.width - piece.tiles[0].length * tileSize) / 2;
            let offsetY = (canvas.height - piece.tiles.length * tileSize) / 2;
            offsetY += (MatUtil.getNumEmptyRowsBottom(piece.tiles) - MatUtil.getNumEmptyRowsTop(piece.tiles)) / 2 * tileSize; // offset due to piece not being exactly in the center
            
            this.renderPiece(canvas, piece, offsetX, offsetY, tileSize);
        }
    }

    renderPiece(canvas: HTMLCanvasElement, piece: Piece, offsetX: number, offsetY: number, tileSize: number, boardY: number=null, isGhost=false) {
        let offsetYFromLineClearAnim = 0;
        if (boardY != null) {
            for (let i = this.lineClearAnimOffsetY.length - 1; i >= boardY + piece.tiles.length; i--) {
                offsetYFromLineClearAnim += this.lineClearAnimOffsetY[i];
            }
        }

        let g = canvas.getContext('2d');
        for (let i = piece.tiles.length - 1; i >= 0; i--) {
            if (boardY != null && boardY + i < this.lineClearAnimOffsetY.length) {
                offsetYFromLineClearAnim += this.lineClearAnimOffsetY[boardY + i];
            }
            for (let j = 0; j < piece.tiles[i].length; j++) {
                let tile = piece.tiles[i][j];
                if (tile != null) {
                    let colorId = piece.tiles[i][j].colorId;


                    g.globalAlpha = isGhost ? 0.3 : 1;
                    g.drawImage(this.img.getImg('tiles'),
                        colorId*(this.sourceTileSize + this.sourceTileSpacing),
                        0,
                        this.sourceTileSize,
                        this.sourceTileSize,
                        offsetX + j*tileSize,
                        offsetY + (i-offsetYFromLineClearAnim)*tileSize,
                        tileSize,
                        tileSize,
                    );
                }
            }
        }

        // reset
        g.globalAlpha = 1;
    }
    
    renderGhost() {
        let activePiece = this.playerEngine.sideData.activePiece;
        let sideRule = this.playerEngine.sideRule;
        let invisibleHeight = sideRule.height - sideRule.playHeight;

        if (activePiece) {
            let dy = 0;
            while (!this.playerEngine.checkActivePieceCollision(dy + 1, 0, 0)) {
                dy++;
            }

            let t = this.tilePositioning;
    
            let offsetX = t.offsetX + activePiece.x * t.tileSize;
            let offsetY = t.offsetYGrid + (activePiece.y + dy - invisibleHeight) * t.tileSize;
            this.renderPiece(this.uiBoard.nativeElement, activePiece.piece, offsetX, offsetY, t.tileSize, activePiece.y + dy, true);
        }
    }

    getAspectRatio() {
        let rect = this.uiContainer.nativeElement.getBoundingClientRect();
        return rect.width / rect.height;
    }
    
    setPosition(rect: any, updatePrevPos: boolean) {
        if (updatePrevPos) {
            this.prevPos = {
                x: this.pos.x,
                y: this.pos.y,
                scale: this.pos.scale,
                opacity: this.pos.opacity,
            };
            this.prevPosSince = Date.now();
            this.posAnimDone = false;
        }
        this.targetPos = {
            x: rect.x,
            y: rect.y,
            scale: rect.width / (this.uiContainer.nativeElement.getBoundingClientRect().width / this.pos.scale),
            opacity: rect.opacity != null ? rect.opacity : this.pos.opacity,
        }
    }
}

function pad2(num: number) {
    return (num < 10 ? '0' : '') + num;
}
