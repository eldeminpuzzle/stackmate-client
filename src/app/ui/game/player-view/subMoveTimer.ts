import { SocketService } from "src/app/service/socket.service";
import { PlayerEngine, SubmoveInfo } from "src/app/shared/playerEngine";

export class SubMoveTimer {
    
    subMoveTimeout: any = null;

    constructor(
        private playerEngine: PlayerEngine,
        private socket: SocketService,
    ) {
        playerEngine.listenersOnRoundStarted.add(() => this.resetTimeout());
        playerEngine.listenersOnLockDown.add(() => this.resetTimeout());
        playerEngine.listenersOnRoundEnded.add(() => this.deleteTimeout());
    }

    tick() {
        this.setTimeout();
    }

    resetTimeout() {
        this.deleteTimeout();
        this.setTimeout();
    }

    private setTimeout() {
        if (this.subMoveTimeout == null) {
            this.subMoveTimeout = setTimeout(() => {
                if (this.playerEngine.isRunning() && this.playerEngine.sideData.isAlive) {
                    this.socket.send('subMove', this.playerEngine.createSubMove());
                    this.subMoveTimeout = null;
                }
            }, 500);
        }
    }

    deleteTimeout() {
        if (this.subMoveTimeout != null) {
            clearTimeout(this.subMoveTimeout);
            this.subMoveTimeout = null;
        }
    }
}
