import { SoundService } from "src/app/data/sound.service";
import { ComboMode, SideRule, SpinBonusMode } from "src/app/shared/gameRule";
import { PlayerEngine } from "src/app/shared/playerEngine";

export class PlayerSound {
    constructor(
        private playerEngine: PlayerEngine,
        private soundService: SoundService,
        private sideRule: SideRule,
    ) {
        let channel = playerEngine.isLocal ? 0 : 1;
        playerEngine.listenersOnLockDown.add((m, r) => {
            this.soundService.play('harddrop', channel);
            if (r.backToBack) {
                if (this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && r.isThreeCornersSpin && !r.isMiniTSpin) {
                    this.soundService.play('tspin', channel);
                } else if (this.sideRule.spinBonusMode == SpinBonusMode.IMMOBILE && r.isImmobileSpin) {
                    this.soundService.play('immobile', channel);
                } else {
                    this.soundService.play('clearb2b', channel);
                }
            }
            
            if (this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && r.isMiniTSpin) {
                this.soundService.play('tspinmini', channel); // TODO: tsm sound
            }

            if (r.clearedLines > 0) {
                this.soundService.play('clear', channel);
                if (r.combo == 1 && this.sideRule.comboMode == ComboMode.TIMER) {
                    this.soundService.play('combotimerstart', channel);
                }
            }

            if (r.clearedLines > 0 && r.combo > 1) {
                // skip 2 sounds
                this.soundService.play('combo', channel, Math.min(6, r.combo - 2));
            }

            if (r.isColorClear && sideRule.colorClearBonus != null && sideRule.colorClearBonus > 0) {
                this.soundService.play('colorclear', channel);
            }

            if (r.isPerfectClear && sideRule.perfectClearBonus != null && sideRule.perfectClearBonus > 0) {
                this.soundService.play('perfectclear', channel);
            }
        });

        playerEngine.listenersOnPieceMoved.add((dy, dx, drot, kick) => {
            if (drot != 0) {
                if (kick) {
                    this.soundService.play('rotkick', channel);
                } else {
                    this.soundService.play('rotate', channel);
                }
            }
        });

        playerEngine.listenersOnPieceMoveFailed.add((dy, dx, drot) => {
            if (drot != 0) {
                this.soundService.play('stuck', channel);
            }
        });

        playerEngine.listenersOnLocalHold.add(() => this.soundService.play('hold', channel));

        // death sound always local channel
        playerEngine.listenersOnLockOut.add(() => this.soundService.play('die', 0));

        playerEngine.listenersOnGarbageEntered.add((numLines, isFixRate) => {
            if (isFixRate) {
                
            } else if (numLines < 6) {
                this.soundService.play('receiveone', channel);
            } else if (numLines < 10) {
                this.soundService.play('receivetwo', channel);
            } else if (numLines >= 10) {
                this.soundService.play('receivemany', channel);
            } else {
                this.soundService.play('receivespike', channel);
            }
        });
    }
}
