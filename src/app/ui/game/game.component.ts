import { Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Util } from 'src/app/shared/util';
import { Command, Commands, StartGameData } from 'src/app/shared/gameIntf';
import { Control } from '../control';
import { RoomEnteredInfo } from 'src/app/shared/intf';
import { SocketService } from 'src/app/service/socket.service';

import { GamePixi } from './pixi/gamePixi';
import { ImageService } from 'src/app/data/image.service';
import pixiUtil from './pixi/pixiUtil';
import { SoundService } from 'src/app/data/sound.service';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

    @Output("exitGame") exitGame = new EventEmitter();

    @Input("roomEnteredInfo") roomEnteredInfo: RoomEnteredInfo;

    @ViewChild('pixiContainer', {static:true}) pixiContainer: ElementRef<HTMLDivElement>;

    gamePixi: GamePixi;

    // fpsTimer = new FpsTimer(this.onTick.bind(this), this.render.bind(this), 1000/100000);

    constructor(
        private socket: SocketService,
        private control: Control,
        private img: ImageService,
        private snd: SoundService,
        private ngZone: NgZone,
    ) {
        
    }

    ngOnInit() {
        Util.autoBind(this, this.socket);

        pixiUtil.init(this.img);
        this.gamePixi = new GamePixi(this.socket, this.control, this.snd);
        this.pixiContainer.nativeElement.appendChild(this.gamePixi.getView());

    }

    startGame(startGameData: StartGameData) {
        this.gamePixi.listenersOnRoundEnded.add(() => {
            if (this.gamePixi.gameEngine.gameData.winner != null) {
                setTimeout(() => {
                    // this.fpsTimer.pause();
                    this.exitGame.emit();
                }, 1000);
            }
        });

        this.gamePixi.startGame(startGameData);

        let localEngine = this.gamePixi.gameEngine.playerEngines[this.gamePixi.gameEngine.localSeatNr];
        localEngine.listenersOnLockDown.add((moveInfo, moveResult) => {
            this.socket.send('submitMove', moveInfo);
            // this.socket.send('debugData', this.playerEngine.sideData);
        });
    }

    on_gameCommand(commands: Command[]) {
        // game engine can be null if the player joined a running room. Just wait for next round.
        if (this.gamePixi.gameEngine != null) {
            this.gamePixi.executeCommands(commands);
        }
    }
}
