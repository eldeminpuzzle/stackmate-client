import { style } from "@angular/animations";
import { Container, Sprite } from "pixi.js";
import { PlayerEngine } from "src/app/shared/playerEngine";
import { ShapeUtil } from "./shapeUtil";

export class GarbageIndicatorPixi {
    shapeUtil = new ShapeUtil();

    container = new Container();

    rows: number;

    delaySprites: Sprite[] = [];
    fullSprite = this.shapeUtil.rect(0, 0, 1, 0, 0xcccccc);

    delayColor = 0xffff00;
    readyColor = [0xff0000, 0x990000];

    constructor(
        private playerEngine: PlayerEngine, x: number, y: number, width: number, height: number) {
        let rows = this.playerEngine.sideRule.playHeight - 1;
        this.container.position.set(x-1, y);
        this.container.scale.set(width, height / rows);
        this.rows = rows;

        this.container.addChild(this.fullSprite);
    }

    tick(delta: number, ct: number) {
        let queue = this.playerEngine.sideData.garbageQueue;
        let ty = this.rows;
        let breakGb = false;
        let targetLen = queue.length;
        let styleNr = 0;
        for (let i = 0; i < queue.length; i++) {
            let gb = queue[i];
            if (this.delaySprites.length < i + 1) {
                this.delaySprites.push(this.createDelaySprite());
                this.container.addChild(this.delaySprites[i]);
            }

            let th = gb.heightLeft != null ? gb.heightLeft : gb.totalHeight;
            if (ty < th) {
                th = ty;
                breakGb = true;
            }
            ty -= th;

            let p = Math.min(1, (ct - gb.queuedAt) / (gb.delayTime*1000));

            this.delaySprites[i].position.y = ty + (1-p) * th;
            this.delaySprites[i].height = p * th;
            this.delaySprites[i].tint = p < 1 ? this.delayColor : this.readyColor[styleNr];

            if (breakGb) {
                targetLen = i;
                break;
            }

            styleNr = 1 - styleNr;
        }

        this.fullSprite.position.y = ty;
        this.fullSprite.height = this.rows - ty;

        if (this.delaySprites.length > targetLen) {
            for (let i = targetLen; i < this.delaySprites.length; i++) {
                this.container.removeChild(this.delaySprites[i]);
            }
            this.delaySprites.splice(queue.length);
        }
    }

    getPixi() {
        return this.container;
    }

    createDelaySprite() {
        return this.shapeUtil.rect(0.3, 0, 0.7, 1, 0xff0000);
    }
}
