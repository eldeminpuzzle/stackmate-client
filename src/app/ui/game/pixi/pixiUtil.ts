import { Rectangle, Texture } from "pixi.js";
import { ImageService } from "src/app/data/image.service";
import { Tile } from "src/app/shared/gameData";
import { MinoPixi } from "./minoPixi";

class PixiUtil {
    
    private tilesTexture: Texture;
    private tileTextures: Texture[] = [];

    private img: ImageService;

    init(img: ImageService) {
        this.img = img;
        this.tilesTexture = Texture.from(this.img.getImg('tiles'));
        
        for (let i = 0; i < 12; i++) {
            let tx = i * 31;
            this.tileTextures.push(new Texture(this.tilesTexture.baseTexture, new Rectangle(tx, 0, 30, 30)));
        }
    }

    getTileTexture(tile: Tile) {
        return this.tileTextures[tile.colorId];
    }

    createMino(tile: Tile, i: number, j: number) {
        return new MinoPixi(this.tileTextures[tile.colorId], j, i);
    }

}

const pixiUtil = new PixiUtil();
export default pixiUtil;
