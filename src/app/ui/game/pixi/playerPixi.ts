
import { Container, Sprite, Texture } from "pixi.js";
import { ImageService } from "src/app/data/image.service";
import { SoundService } from "src/app/data/sound.service";
import { SocketService } from "src/app/service/socket.service";
import { ComboMode } from "src/app/shared/gameRule";
import { PlayerEngine } from "src/app/shared/playerEngine";
import { Control } from "../../control";
import { ComboTimerPixi } from "../comboTimerPixi";
import { PlayerSound } from "../player-view/playerSound";
import { SubMoveTimer } from "../player-view/subMoveTimer";
import { SidePos } from "./fieldLayout";
import { FieldPixi } from "./fieldPixi";
import { GarbageIndicatorPixi } from "./garbageIndicatorPixi";
import { PieceSlotPixi } from "./pieceSlotPixi";
import { ShapeUtil } from './shapeUtil';

export class PlayerPixi {
    private subMoveTimer: SubMoveTimer;

    private container: Container = new Container();
    private gbiPixi: GarbageIndicatorPixi;
    private fieldPixi: FieldPixi;
    private holdPixi: PieceSlotPixi;
    private previewPixis: PieceSlotPixi[] = [];
    private comboTimerPixi: ComboTimerPixi;
    
    private shapeUtil = new ShapeUtil();
    
    private borderThickness = 1;
    private borderTint = 0x444444;
    
    private padding = 4;
    private leftW = 40;
    private gbiW = 3;
    private fieldW = 100;
    private rightW = 40;
    
    private innerW = this.leftW + this.gbiW + this.fieldW + this.rightW;
    private innerH = 200;
    
    private tw = this.padding * 2 + this.leftW + this.fieldW + this.rightW;
    private th = this.padding * 2 + this.innerH;

    // animation
    private ctTargetPos: SidePos = { x: 0, y: 0, w: 0, h: 0};
    private ctPrevPos: SidePos = this.ctTargetPos;
    private ctAnimElapsed: number = 0;
    private ctAnimDuration = 1000;
    private deathAnimElapsed: number = null;
    private deathAnimDuration = 1000;
    private deathOnFinish: () => void = null;

    constructor(
        private playerEngine: PlayerEngine,
        private socket: SocketService,
        private control: Control,
        private snd: SoundService,
    ) {
        // this.createBackground();

        let x = this.padding;
        let y = this.padding;
        let borderW = this.borderThickness * 4 + this.innerW;
        let borderH = this.innerH;

        let pieceSlotWidth = this.leftW - this.borderThickness;
        let pieceSlotHeight = pieceSlotWidth * 0.65;

        // top border
        this.container.addChild(this.shapeUtil.rect(x, y, borderW, this.borderThickness, this.borderTint));

        // main row
        x = this.padding;
        y += this.borderThickness;
        
        this.container.addChild(this.shapeUtil.rect(x, y, this.borderThickness, borderH, this.borderTint));
        x += this.borderThickness;

        //      left
        {
            let prevY = y;

            //      hold
            this.holdPixi = new PieceSlotPixi(x, y, pieceSlotWidth, pieceSlotHeight);
            this.container.addChild(this.holdPixi.getPixi());
            y += pieceSlotHeight;

            //      comboTimer
            if (this.playerEngine.sideRule.comboMode == ComboMode.TIMER) {
                let comboTimerDiameter = this.leftW * 0.7;
                this.comboTimerPixi = new ComboTimerPixi(x + (this.leftW - comboTimerDiameter) / 2, y, comboTimerDiameter, this.playerEngine);
                this.container.addChild(this.comboTimerPixi.getPixi());
                y += comboTimerDiameter;
            }
            y = prevY;
        }

        x += this.leftW;

        //      border
        this.container.addChild(this.shapeUtil.rect(x, y, this.borderThickness, borderH, this.borderTint));
        x += this.borderThickness;

        //      gbi
        this.gbiPixi = new GarbageIndicatorPixi(playerEngine, x, y, this.gbiW, this.innerH);
        this.container.addChild(this.gbiPixi.getPixi());
        x += this.gbiW;

        //      field
        this.fieldPixi = new FieldPixi(playerEngine, x, y, this.fieldW, this.innerH);
        this.container.addChild(this.fieldPixi.getPixi());
        x += this.fieldW;

        //      border
        this.container.addChild(this.shapeUtil.rect(x, y, this.borderThickness, borderH, this.borderTint));
        x += this.borderThickness;

        //      right (piece previews)
        {
            let prevY = y;
            for (let i = 0; i < this.playerEngine.sideRule.numPreviews; i++) {
                let previewPixi = new PieceSlotPixi(x, y, pieceSlotWidth, pieceSlotHeight);
                this.previewPixis.push(previewPixi);
                this.container.addChild(previewPixi.getPixi());
                y += pieceSlotHeight;
            }
            x += this.rightW;
            y = prevY;
        }
        
        //      border
        this.container.addChild(this.shapeUtil.rect(x, y, this.borderThickness, borderH, this.borderTint));
        x += this.borderThickness;

        // bottom row
        x = this.padding;
        y += this.innerH;
        // bottom border
        this.container.addChild(this.shapeUtil.rect(x, y, borderW, this.borderThickness, this.borderTint));
        
        

        // sound
        // TODO: if too many players, only local?
        new PlayerSound(this.playerEngine, this.snd, this.playerEngine.sideRule);

        this.playerEngine.listenersOnRoundStarted.add(this.onRoundStarted.bind(this));
        this.playerEngine.listenersOnLockDown.add(this.updatePieceSlots.bind(this));
        this.playerEngine.listenersOnLocalHold.add(this.updatePieceSlots.bind(this));

        this.playerEngine.listenersOnLockOut.add(() => {
            if (this.playerEngine.isLocal) {
                this.socket.send('die');
            }
        });
    }

    private onRoundStarted() {
        if (this.playerEngine.isLocal) {
            this.control.bind(this, this.playerEngine);
            this.control.unpressKeys();
            this.subMoveTimer = new SubMoveTimer(this.playerEngine, this.socket);
        }

        this.updatePieceSlots();
    }

    private updatePieceSlots() {
        this.holdPixi.setPiece(this.playerEngine.sideData.hold);
        for (let i = 0; i < this.playerEngine.sideData.previews.length; i++) {
            this.previewPixis[i].setPiece(this.playerEngine.sideData.previews[i]);
        }
    }

    getPixi(): any {
        return this.container;
    }

    setPos(pos: SidePos) {
        this.ctPrevPos = this.ctTargetPos;
        this.ctTargetPos = pos;
        this.ctAnimElapsed = 0;
    }

    tick(dt: number, ct: number) {
        this.gbiPixi.tick(dt, ct);
        this.fieldPixi.tick(dt, ct);

        if (this.playerEngine.isLocal) {
            if (this.subMoveTimer != null) {
                this.subMoveTimer.tick();
            }
        }

        // field layout animation
        this.ctAnimElapsed += dt;
        let p = Math.min(1, this.ctAnimElapsed / this.ctAnimDuration);
        let tx = this.intp(this.ctPrevPos.x, this.ctTargetPos.x, p);
        let ty = this.intp(this.ctPrevPos.y, this.ctTargetPos.y, p);
        let tw = this.intp(this.ctPrevPos.w, this.ctTargetPos.w, p);
        let th = this.intp(this.ctPrevPos.h, this.ctTargetPos.h, p);

        // death animation
        if (this.deathAnimElapsed != null) {
            this.deathAnimElapsed += dt;
            let pd = Math.min(1, this.deathAnimElapsed / this.deathAnimDuration);
            ty -= this.intp(0, th * 0.3, pd);
            this.container.alpha = Math.pow(1 - pd, 9);

            if (pd == 1) {
                this.deathOnFinish();
            }
        }
        this.container.position.set(tx, ty);
        this.container.scale.set(tw / this.tw, th / this.th);


        if (this.comboTimerPixi != null) {
            this.comboTimerPixi.tick(dt, ct);
        }
    }

    private intp(start: number, end: number, x: number) {
        let p = 1 - Math.pow(1-x, 9);
        return p * end + (1-p) * start;
    }

    deathAnimation(onFinish: () => void) {
        this.deathAnimElapsed = 0;
        this.deathOnFinish = onFinish;
    }
}
