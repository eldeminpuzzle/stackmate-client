import { Sprite, Texture } from "pixi.js";

export class MinoPixi {

    sprite: Sprite;

    constructor(texture: Texture, x: number, y: number) {
        this.sprite = new Sprite(texture);
        this.sprite.width = 1;
        this.sprite.height = 1;
        this.sprite.position.set(x, y);
    }

    getPixi() {
        return this.sprite;
    }

    rotate(rows: number) {
        let p = this.sprite.position;
        p.set(rows - 1 - p.y, p.x);
    }
    
    addPosition(x: number, y: number) {
        let p = this.sprite.position;
        p.set(x + p.x, y + p.y);
    }

    setPosition(x: number, y: number) {
        let p = this.sprite.position;
        p.set(x, y);
    }

    setTexture(texture: Texture) {
        this.sprite.texture = texture;
    }
}
