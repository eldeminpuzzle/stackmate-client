import { Sprite, Texture } from "pixi.js";

export class ShapeUtil {

    hLines(x: number, width: number, ys: number[], thickness: number, color: number): Sprite[] {
        return ys.map(y => {
            let sprite = new Sprite(Texture.WHITE);
            sprite.position.set(x, y - thickness/2);
            sprite.width = width;
            sprite.height = thickness;
            sprite.tint = color;
            return sprite;
        });
    }

    vLines(y: number, height: number, xs: number[], thickness: number, color: number): Sprite[] {
        return xs.map(x => {
            let sprite = new Sprite(Texture.WHITE);
            sprite.position.set(x - thickness/2, y);
            sprite.width = thickness;
            sprite.height = height;
            sprite.tint = color;
            return sprite;
        });
    }

    rect(x: number, y: number, width: number, height: number, color: number): Sprite {
        let sprite = new Sprite(Texture.WHITE);
        sprite.position.set(x, y);
        sprite.width = width;
        sprite.height = height;
        sprite.tint = color;
        return sprite;
    }

}
