import { Emitter } from "pixi-particles";
import { Container, Graphics, Sprite, Texture } from "pixi.js";
import { Tile } from "src/app/shared/gameData";
import { SideRule } from "src/app/shared/gameRule";
import { ClearInfo, MoveInfo, MoveResult, PlayerEngine } from "src/app/shared/playerEngine";
import { MinoPixi } from "./minoPixi";
import { createLineClearParticleEmitter } from "./particle/lineClear";
import pixiUtil from "./pixiUtil";

export class FieldRow {
    container = new Container();
    minos: MinoPixi[] = [];
    baseY: number;
    lcRect: Sprite;
    lcaY = 0; // line clear animation Y
    sideRule: SideRule;

    constructor(private parent: Container, sideRule: SideRule, baseY: number) {
        this.parent.addChild(this.container);
        this.sideRule = sideRule;
        this.minos = new Array(sideRule.width);
        this.container.y = baseY;
        this.baseY = baseY;
    }

    updateMino(tile: Tile, x: number) {
        let mino = this.minos[x];
        if (tile == null && mino != null) {
            this.container.removeChild(mino.getPixi());
            this.minos[x] = null;
        } else if (tile != null && mino == null) {
            mino = pixiUtil.createMino(tile, 0, x);
            this.container.addChild(mino.getPixi());
            this.minos[x] = mino;
        } else if (tile != null && mino != null) {
            this.minos[x].setTexture(pixiUtil.getTileTexture(tile));
        }
    }

    setBaseY(baseY: number) {
        this.baseY = baseY;
        this.container.y = this.baseY;
    }
    
    addLcaY(lcaY: number) {
        this.lcaY += lcaY;
    }

    destroy() {
        this.parent.removeChild(this.container);
    }

    tick(dt: number, ct: number, rowBelow: FieldRow) {
        this.lcaY = Math.max(0, this.lcaY - dt/1000*this.sideRule.lineClearAnimationRate);
        this.container.y = this.baseY - this.lcaY;

        let lcH = (rowBelow == null ? (this.baseY + 1) : (rowBelow.baseY - rowBelow.lcaY)) - (this.baseY - this.lcaY + 1);
        if (lcH > 0) {
            if (this.lcRect == null) {
                this.lcRect = new Sprite(Texture.WHITE);
                this.lcRect.width = this.minos.length;
                this.container.addChild(this.lcRect);
            }
            this.lcRect.position.y = 1;
            this.lcRect.height = lcH;
        } else {
            if (this.lcRect != null) {
                this.container.removeChild(this.lcRect);
                this.lcRect = null;
            }
        }
    }
}

export class FieldPixi {
    private container: Container = new Container();
    private fieldContainer: Container = new Container();
    private maskedContainer = new Container();
    private width: number;
    private height: number;

    private activePiece: Container = new Container();
    private activeMinos: MinoPixi[] = [];

    private ghostPiece: Container = new Container();
    private ghostMinos: MinoPixi[] = [];

    private fieldRows: FieldRow[] = [];

    private tileSize: number;

    // line clear / garbage animation
    private gbAnimY = 0;

    constructor(
        private playerEngine: PlayerEngine,
        x: number, y: number, width: number, height: number
    ) {
        this.repositionField(width, height, x, y);

        
        // init field
        this.container.addChild(this.fieldContainer);
        this.fieldContainer.addChild(this.maskedContainer);
        this.fieldRows = new Array(this.playerEngine.sideRule.height);
        for (let i = 0; i < this.playerEngine.sideRule.height; i++) {
            this.fieldRows[i] = this.createRow(i);
        }

        this.playerEngine.listenersOnPieceMoved.add(this.onPieceMoved.bind(this));
        this.playerEngine.listenersOnLockDown.add(this.onLockDown.bind(this));
        this.playerEngine.listenersOnLinesCleared.add(this.onLinesCleared.bind(this));
        this.playerEngine.listenersOnMinoUpdated.add(this.onMinoUpdated.bind(this));
        this.playerEngine.listenersOnGarbageEntered.add(this.onGarbageEntered.bind(this));

        // active piece
        this.maskedContainer.addChild(this.activePiece);
        this.maskedContainer.addChild(this.ghostPiece);
        this.ghostPiece.alpha = 0.15;
        this.playerEngine.listenersOnLocalHold.add(this.resetActivePiece.bind(this));
        this.playerEngine.listenersOnRoundStarted.add(this.resetActivePiece.bind(this));
    }

    private createRow(baseY: number): FieldRow {
        return new FieldRow(this.maskedContainer, this.playerEngine.sideRule, baseY);
    }

    private onGarbageEntered(numLines: number, isFixRate: boolean) {
        const board = this.playerEngine.sideData.board;

        // delete top rows
        for (let i = 0; i < numLines; i++) {
            this.fieldRows[i].destroy();
        }

        // shift middle rows
        for (let i = 0; i < board.length - numLines; i++) {
            this.fieldRows[i] = this.fieldRows[i + numLines];
            this.fieldRows[i].setBaseY(i);
        }

        // spawn bottom rows
        for (let i = board.length - numLines; i < board.length; i++) {
            this.fieldRows[i] = this.createRow(i);
            for (let j = 0; j < board[i].length; j++) {
                this.updateMino(j, i);
            }
        }

        this.activePiece.position.y = this.playerEngine.sideData.activePiece.y;
        this.repositionGhost();

        this.gbAnimY += numLines;
    }

    private resetActivePiece() {
        this.activePiece.removeChildren();
        this.activeMinos = [];
        this.ghostPiece.removeChildren();
        this.ghostMinos = [];

        let ap = this.playerEngine.sideData.activePiece;
        this.activePiece.position.set(ap.x, ap.y);
        let tiles = ap.piece.tiles;
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < tiles.length; j++) {
                let tile = tiles[i][j];
                if (tile != null) {
                    let mino = pixiUtil.createMino(tile, i, j);
                    this.activeMinos.push(mino);
                    let ghostMino = pixiUtil.createMino(tile, i, j);
                    this.ghostMinos.push(ghostMino);
                    
                }
                this.activeMinos.forEach(m => this.activePiece.addChild(m.getPixi()));
                this.ghostMinos.forEach(m => this.ghostPiece.addChild(m.getPixi()));
            }
        }
        this.repositionGhost();
    }

    private onPieceMoved(dy: number, dx: number, drot: number, kick: boolean) {
        let ap = this.playerEngine.sideData.activePiece;
        this.activePiece.position.set(ap.x, ap.y);
        this.activeMinos.forEach(m => {
            for (let i = 0; i < drot; i++) {
                m.rotate(ap.piece.tiles.length);
            }
        });
        
        this.repositionGhost();
        this.ghostMinos.forEach(m => {
            for (let i = 0; i < drot; i++) {
                m.rotate(ap.piece.tiles.length);
            }
        });
    }

    private repositionGhost() {
        let ap = this.playerEngine.sideData.activePiece;
        let gy = 0;
        while (!this.playerEngine.checkActivePieceCollision(gy + 1, 0, 0)) { gy++; }
        this.ghostPiece.position.set(ap.x, ap.y + gy);
    }

    private onMinoUpdated(poses: {x: number, y: number}[]) {
        for (let pos of poses) {
            this.updateMino(pos.x, pos.y);
        }
    }

    private updateMino(x: number, y: number) {
        let tile = this.playerEngine.sideData.board[y][x];
        this.fieldRows[y].updateMino(tile, x);
    }

    private onLockDown(m: MoveInfo, r: MoveResult) {
        this.resetActivePiece();
    }

    private onLinesCleared(clearInfo: ClearInfo) {
        let lines = clearInfo.linesCleared;
        let count = 1;

        // delete rows
        for (let i of lines) {
            this.fieldRows[i].destroy();
            
            const emitterContainer = new Container();
            this.fieldContainer.addChild(emitterContainer);
            emitterContainer.position.set(0, i);
            
            if (this.playerEngine.isLocal) {
                const emitter = createLineClearParticleEmitter(emitterContainer, clearInfo.linesCleared.length);
                emitter.playOnceAndDestroy(() => {
                    this.fieldContainer.removeChild(emitterContainer);
                });
            }
        }

        // shift rows
        for (let i = lines[lines.length - 1] - 1; i >= 0; i--) {
            if (count < lines.length && lines[lines.length - 1 - count] == i) {
                count++;
            } else {
                this.fieldRows[i].setBaseY(i + count);
                this.fieldRows[i].addLcaY(count);
                this.fieldRows[i+count] = this.fieldRows[i];
            }
        }

        // create empty rows on top
        for (let i = 0; i < lines.length; i++) {
            this.fieldRows[i] = this.createRow(i);
        }
    }

    private repositionField(width: number, height: number, x: number, y: number) {
        let cols = this.playerEngine.sideRule.width;
        let rows = this.playerEngine.sideRule.playHeight - 1;
        let invRows = this.playerEngine.sideRule.height - (this.playerEngine.sideRule.playHeight - 1);
        this.width = width;
        this.height = height;
        this.tileSize = Math.min(this.width / cols, this.height / rows);
        this.container.position.set(x + (width - this.tileSize * cols) / 2, y + (height - this.tileSize * rows) - (invRows * this.tileSize));
        this.container.scale.set(this.tileSize, this.tileSize);

        
        this.maskedContainer.mask = new Graphics()
            .beginFill(0xffffff)
            .drawRect(0, invRows, cols, this.playerEngine.sideRule.playHeight - 1)
            .endFill();
        this.maskedContainer.addChild(this.maskedContainer.mask);
    }

    getPixi() {
        return this.container;
    }

    tick(dt: number, ct: number) {
        // line clear animation
        this.fieldRows[this.fieldRows.length - 1].tick(dt, ct, null);
        for (let i = this.fieldRows.length - 2; i >= 0; i--) {
            this.fieldRows[i].tick(dt, ct, this.fieldRows[i+1]);
        }
        
        // garbage entry animation
        this.gbAnimY = Math.max(0, this.gbAnimY - dt/1000*this.playerEngine.sideRule.garbageEntryAnimationRate)
        let gbFixRateY = this.playerEngine.getProgressToNextFixRateGarbage(ct) - 1;
        this.fieldContainer.y = this.gbAnimY - gbFixRateY;
    }
}
