
import * as PIXI from 'pixi.js';
import { Application } from "pixi.js";
import { SoundService } from 'src/app/data/sound.service';
import { SocketService } from 'src/app/service/socket.service';
import { GameEngine } from 'src/app/shared/gameEngine';
import { Command, Commands, DieCommand, StartGameData } from 'src/app/shared/gameIntf';
import { Control } from '../../control';
import { FieldLayout, LayoutInput } from './fieldLayout';
import { PlayerPixi } from './playerPixi';

export class GamePixi {
    pixi: Application;
    gameEngine: GameEngine;

    container = new PIXI.Container();
    playerPixis: PlayerPixi[] = [];
    
    listenersOnRoundEnded: Set<any> = new Set();

    // dependencies
    fieldLayout = new FieldLayout();

    constructor (
        private socket: SocketService,
        private control: Control,
        private snd: SoundService,
    ) {
        this.pixi = new PIXI.Application({ resizeTo: window });
        this.pixi.ticker.stop();
        
        this.pixi.ticker.add((frame) => {
            // console.log(this.pixi.ticker.FPS);
            this.tick(this.pixi.ticker.elapsedMS);
        });

        this.pixi.renderer.on('resize', () => {
            if (this.gameEngine != null) {
                this.recalculatePlayerLayouts();
            }
        });

        this.pixi.stage.addChild(this.container);
    }

    startGame(startGameData: StartGameData) {
        this.pixi.ticker.start();
        this.container.removeChildren();
        
        this.gameEngine = new GameEngine(startGameData, false);

        this.gameEngine.listenersOnRoundEnded.add(() => {
            this.listenersOnRoundEnded.forEach(l => l());
        });

        this.container.removeChildren();
        this.playerPixis = [];
        for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
            let playerPixi = new PlayerPixi(this.gameEngine.playerEngines[i], this.socket, this.control, this.snd);
            this.container.addChild(playerPixi.getPixi());
            this.playerPixis.push(playerPixi);
        }

        this.recalculatePlayerLayouts();
        // this.recalculatePlayerLayouts(true);
        // this.gameEngine.playerEngines.forEach(p => p.listenersOnLockOut.add(() => this.recalculatePlayerLayouts(true)));
        // this.fpsTimer.start();
    }

    private recalculatePlayerLayouts() {
        let remap = new Map<number, number>();
        let teamsInp: number[] = [];
        let main = null;
        this.gameEngine.gameData.sides.forEach((sideData, index) => {
            if (sideData.isAlive) {
                remap.set(teamsInp.length, index);
                teamsInp.push(this.gameEngine.gameRule.sides[index].team);
            }

            if (this.gameEngine.localSeatNr == index) {
                // TODO: allow main on other players if local is dead
                main = index;
            }
        });

        let inp: LayoutInput = {
            teams: teamsInp,
            main,
            width: this.pixi.renderer.width,
            height: this.pixi.renderer.height,
        };
        let poses = this.fieldLayout.getTargetPoses(inp);
        poses.forEach((pos, index) => {
            this.playerPixis[remap.get(index)].setPos(pos);
        });
    }

    tick(delta: number) {
        this.control.updateTick(delta/1000);

        let ct = Date.now();
        this.gameEngine.tick(delta, ct);
            this.playerPixis.forEach(p => {
                if (p != null) {
                    p.tick(delta, ct);
                }
            });
    }

    getView() {
        return this.pixi.view;
    }

    
    executeCommands(commands: Command[]) {
        // game engine can be null if the player joined a running room. Just wait for next round.
        if (this.gameEngine != null) {
            this.gameEngine.executeCommands(commands);
        }

        commands.forEach(cmd => {
            if (cmd.type == Commands.DIE) {
                this.recalculatePlayerLayouts();
                this.playerPixis[cmd.sideNr].deathAnimation(() => {
                    if (this.playerPixis[cmd.sideNr] != null) {
                        this.container.removeChild(this.playerPixis[cmd.sideNr].getPixi());
                        this.playerPixis[cmd.sideNr] = null;
                    }
                });
            }
        });
    }
}
