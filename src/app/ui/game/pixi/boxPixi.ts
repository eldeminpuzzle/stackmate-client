import { Container } from "pixi.js";

export class VBox {
    container = new Container();
    w: number;
    h: number;

    

    constructor(x: number, y: number, w: number, h: number) {
        this.container.position.set(x, y);
        this.w = w;
        this.h = h;
    }


}
