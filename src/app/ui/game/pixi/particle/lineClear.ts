import { Emitter } from "pixi-particles";
import { BaseTexture, Container, Texture } from "pixi.js";

const img = new Image();
img.src = 'assets/img/default/particle/particle.png';
const base = new BaseTexture(img);
const texture = new Texture(base);// return you the texture

export function createLineClearParticleEmitter( container: Container, power: number ) {
    let pp = Math.min(4, power);
    let emitterLifeTime = 0.015 * (pp + 2);
    let speed = [8,16,20,24,24][pp];
    let lifetime = [0.2, 0.8, 1.2, 1.2, 1.2][pp];

    return new Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on top of a bitmap, and not use the root stage Container
        container,

        // The collection of particle images to use
        // [Texture.WHITE],
        [texture],

        // Emitter configuration, edit this to change the look
        // of the emitter
        {
            "alpha": {
                "start": 1,
                "end": 0
            },
            "scale": {
                "start": 0.03,
                "end": 0.012,
                "minimumScaleMultiplier": 0.5
            },
            "color": {
                "start": "#ffffff",
                "end": "#88ffff"
                // "start": "#00f2ff",
                // "end": "#0000ff"
            },
            "speed": {
                "start": speed,
                "end": 0,
                "minimumSpeedMultiplier": 0.001
            },
            "acceleration": {
                "x": 0,
                "y": 3
            },
            "maxSpeed": 0,
            "startRotation": {
                "min": 0,
                "max": 360
            },
            "noRotation": false,
            "rotationSpeed": {
                "min": 360,
                "max": 720
            },
            "lifetime": {
                "min": 0.2,
                "max": lifetime
            },
            // "blendMode": "normal",
            "frequency": 0.006,
            "emitterLifetime": emitterLifeTime,
            "maxParticles": 500,
            "pos": {
                "x": 0,
                "y": 0
            },
            "addAtBack": false,
            "spawnType": "rect",
            "spawnRect": {
                "x": 0,
                "y": 0,
                "w": 10,
                "h": 1
            }
        }
    );
}