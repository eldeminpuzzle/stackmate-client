import { Container } from "pixi.js";
import { Piece } from "src/app/shared/gameData";
import { MatUtil } from "src/app/shared/matUtil";
import { MinoPixi } from "./minoPixi";
import pixiUtil from "./pixiUtil";

export class PieceSlotPixi {
    container = new Container();
    gridContainer = new Container();
    minos: MinoPixi[] = [];

    x: number;
    y: number;
    width: number;
    height: number;

    constructor(x: number, y: number, w: number, h: number) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;

        this.container.position.set(this.x, this.y);
        this.container.addChild(this.gridContainer);
    }

    getPixi() {
        return this.container;
    }

    setPiece(piece: Piece) {
        this.gridContainer.removeChildren();
        this.minos = [];

        if (piece != null) {
            let tileSize = this.width / Math.max(4, piece.size);
            let offsetX = (this.width - tileSize * piece.tiles[0].length) / 2;
            let offSetY = (this.height - tileSize * piece.tiles.length) / 2 + (MatUtil.getNumEmptyRowsBottom(piece.tiles) - MatUtil.getNumEmptyRowsTop(piece.tiles)) / 2 * tileSize;
            this.gridContainer.position.set(offsetX, offSetY);
            this.gridContainer.scale.set(tileSize, tileSize);
    
            for (let i = 0; i < piece.tiles.length; i++) {
                for (let j = 0; j < piece.tiles[i].length; j++) {
                    let tile = piece.tiles[i][j];
                    if (tile != null) {
                        let mino = pixiUtil.createMino(tile, i, j);
                        this.gridContainer.addChild(mino.getPixi());
                        this.minos.push(mino);
                    }
                }
            }
        }
    }
}
