import { ChangeDetectorRef, Component, EventEmitter, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { Util } from 'src/app/shared/util';
import { GameResult, PlayerJoinedInfo, RoomEnteredInfo, RoomInfo } from 'src/app/shared/intf';
import { TestConfig } from 'src/app/testConfig';
import { GameRuleInputComponent } from './game-rule-input/game-rule-input.component';
import { SocketService } from 'src/app/service/socket.service';
import { Session } from 'src/app/endpoint/session';
import { RoomSettingsComponent } from './room-settings/room-settings.component';
import { GameResultComponent } from './game-result/game-result.component';

enum Tabs { ROOM_SETTINGS, RULE_SETTINGS, GAME_RESULT };

@Component({
    selector: 'app-room',
    templateUrl: './room.component.html',
    styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

    @Output() startGame = new EventEmitter();

    @ViewChild('uiRoomSettings', {static: false}) uiRoomSettings: RoomSettingsComponent;
    @ViewChild('uiGameRuleInput', {static: false}) uiGameRuleInput: GameRuleInputComponent;
    @ViewChild('uiGameResult', {static: false}) uiGameResult: GameResultComponent;

    roomEnteredInfo: RoomEnteredInfo;
    allowStart = false;

    tabs = Tabs;
    tab = Tabs.ROOM_SETTINGS;

    gameResult: GameResult = null;

    constructor(
        private socket: SocketService,
        private session: Session,
        private ngZone: NgZone,
    ) {
        
    }

    ngOnInit() {
        Util.autoBind(this, this.socket);
    }
    
    enterRoom(info: RoomEnteredInfo) {
        if (info.gameRuleJson != null) {
            setTimeout(() => {
                this.uiGameRuleInput.loadRuleJson(info.gameRuleJson);
            }, 500);
        }
        this.roomEnteredInfo = info;
        this.checkAllowStart();
    }

    on_playerJoined(info: PlayerJoinedInfo) {
        this.ngZone.run(() => {
            if (info.seatNr == null) {
                this.roomEnteredInfo.room.spectators.push(info.player);
            } else {
                this.roomEnteredInfo.room.players[info.seatNr] = info.player;
            }
            this.checkAllowStart();
        });
    }

    on_updateGameRule(json: string, roomInfo: RoomInfo) {
        this.ngZone.run(() => {
            this.uiGameRuleInput.loadRuleJson(json);
            this.roomEnteredInfo.room = roomInfo;
        });
    }

    on_playerAutoJoined() {
        this.ngZone.run(() => {
            this.uiGameRuleInput.addSlotDueToAutoJoin();
        });
    }

    on_updateRoomInfo(roomInfo: RoomInfo) {
        this.ngZone.run(() => {
            this.roomEnteredInfo.room = roomInfo;
        });
    }

    on_gameEnded(gameResult: GameResult) {
        if (gameResult.roomId == this.roomEnteredInfo.room.id) {
            this.gameResult = gameResult;
        }

        if (gameResult.coinReward != null) {
            this.session.account.coins += gameResult.coinReward;
        }
    }
    
    showGameResult() {
        this.tab = Tabs.GAME_RESULT;
        this.uiGameResult.showAnimation();
    }

    checkAllowStart() {
        this.allowStart = false;
        if (!this.roomEnteredInfo.room.isHost) return;
        if (this.roomEnteredInfo.room.autoStart != null) return;
        if (!this.roomEnteredInfo.room.autoJoin) {
            for (let player of this.roomEnteredInfo.room.players) {
                if (player == null) {
                    return;
                }
            }
        }

        this.allowStart = true;
    }

    onStart() {
        if (this.roomEnteredInfo.room.autoJoin || this.isAllPlayersReady()) {
            this.socket.send("startGame", this.uiGameRuleInput.getGameRule());
        } else {
            alert(this.uiGameRuleInput.getNumPlayersRequired() + ' players required. Wait for more players or remove empty player slots to start the game!');
        }
    }

    isAllPlayersReady() {
        let numPlayersRequired = this.uiGameRuleInput.getNumPlayersRequired();
        for (let i = 0; i < numPlayersRequired; i++) {
            if (this.roomEnteredInfo.room.players[i] == null) {
                return false;
            }
        }
        return true;
    }

    confirmGameRule(data: any) {
        this.socket.send('updateGameRule', data.gameRule, data.json);
    }

    onSwitchSeats(sideNr: number) {
        console.log(sideNr);
        this.socket.send('switchSeatNr', sideNr);
    }
    
    onTransferHost(e: {host: number, hostIsSpectator: boolean}) {
        this.socket.send('transferHost', e.host, e.hostIsSpectator);
    }
    
    onTeamClick(sideNr: number) {
        if (this.roomEnteredInfo.room.isHost) {
            let oldTeam = this.uiGameRuleInput.getEffectiveSideRule('team', sideNr);
            this.uiGameRuleInput.submitValue('team', (oldTeam + 1) % 5, sideNr);
        }
    }

    onJoin() {
        this.socket.send('autoJoin');
    }

    toggleAutoJoin() {
        this.roomEnteredInfo.room.autoJoin = !this.roomEnteredInfo.room.autoJoin;
        this.socket.send('setAutoJoin', this.roomEnteredInfo.room.autoJoin);
    }

    toggleAutoStart() {
        this.roomEnteredInfo.room.autoStart = this.roomEnteredInfo.room.autoStart == null ? 10 : null;
        this.socket.send('setAutoStart', this.roomEnteredInfo.room.autoStart);
    }

    openTab(tab: Tabs) {
        this.tab = tab;
    }
}
