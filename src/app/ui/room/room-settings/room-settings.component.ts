import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RoomEnteredInfo } from 'src/app/shared/intf';

@Component({
    selector: 'app-room-settings',
    templateUrl: './room-settings.component.html',
    styleUrls: ['./room-settings.component.scss']
})
export class RoomSettingsComponent implements OnInit {

    @Input() roomEnteredInfo: RoomEnteredInfo;
    @Output() start = new EventEmitter();
    @Output() toggleAutoJoin = new EventEmitter();
    @Output() toggleAutoStart = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

}
