import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RuleField } from 'src/app/shared/gameRuleFields';

@Component({
    selector: 'app-choice-input',
    templateUrl: './choice-input.component.html',
    styleUrls: ['./choice-input.component.scss']
})
export class ChoiceInputComponent implements OnInit {

    @Output() submitValue = new EventEmitter();
    @Input() value: number;
    @Input() field: RuleField;
    @Input() editable: boolean;

    constructor() { }

    ngOnInit() {
    }

}
