import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameRuleInputComponent } from './game-rule-input.component';

describe('GameRuleInputComponent', () => {
  let component: GameRuleInputComponent;
  let fixture: ComponentFixture<GameRuleInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameRuleInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameRuleInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
