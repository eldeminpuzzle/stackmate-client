
interface TimePreset {
    name: string;
    desc: string;
    json: string;
}

const timePresets: TimePreset[] = [{
    name: 'Real time',
    desc: 'Play as fast as you want',
    json: '{"turnBased": false}',
}, {
    name: '7-Slow',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":7,"timeInitial":30,"timePerTurn":0,"timePerMove":10,"timeMaxPool":60}',
}, {
    name: '7-Fast',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":7,"timeInitial":30,"timePerTurn":0,"timePerMove":4,"timeMaxPool":60}',
}, {
    name: '7-Blitz',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":7,"timeInitial":30,"timePerTurn":0,"timePerMove":2,"timeMaxPool":60}',
}, {
    name: '7-Bullet',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":7,"timeInitial":30,"timePerTurn":0,"timePerMove":1,"timeMaxPool":60}',
}, {
    name: '7-Extreme',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":7,"timeInitial":30,"timePerTurn":0,"timePerMove":0.5,"timeMaxPool":60}',
}, {
    name: '1-Slow',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":1,"timeInitial":30,"timePerTurn":0,"timePerMove":10,"timeMaxPool":60}',
}, {
    name: '1-Fast',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":1,"timeInitial":30,"timePerTurn":0,"timePerMove":4,"timeMaxPool":60}',
}, {
    name: '1-Blitz',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":1,"timeInitial":30,"timePerTurn":0,"timePerMove":2,"timeMaxPool":60}',
}, {
    name: '1-Bullet',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":1,"timeInitial":30,"timePerTurn":0,"timePerMove":1,"timeMaxPool":60}',
}, {
    name: '1-Extreme',
    desc: 'Fast, 7 pieces per turn',
    json: '{turnBased":true,"piecesPerTurn":1,"timeInitial":30,"timePerTurn":0,"timePerMove":0.5,"timeMaxPool":60}',
}];
