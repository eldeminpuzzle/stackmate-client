import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RuleField, RuleFieldType } from 'src/app/shared/gameRuleFields';

@Component({
    selector: 'app-integer-input',
    templateUrl: './integer-input.component.html',
    styleUrls: ['./integer-input.component.scss']
})
export class IntegerInputComponent implements OnInit {

    @Output() submitValue = new EventEmitter();
    @Input() value: number;
    @Input() field: RuleField;
    @Input() editable: boolean;

    editMode = false;
    editedValue: any = null;

    @ViewChild("input", {static: false}) uiInput: ElementRef<HTMLInputElement>;

    constructor(
        private cd: ChangeDetectorRef
    ) { }

    ngOnInit() {

    }

    onEditClicked() {
        if (this.editable) {
            this.editMode = true;
            this.editedValue = this.value;
    
            setTimeout(() => {
                this.uiInput.nativeElement.focus();
                this.uiInput.nativeElement.select();
            });
        }
    }
    
    onResetClicked() {

    }

    onSubmitValue() {
        if (this.editable) {
            let v: any = NaN;
            if (this.field.type == RuleFieldType.DECIMAL_BOX) {
                v = parseFloat(this.editedValue);
            } else if (this.field.type == RuleFieldType.INTEGER_BOX) {
                v = parseInt(this.editedValue);
            } else if (this.field.type == RuleFieldType.STRING) {
                v = this.editedValue;
            }
            if ((this.field.type == RuleFieldType.STRING || !isNaN(v))
                && !(this.field.minValue != null && v < this.field.minValue)
                && !(this.field.maxValue != null && v > this.field.maxValue)
                && (this.field.validator == null || this.field.validator(v))
            ) {
                this.submitValue.emit(v);
            }
            this.editMode = false;
        }
    }
}
