
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GameRule, SideRule } from 'src/app/shared/gameRule';
import { GameRuleInput, RuleField, RuleFieldDetailLevel, RuleFieldType, sideFields } from 'src/app/shared/gameRuleFields';
import gameRuleUtils from 'src/app/shared/gameRuleUtils';

@Component({
    selector: 'app-game-rule-input',
    templateUrl: './game-rule-input.component.html',
    styleUrls: ['./game-rule-input.component.scss']
})
export class GameRuleInputComponent implements OnInit {

    @Input() editable: boolean;
    @Output() confirmGameRule = new EventEmitter();

    sideFields = sideFields;
    ruleFieldType = RuleFieldType;
    detailLevel = RuleFieldDetailLevel;

    data: GameRuleInput = {
        gameRules: new Map(),
        globalSideRules: new Map(),
        sideRules: [],
    }

    detailLevelChoices = [
        {value: RuleFieldDetailLevel.BASIC,      label: 'basic view'},
        {value: RuleFieldDetailLevel.ADVANCED,   label: 'advanced view'},
        {value: RuleFieldDetailLevel.DETAILED,   label: 'detailed view'},
        // {value: RuleFieldDetailLevel.HIDDEN,     label: 'dev view'},
    ];
    selectedDetailLevel = RuleFieldDetailLevel.BASIC;

    // ui Helper
    sideNrs: number[] = [];
    displaySideNr: number = null;
    hint = this.getRandomHint();

    parseInt = parseInt;

    constructor(
        private cd: ChangeDetectorRef,
    ) { }

    getNumPlayersRequired() {
        return this.data.sideRules.length;
    }

    ngOnInit() {
        this.resetRules();
    }

    resetRules() {
        this.data.gameRules.clear();
        this.data.globalSideRules.clear();
        this.data.sideRules.splice(0);

        for (let i = 0; i < 2; i++) {
            this.data.sideRules.push(new Map());
        }
        
        for (let sideField of sideFields) {
            this.data.globalSideRules.set(sideField.key, sideField.defaultValue);
        }
        
        this.updateUiHelpers();
    }

    updateUiHelpers() {
        this.sideNrs.splice(0);
        for (let i = 0; i < this.data.sideRules.length; i++) {
            this.sideNrs.push(i);
        }
        if (this.displaySideNr != null) {
            this.displaySideNr = Math.min(this.displaySideNr, this.data.sideRules.length);
        }
    }

    setDisplaySideNr(sideNr: number) {
        this.displaySideNr = sideNr;
    }

    updateHint(field: RuleField) {
        if (field != null && field.desc) {
            this.hint = field.path + ': ' + field.desc;
        } else {
            this.hint = '';
            // this.hint = this.getRandomHint();
        }
    }

    getRandomHint() {
        let hints = [
            'Modify the game rule below. Global rules apply to everyone, but each rule can be overridden for specific players.',
            'If the value fails to change, you may have given an unacceptable value.',
            'The number of players in the ruleset must match the number of players in the room for the game to start.',
            'You can use the clipboard or floppy disk icons to save your well-crafted ruleset in JSON format.',
            'If you paste a ruleset in JSON format in the "Paste here..." box, all saved fields will be loaded into the current settings.',
        ];
        return hints[Math.floor(Math.random() * hints.length)];
    }

    getDisplayedSideRule() {
        if (this.displaySideNr == null) {
            return this.data.globalSideRules;
        } else {
            return this.data.sideRules[this.displaySideNr];
        }
    }

    getEffectiveSideRule(key: string, sideNr=this.displaySideNr) {
        let val = null;
        if (sideNr != null) {
            val = this.data.sideRules[sideNr].get(key);
        }
        if (val == null) {
            val = this.data.globalSideRules.get(key);
        }
        return val;
    }

    submitValue(key: string, value: any, sideNr=this.displaySideNr) {
        let sideRule = sideNr == null ? this.data.globalSideRules : this.data.sideRules[sideNr];
        sideRule.set(key, value);
        this.updateGameRule();
    }

    toggleOverride(field: RuleField, override: boolean) {
        if (override) {
            this.submitValue(field.key, field.defaultValue);
        } else {
            this.getDisplayedSideRule().delete(field.key);
            this.updateGameRule();
        }
    }

    updateGameRule() {
        this.confirmGameRule.emit({gameRule: this.getGameRule(), json: gameRuleUtils.getRuleJson(this.data)});
    }
    
    getGameRule() {
        return gameRuleUtils.getGameRule(this.data);
    }

    loadRuleJson(json: string) {
        let gri = gameRuleUtils.loadRuleJson(json);
        if (gri != null) {
            this.data = gri;
            this.updateUiHelpers();
            this.updateGameRule();
        }
    }

    addPlayer() {
        if (this.data.sideRules.length < 20) {
            this.data.sideRules.push(new Map());
            this.sideNrs.push(this.sideNrs.length);
        }
        this.updateGameRule();
    }

    removePlayer() {
        if (this.data.sideRules.length > 1) {
            this.data.sideRules.pop();
            this.sideNrs.pop();
            if (this.displaySideNr != null) {
                this.displaySideNr = Math.min(this.displaySideNr, this.data.sideRules.length);
            }
        }
        this.updateGameRule();
    }

    saveRuleToClipboard() {
        navigator.clipboard.writeText(gameRuleUtils.getRuleJson(this.data));
    }

    saveRuleToFile() {
        this.download("ruleset.json", gameRuleUtils.getRuleJson(this.data));
    }

    loadRule(e: ClipboardEvent) {
        e.preventDefault();
        let gri = gameRuleUtils.loadRuleJson(e.clipboardData.getData('Text'));
        if (gri != null) {
            this.data = gri;
            this.updateUiHelpers();
            this.updateGameRule();
            alert('Rule set loaded :) ');
        } else {
            alert('The rule set data is invalid :( ');
        }
    }

    download(filename: string, text: string) {
        let element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
      
        element.style.display = 'none';
        document.body.appendChild(element);
      
        element.click();
      
        document.body.removeChild(element);
    }

    addSlotDueToAutoJoin() {
        this.data.sideRules.push(new Map());
    }
}
