import { Component, Input, OnInit } from '@angular/core';
import { GameResult } from 'src/app/shared/intf';

interface InterpolateValues {
    start: number;
    end: number;
    value: number;
}

@Component({
    selector: 'app-game-result',
    templateUrl: './game-result.component.html',
    styleUrls: ['./game-result.component.scss']
})
export class GameResultComponent implements OnInit {

    @Input() gameResult: GameResult;

    rating: InterpolateValues;
    coin: InterpolateValues;
    animationStart: number;

    animDur = 2500;
    animDurCoinDelay = 1000;

    constructor() { }

    ngOnInit() {
    }

    showAnimation() {
        if (this.gameResult == null) return;
        
        if (this.gameResult.ratingUpdateInfo != null) {
            let ratingStart = this.gameResult.ratingUpdateInfo.old == null ? 0
                            : this.gameResult.ratingUpdateInfo.old.seasonPoints;
            this.rating = {
                start: ratingStart,
                end: this.gameResult.ratingUpdateInfo.new.seasonPoints,
                value: ratingStart,
            }
        } else {
            this.rating = null;
        }

        if (this.gameResult.coinReward != null) {
            this.coin = {
                start: 0,
                end: this.gameResult.coinReward,
                value: 0,
            }
        } else {
            this.coin = null;
        }

        this.animationStart = Date.now();

        this.animate();
    }

    animate() {
        if (this.rating != null) {
            this.rating.value = this.intpVal(this.intp(), this.rating);
            this.animDurCoinDelay = 1000;
        } else {
            this.animDurCoinDelay = 0;
        }

        if (this.coin != null) {
            this.coin.value = this.intpVal(this.intp(this.animDurCoinDelay), this.coin);
        }

        if (this.intp(this.animDurCoinDelay) < 1) {
            requestAnimationFrame(() => this.animate());
        }
    }

    private intpVal(p: number, val: InterpolateValues) {
        return Math.round(val.start * (1-p) + val.end * p);
    }

    private intp(delay=0) {
        let x = (Date.now() - delay - this.animationStart) / this.animDur;
        return x < 0 ? 0
             : x > 1 ? 1
             : this.easeInOutQuint(x);

    }

    private easeInOutQuint(x: number): number {
        return x < 0.5 ? 16 * x * x * x * x * x : 1 - Math.pow(-2 * x + 2, 5) / 2;
    }

}
