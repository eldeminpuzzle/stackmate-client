import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Session } from 'src/app/endpoint/session';

@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

    @Output() goToSettings = new EventEmitter();
    @Output() goToCredits = new EventEmitter();
    @Output() toggleSound = new EventEmitter();
    @Output() toggleChat = new EventEmitter();
    @Output() exit = new EventEmitter();
    @Input() soundEnabled: boolean;
    
    chatNotification: number = null;

    constructor(
        public session: Session
    ) { }

    ngOnInit() {
    }

    clickControlSettings() {
        this.goToSettings.emit();
    }

    clickExit() {
        this.exit.emit();
    }

    clickChat() {
        this.toggleChat.emit();
    }

    clickSound() {
        this.toggleSound.emit();
    }

    clickCredits() {
        this.goToCredits.emit();
    }

    notifyChat() {
        this.chatNotification = this.chatNotification == null ? 1 : Math.min(9, this.chatNotification + 1);
    }

    clearNotifyChat() {
        this.chatNotification = null;
    }
}
