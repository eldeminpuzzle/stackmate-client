
export class FpsTimer {

    to: any = null;
    raf: number = null;

    now: number;

    inactiveTimeout = 1000;



    constructor(
        private onTick: (dt: number, ct: number) => void,
        private onRender: any,
        private mspf = 1000 / 10,
    ) {

    }

    start() {
        if (this.raf != null) {
            clearTimeout(this.raf);
        }
        this.raf = requestAnimationFrame(this.tickHandler.bind(this));
        this.to = setTimeout(this.tickHandler.bind(this), this.inactiveTimeout);
        this.now = Date.now();
    }

    pause() {
        cancelAnimationFrame(this.raf);
    }

    private resetInactiveTimeout() {
        if (this.to != null) {
            clearTimeout(this.to);
            this.to = setTimeout(this.tickHandler.bind(this), this.inactiveTimeout);
        }
    }

    private tickHandler() {
        const ct = Date.now();
        const dt = (ct - this.now) / 1000;
        this.now = ct;

        this.resetInactiveTimeout();

        
        this.onTick(dt, ct);
        this.onRender();

        this.raf = requestAnimationFrame(this.tickHandler.bind(this));
        
        // if (dt > 0.01) {
        //     console.log(dt, Date.now() - ct);
        // }
    }

}
