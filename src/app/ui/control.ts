
import { Injectable, NgZone } from '@angular/core';
import { IdbManager } from '../service/idbManager';
import { SideRule } from '../shared/gameRule';
import { PlayerEngine } from '../shared/playerEngine';
import { PlayerPixi } from './game/pixi/playerPixi';
import { PlayerViewComponent } from './game/player-view/player-view.component';

class KeyTracker {

	public heldTime = 0;
	public held = false;
	public DAS = 5; // 13
	public ARR = 1; // 5
	public antagonist : KeyTracker = null;
    public key: number;
    public lastArrFail = false;

	constructor(
		public name: string,
		public defaultKey: number,
		public proc: () => boolean,
        public autoRepeatable = false,
        public isSystem = false,
        public ignoreOnInputElements = true,
	) {
		this.key = this.defaultKey;
	}
}

@Injectable({
    providedIn: 'root'
})
export class Control {
    private keyMap = new Map<number, KeyTracker>();
    
    private playerPixi: PlayerPixi;
    private playerEngine: PlayerEngine;
    
    public listenerOnChatPressed = new Set<() => void>();

	public keys = [
		new KeyTracker('Move Left'  , 37    , (() => this.tryMovePiece(0, -1, 0)), true),
		new KeyTracker('Move Right' , 39    , (() => this.tryMovePiece(0, 1, 0)), true),
		new KeyTracker('Soft Drop'  , 40    , (() => {
            let ret = this.tryMovePiece(1, 0, 0);
            if (ret) {
                this.gravityWaitLeft = this.playerEngine.sideRule.gravity <= 0 ? 0 : 1 / this.playerEngine.sideRule.gravity;
            }
            return ret;
        }), true),
		new KeyTracker('Rotate CW'  , 38    , (() => this.tryMovePiece(0, 0, 1))),
		new KeyTracker('Rotate 180' , 88    , (() => this.playerEngine.sideRule.allow180Rotation ? this.tryMovePiece(0, 0, 2) : false)),
		new KeyTracker('Rotate CCW' , 90    , (() => this.tryMovePiece(0, 0, 3))),
		new KeyTracker('Hard Drop'  , 32    , (() => { 
            if (this.playerEngine.isTurn()) {
                this.playerEngine.hardDrop();
                this.resetLockDelays();
            }
            return true;
        })),
        new KeyTracker('Hold'       , 67    , (() => { if (this.playerEngine.hold()) this.resetLockDelays(); return true; })),
        new KeyTracker('Chat'       , 192   , (() => {
            this.listenerOnChatPressed.forEach(l => l());
            this.unpressKeys();
            return true;
        }), false, true, false),
        
        // new KeyTracker('Start'      , 113   , (() => this.playerEngine.newGame()).bind(this), false, true),
        // new KeyTracker('Pause'      , 114   , (() => this.playerEngine.togglePause()).bind(this), false, true),
	];
    
    private gravityWaitLeft: number;
    private lockDelayLeft: number;
    private lockDelayLimitLeft: number;
    private isGravityRunning = false;
    
    constructor(
        private idbManager: IdbManager,
        private ngZone: NgZone,
    ) {
        // default values
        this.keys[2].DAS = 0;
        this.keys[2].ARR = 1;

        this.keys.forEach(key => {
            let proc = key.proc;
            key.proc = () => ngZone.runOutsideAngular(proc);
        });
    }

    private tryMovePiece(dy: number, dx: number, drot: number) {
        let r = this.playerEngine.attemptMove(dy, dx, drot);
        if (r) {
            this.lockDelayLeft = this.playerEngine.sideRule.lockDelay;
            if (drot != 0) {
                this.retryFailArr();
            }
        }
        return r;
    }

    bind(playerPixi: PlayerPixi, playerEngine: PlayerEngine) {
        this.playerPixi = playerPixi;
        this.playerEngine = playerEngine;

        this.playerEngine.listenersOnRoundStarted.add(() => { if (!this.playerEngine.sideRule.turnBased) this.startGravity(); });
        this.playerEngine.listenersOnTurnStarted.add(() => this.startGravity());
        this.playerEngine.listenersOnLockDown.add(() => this.resetLockDelays());
        this.playerEngine.listenersOnTurnEnded.add(() => this.stopGravity());
        this.playerEngine.listenersOnLockOut.add(() => this.stopGravity());
        this.playerEngine.listenersOnRoundEnded.add(() => this.stopGravity());
    }

    unpressKeys() {
        this.keys.forEach(k => this.keyUp(k));
    }

    private procKey(key: KeyTracker) {
        if (this.playerEngine.isRunning() && this.playerEngine.sideData.activePiece != null) {
            key.proc();
        }
    }

    private retryFailArr() {
        for (let i of [0, 1]) { // LEFT & RIGHT KEYS
            if (this.keys[i].lastArrFail) {
                this.procKey(this.keys[i]);
            }
        }
    }

    private startGravity() {
        if (this.playerEngine.sideRule.gravity != 0) {
            this.isGravityRunning = true;
            this.resetLockDelays();
        }
    }

    private resetLockDelays() {
        this.gravityWaitLeft = this.playerEngine.sideRule.gravity <= 0 ? 0 : 1 / this.playerEngine.sideRule.gravity;
        this.lockDelayLeft = this.playerEngine.sideRule.lockDelay;
        this.lockDelayLimitLeft = this.playerEngine.sideRule.lockDelayResetLimit;
    }

    private stopGravity() {
        this.isGravityRunning = false;
    }

	init() {
		this.keys[0].antagonist = this.keys[1];
		this.keys[1].antagonist = this.keys[0];

		for (let key of this.keys) {
			this.keyMap.set(key.defaultKey, key);
		}

		document.addEventListener('keydown', this.onKeyDown.bind(this));

		document.addEventListener('keyup', this.onKeyUp.bind(this));
        
        this.loadControlSettings();
    }

    onKeyDown(e: KeyboardEvent) {
        let keyCode = e.keyCode || e.which;
        let key = this.keyMap.get(keyCode);

        if (this.keys[8] != key) {
            let ev: any = e;
            switch (ev.target.tagName) {  
                case "INPUT": case "SELECT": case "TEXTAREA": return;  
            }
        }

        if (key != null) {
            if (!key.isSystem && this.playerEngine == null) return; // unbound
    
            e.preventDefault();
            e.returnValue = false;
        }

        if (e.repeat) return;
        if (key) {
            this.keyDown(e, key);
        }
    }

    onKeyUp(e: KeyboardEvent) {
        let keyCode = e.keyCode || e.which;
        let key = this.keyMap.get(keyCode);

        if (this.keys[8] != key) {
            let ev: any = e;
            switch (ev.target.tagName) {  
                case "INPUT": case "SELECT": case "TEXTAREA": return;  
            }
        }

        if (key != null) {
            if (!key.isSystem && this.playerEngine == null) return; // unbound
    
            e.preventDefault();
            e.returnValue = false;
        }

        if (key && (key.isSystem || this.playerEngine && this.playerEngine.isRunning())) {
            this.keyUp(key);
        }
    }
    
	keyDown(e: KeyboardEvent, key: KeyTracker) {
        if (key.ignoreOnInputElements && (e as any).target.tagName.toUpperCase() == 'INPUT') {
            return;
        }

        e.preventDefault();
        if (key.isSystem) {
            key.proc();
        } else if ((this.playerEngine && (!key.isSystem && this.playerEngine.isRunning()))) {
            this.procKey(key);
			if (key.autoRepeatable) {
				key.held = true;
				key.heldTime = -key.ARR;
			}
			if (key.antagonist) {
				key.antagonist.held = false;
            }
		}
	}

	keyUp(key: KeyTracker) {
        if (this.playerEngine == null) return; // unbound
        
		if (key.autoRepeatable) {
			key.held = false;
            key.lastArrFail = false;
		}
	}

	loadControlSettings() {
		for (let i = 0; i < this.keys.length; i++) {
			this.idbManager.get('key_'+i, (data: any) => {
				if (data) {
					this.updateKey(i, data);
				}
			});
        }
        
        this.idbManager.get('keyDAS', (data: any) => {
            if (data != null) {
                this.setDasAll(data);
            }
        });
        
        this.idbManager.get('keyARR', (data: any) => {
            if (data != null) {
                this.setArrAll(data);
            }
        });

        this.idbManager.get('keySoftDropARR', (data: any) => {
            if (data != null) { 
                this.setArrSoftDrop(data);
            }
        });

        this.idbManager.get('keySoftDropDAS', (data: any) => {
            if (data != null) {
                this.setDasSoftDrop(data);
            }
        })
	}
	
	updateTick(dt: number) {
		for (let key of this.keyMap.values()) {
			if (key.autoRepeatable && key.held) {
				key.heldTime += dt * 1000;
				if (key.heldTime > key.DAS) {
                    if (key.ARR > 0) {
                        let procResult = key.proc();
                        key.lastArrFail = !procResult;
                        key.heldTime -= key.ARR;
                    } else {
                        while (key.proc()) {}
                    }
				}
			}
        }

        let sideRule: SideRule;
        let grounded: boolean;
        if (this.playerEngine != null && this.playerEngine.sideData.activePiece != null) {
            sideRule = this.playerEngine.sideRule;
            grounded = this.playerEngine.checkActivePieceCollision(1, 0, 0);

            if (sideRule.gravity == -1 && !grounded) { // infinite gravity applies even if it is not your turn
                this.playerEngine.sonicDrop();
            }
        }

        if (this.isGravityRunning && this.playerEngine.sideData.activePiece != null) {
            if (sideRule.gravity > 0) {
                this.gravityWaitLeft -= dt;
            }

            if (grounded) {
                this.lockDelayLeft -= dt;
                this.lockDelayLimitLeft -= dt;

                if (this.lockDelayLeft <= 0 || this.lockDelayLimitLeft <= 0) {
                    this.playerEngine.hardDrop();
                }
            } else {
                if (this.gravityWaitLeft <= 0) {
                    this.playerEngine.attemptMove(1, 0, 0);
                    this.gravityWaitLeft = 1 / sideRule.gravity;
                }
            }
        }
	}

	updateKey(keyId: number, keyCode: number) {
        if (this.keyMap.get(this.keys[keyId].key) === this.keys[keyId]) {
            this.keyMap.delete(this.keys[keyId].key);
        }
		this.keys[keyId].key = keyCode;
		this.keyMap.set(this.keys[keyId].key, this.keys[keyId]);
        this.idbManager.put('key_'+keyId, keyCode);
	}

	setDasAll(das: number) {
        for (let i = 0; i < this.keys.length; i++) {
            if (i != 2) { // exclude soft drop
                this.keys[i].DAS = das;
            }
        }
        this.idbManager.put('keyDAS', das);
    }

    setArrAll(arr: number) {
        for (let i = 0; i < this.keys.length; i++) {
            if (i != 2) { // exclude soft drop
                this.keys[i].ARR = arr;
            }
        }
        this.idbManager.put('keyARR', arr);
    }

    setArrSoftDrop(sdr: number) {
        this.keys[2].ARR = sdr;
        this.idbManager.put('keySoftDropARR', sdr);
    }

    setDasSoftDrop(sdd: number) {
        this.keys[2].DAS = sdd;
        this.idbManager.put('keySoftDropDAS', sdd);
    }
}
