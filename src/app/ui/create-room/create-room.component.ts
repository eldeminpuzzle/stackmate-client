import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'; 
import { PresetInfo } from 'src/app/shared/intf';

export interface CreateRoomUiCmd {
    presetId: number
    custom: boolean;
}

@Component({
    selector: 'app-create-room',
    templateUrl: './create-room.component.html',
    styleUrls: ['./create-room.component.scss']
})
export class CreateRoomComponent implements OnInit {

    @Output() openLeaderboard = new EventEmitter();
    @Output() createRoom = new EventEmitter();
    @Output() refresh = new EventEmitter();

    @Input() presetInfos: PresetInfo[];
    
    refreshEnabled = true; // flag to prevent spamming refresh
    refreshCooldown = 1000;

    custom = true;
    opened = false;
    desc: string;

    bgColors = ['#f668', '#ff68', '#6f68', '#6ff8', '#66f8', '#f6f8'];

    constructor() { }

    ngOnInit() {
        
    }

    onCreateRoom(presetId: number) {
        let uiCmd: CreateRoomUiCmd = {presetId, custom: this.custom};
        this.createRoom.emit(uiCmd);
        this.opened = false;
    }

    onRefresh() {
        if (this.refreshEnabled) {
            this.refresh.emit();
            this.refreshEnabled = false;
            setTimeout(() => {
                this.refreshEnabled = true;
            }, this.refreshCooldown);
        }
    }
}
