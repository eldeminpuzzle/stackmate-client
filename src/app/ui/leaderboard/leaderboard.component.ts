import { Component, EventEmitter, NgZone, OnInit, Output } from '@angular/core';
import { SocketService } from 'src/app/service/socket.service';
import { LeaderboardData, LeaderboardListItem } from 'src/app/shared/intf';
import { Util } from 'src/app/shared/util';

@Component({
    selector: 'app-leaderboard',
    templateUrl: './leaderboard.component.html',
    styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

    @Output() close = new EventEmitter();

    leaderboardList: LeaderboardListItem[] = [];
    data: LeaderboardData = null;
    
    rowsPerPage = 50;

    constructor(
        private socket: SocketService,
        private ngZone: NgZone,
    ) { }

    ngOnInit() {
        Util.autoBind(this, this.socket);
    }

    refresh() {
        this.socket.send('loadLeaderboardList');
    }

    on_loadLeaderboardList(leaderboardList: LeaderboardListItem[]) {
        this.ngZone.run(() => {
            this.leaderboardList = leaderboardList;

            // auto load first (any) leaderboard
            if (this.leaderboardList.length > 0) {
                this.loadLeaderboard(this.leaderboardList[0])
            }
        });
    }

    loadLeaderboard(leaderboard: LeaderboardListItem, season?: number) {
        this.socket.send('loadLeaderboard', leaderboard.id, season != null ? season : leaderboard.maxSeason, 0, this.rowsPerPage);
    }

    on_loadLeaderboard(data: LeaderboardData) {
        this.ngZone.run(() => {
            this.data = data;
        });
    }
}
