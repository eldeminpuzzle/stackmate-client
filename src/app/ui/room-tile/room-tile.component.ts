import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GameRuleInput } from 'src/app/shared/gameRuleFields';
import { RoomInfo } from 'src/app/shared/intf';

@Component({
    selector: 'app-room-tile',
    templateUrl: './room-tile.component.html',
    styleUrls: ['./room-tile.component.scss']
})
export class RoomTileComponent implements OnInit {

    @Input()
    roomInfo: RoomInfo;

    @Input()
    allowJoin: boolean = false;

    @Input()
    allowStart: boolean = false;

    @Input()
    gameRuleInput: GameRuleInput;

    @Output("join")
    joinEventEmitter = new EventEmitter();

    @Output("start")
    startEventEmitter = new EventEmitter();

    @Output("switchSeats")
    switchSeatsEventEmitter = new EventEmitter();

    @Output("transferHost")
    transferHostEventEmitter = new EventEmitter();

    @Output("teamClick")
    teamClickEventEmitter = new EventEmitter();

    @Output("addPlayer")
    addPlayerEventEmitter = new EventEmitter();

    @Output("removePlayer")
    removePlayerEventEmitter = new EventEmitter();

    constructor() {
        
    }

    ngOnInit() {

    }

    onJoinClicked() {
        this.joinEventEmitter.emit();
    }

    onStartClicked() {
        this.startEventEmitter.emit();
    }

    playerSeatClicked(sideNr: number) {
        if (this.roomInfo.players[sideNr] == null) {
            this.switchSeatsEventEmitter.emit(sideNr);
        }
    }

    transferHost(host: number, hostIsSpectator: boolean) {
        this.transferHostEventEmitter.emit({host, hostIsSpectator});
    }

    onTeamClick(e: MouseEvent, slotNr: number) {
        e.stopPropagation();
        e.preventDefault();
        this.teamClickEventEmitter.emit(slotNr);
    }
}
