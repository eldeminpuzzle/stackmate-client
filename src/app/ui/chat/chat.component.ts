import { Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { SocketService } from 'src/app/service/socket.service';
import { Message } from 'src/app/shared/chat/message';
import { RandomGen } from 'src/app/shared/randomGen';
import { Util } from 'src/app/shared/util';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

    @Output() message = new EventEmitter();
    @Output() close = new EventEmitter();

    @ViewChild('chatbox', {static: true}) chatbox: ElementRef<HTMLDivElement>;
    @ViewChild('inp', {static: true}) inp: ElementRef<HTMLInputElement>;

    r = new RandomGen();

    // {
    //     username: 'Welcome!',
    //     message: ':)',
    //     timestamp: Date.now(),
    // }, {
    //     username: 'Miko',
    //     message: this.r.pick([
    //         'Heya, warning, I now mirror this chat to discord <3',
    //         'Hi, beware this chat is visible in discord now <3',
    //         'Howdy, please note this chat is publicly visible <3',
    //     ]),
    //     timestamp: Date.now(),
    // }

    messages: Message[] = [];
    bufferLimit = 40;

    constructor(
        private socket: SocketService,
        private ngZone: NgZone,
    ) { }

    ngOnInit() {
        Util.autoBind(this, this.socket);
    }

    onEnterMessage(e: any) {
        let msg = this.inp.nativeElement.value;
        if (msg) {
            this.socket.send('message', msg);
        }
        this.inp.nativeElement.value = '';
    }

    on_message(message: Message) {
        this.ngZone.run(() => {
            if (this.messages.length > this.bufferLimit) {
                this.messages.shift();
            }
            this.messages.push(message);
            this.message.emit();
        });
    }

    focus() {
        this.inp.nativeElement.focus();
    }
}
