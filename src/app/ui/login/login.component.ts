import { Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { IdbManager } from 'src/app/service/idbManager';
import { SocketService } from 'src/app/service/socket.service';
import { AccountInfo } from 'src/app/shared/intf';
import { Util } from 'src/app/shared/util';
import { TestConfig } from 'src/app/testConfig';
import { Session } from '../../endpoint/session';

enum Subscreens { ENTER_NAME, ENTER_PASSWORD, CHOOSE_ANONYMOUS, REGISTER }

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    @ViewChild("inp", {static: false}) uiInput: ElementRef<HTMLInputElement>;
    @Output("login") loginEventEmitter = new EventEmitter();

    subscreens = Subscreens;
    subscreen = Subscreens.ENTER_NAME;

    username: string;
    password: string;
    rememberPassword = false;

    randomNames = ['Albatross','Alligator','Anteater','Antelope','Ape','Armadillo','Donkey','Baboon','Badger','Barracuda','Bat','Bear','Beaver','Bee','Bison','Bluebird','Boar','Buffalo','Butterfly','Camel','Capybara','Caribou','Cassowary','Cat','Caterpillar','Cheetah','Chicken','Chimpanzee','Chinchilla','Cobra','Cougar','Coyote','Crab','Cricket','Crocodile','Crow','Deer','Dog','Dolphin','Donkey','Dragonfly','Duck','Dugong','Eagle','Echidna','Eel','Elephant','Elk','Falcon','Flamingo','Fox','Frog','Gazelle','Gecko','Giraffe','Goat','Goose','Gorilla','Grasshopper','Hamster','Hawk','Hedgehog','Hippopotamus','Hornet','Horse','Hyena','Iguana','Jaguar','Jellyfish','Kangaroo','Koala','Komodo','Lemur','Leopard','Lion','Lizard','Lynx','Mammoth','Meerkat','Mole','Mongoose','Mouse','Nightingale','Ocelot','Octopus','Orangutan','Ostrich','Otter','Ox','Owl','Oyster','Panther','Parrot','Panda','Penguin','Rabbit','Raccoon','Raven','Reindeer','Rhinoceros','Salamander','Seahorse','Seal','Shark','Snake','Spider','Squirrel','Swan','Tiger','Toucan','Viper','Weasel','Wolf','Zebra'];

    inpData = this.randomNames[Math.floor(Math.random() * this.randomNames.length)];

    itemNo = 0;
    version = 'v2021.01.18';
    historyList = [
        'v2021.02.14: Complete graphics rewrite for better performance.',
        'v2021.02.08: Performance improvements, event-based controls',
        'v2021.02.07: Database Update - useless coins, server rooms & experimental rating system, Lobby GUI revamp',
        'v2021.01.24: Installed database. Accounts login/registration.',
        'v2021.01.22: Garbage Genesis Update - configs for garbage holes, edge, locks.',
        'v2021.01.18: Rule presets & create room GUI. See and adjust teams on the player list in room. Separate soft drop rate.',
        'v2021.01.16: Line clear & garbage entry animation. Combo timer & garbage fixed-rate spawn (inspired by cultris).',
        'v2021.01.10: Sound effects, random & memory piece generator.',
        'v2021.01.09: Dynamic game layout, teams mode, in game chat, targeting modes, receive multiplier.',
        'v2021.01.06: Everyone can now see the rule set. Implemented Attack multiplier & seed matching. GUI improvements + logo.',
        'v2021.01.05: Experimental non-turn-based mode. Wrapper for reliable / timeout detection in socket communications. Hopefully fixes desync/disconnection.',
        'v2021.01.04: Support for polyomino. Enables switching slots / spectator mode in room + new room GUI layout.',
        'v2020.01.03: Gravity, multi/spin/b2b attack tables.',
        'v2020.01.02: SRS, bag random, T-Spin & immobile detection, PC/CC bonus.',
        'v2020.01.01: Bare bones playable turn-based stacking game.'
    ]
    historyDisplay: string[] = [];

    msgs = [
        'Even-sized boards can actually make it harder to get perfect clears due to garbage lines.',
        'This game used to be turn-based only. We should rename it...',
        'Combo timer in this game is inspired by Cultris. Check it out at gewaltig.net.',
        'The "Least uncleared" targetting mode is recommended for team mode, but not for free for all.',
        'Use "Attack multiplier" and "Defense multiplier" to balance the game between players of different skill levels.',
        'If the "Nearest" rotation system is selected, tetrominoes will move according to the same logic as all other polyominoes.',
    ];
    msg: string;

    constructor(
        private socket: SocketService,
        private session: Session,
        private ngZone: NgZone,
        private idbManager: IdbManager,
    ) {
        Util.autoBind(this, socket);
    }

    setRandomName() {
        this.inpData = this.randomNames[Math.floor(Math.random() * this.randomNames.length)]
    }
    
    reset() {
        this.subscreen = Subscreens.ENTER_NAME;
        this.password = null;
        this.inpData = this.username;
        if (this.username == null) {
            this.setRandomName();
        }
    }

    raiseItemNo(source: any) {
        if (this.itemNo < this.historyList.length) {
            let targetItemNo = this.itemNo == 0 ? Math.min(this.historyList.length, 3) : this.itemNo + 1;
            while (this.itemNo < targetItemNo) {
                this.historyDisplay.push(this.historyList[this.itemNo])
                this.itemNo++;
            }
        }
    }

    ngOnInit() {
        this.randomMessage();
        setTimeout(() => {
            this.uiInput.nativeElement.focus();
            this.uiInput.nativeElement.select();
        });

        this.idbManager.get('username', (u: string) => {
            this.idbManager.get('password', (p: string) => {
                if (u != null && p != null) {
                    this.username = u;
                    this.inpData = u;
                    this.rememberPassword = true;
                } else {
                    this.rememberPassword = false;
                }
            });
        });
    }

    randomMessage() {
        this.msg = this.msgs[Math.floor(Math.random() * this.msgs.length)];
    }

    on_login(account: AccountInfo) {
        this.ngZone.run(() => {
            this.session.account = account;
            this.loginEventEmitter.emit(account);

            if (!account.anonymous && this.rememberPassword) {
                this.idbManager.put('username', account.name);
                this.idbManager.put('password', this.password);
            } else {
                this.idbManager.put('username', null);
                this.idbManager.put('password', null);
            }
        });
    }

    onLoaded() {
        if (TestConfig.autoLogin) {
            // this.onSubmit();
        }
    }


    onSubmit() {
        let reserved = false;
        this.randomNames.forEach(n => {
            if (n.toUpperCase() === this.inpData.toUpperCase()) {
                reserved = true;
            }
        })
        if (reserved) {
            this.socket.send('login', this.inpData, null);
        } else {
            this.socket.send("checkAccountExists", this.inpData);
        }
        // TODO: disable input
    }

    on_checkAccountExists(username: string, exists: boolean) {
        this.ngZone.run(() => {
            this.username = username;
            if (exists) {
                this.inpData = '';
                this.subscreen = Subscreens.ENTER_PASSWORD;

                // auto fill password if rememberPassword
                this.idbManager.get('username', (u: string) => {
                    this.idbManager.get('password', (p: string) => {
                        if (u!= null && username.toLowerCase() == u.toLowerCase() && this.rememberPassword) {
                            this.inpData = p;
                            this.onSubmitPassword();
                            this.rememberPassword = true;
                        } else {
                            this.rememberPassword = false;
                        }
                    });
                });
            } else {
                this.inpData = '';
                this.subscreen = Subscreens.CHOOSE_ANONYMOUS;
            }
        });
    }

    onCancel() {
        this.username = null;
        this.subscreen = Subscreens.ENTER_NAME;
    }

    onResetPassword() {
        alert('Uh... so you clicked the reset password button :) . But if you really lost your password, please just contact Kevin#2642 on discord.');
    }

    onSubmitPassword() {
        this.socket.send('login', this.username, this.inpData);
        this.password = this.inpData;
        // TODO: disable input
    }

    on_wrongPassword(username: string) {
        this.ngZone.run(() => {
            this.inpData = null;
        });
    }

    onSkipCreation() {
        this.socket.send('login', this.username);
        // TODO: disable input
    }

    onCreateAccount() {
        this.subscreen = Subscreens.REGISTER;
    }

    onCreatePassword() {
        this.socket.send('register', this.username, this.inpData);
    }

    on_registered(username: string) {
        this.ngZone.run(() => {
            this.subscreen = Subscreens.ENTER_PASSWORD;
            this.inpData = '';
        });
    }

    on_registrationFailed(username: string) {
        // TODO: show error
        this.ngZone.run(() => {
            this.subscreen = Subscreens.ENTER_NAME;
        });
    }

}
