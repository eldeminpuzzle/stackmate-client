import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-credits',
    templateUrl: './credits.component.html',
    styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {

    @Output() close = new EventEmitter();

    sounds = [{
        usage: 'Hard drop',
        source: 'https://freesound.org/people/Kekskiller/sounds/87097/',
    }, {
        usage: 'Line clear',
        source: 'https://freesound.org/people/cablique/sounds/152767/',
    }, {
        usage: 'Multi-clear',
        source: 'https://freesound.org/people/Bertsz/sounds/500912/',
    }, {
        usage: 'Strong clear',
        source: 'https://freesound.org/people/vabadus/sounds/151076/',
    }, {
        usage: 'Rotate A',
        source: 'https://freesound.org/people/junggle/sounds/28849/',
    }, {
        usage: 'Rotate B',
        source: 'https://freesound.org/people/Erokia/sounds/470377/',
    }, {
        usage: 'Rotate C', // kick
        source: 'https://freesound.org/people/acollier123/sounds/122671/',
    }, {
        usage: 'Rotate D', // stuck
        source: 'https://freesound.org/people/BMacZero/sounds/96149/',
    }, {
        usage: 'Garbage*', // receive one/two/many/spike
        source: 'https://freesound.org/people/Timbre/sounds/403913/',
    }, {
        usage: 'Death*',
        source: 'https://freesound.org/people/Kinoton/sounds/427823/',
    }, {
        usage: 'Board Clear A',
        source: 'https://freesound.org/people/Loyalty_Freak_Music/sounds/407479/',
    }, {
        usage: 'Board Clear B',
        source: 'https://freesound.org/people/GameAudio/sounds/220184/',
    }, {
        usage: 'Combo',
        source: 'https://freesound.org/people/Wagna/sounds/325805/',
    }, {
        usage: 'T-Spin',
        source: 'https://freesound.org/people/Caitlin_100/sounds/365482/',
    }, {
        usage: 'Immobile',
        source: 'https://freesound.org/people/Robinhood76/sounds/466425/',
    }];

    constructor() { }

    ngOnInit() {
    }
}
