import { ChangeDetectorRef, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { Util } from 'src/app/shared/util';
import { StartGameData } from 'src/app/shared/gameIntf';
import { AccountInfo, PresetInfo, RoomEnteredInfo } from 'src/app/shared/intf';
import { GameComponent } from 'src/app/ui/game/game.component';
import { LobbyComponent } from 'src/app/ui/lobby/lobby.component';
import { LoginComponent } from 'src/app/ui/login/login.component';
import { RoomComponent } from 'src/app/ui/room/room.component';
import { Control } from '../control';
import { MenuControlComponent } from '../menu-control/menu-control.component';
import { SocketService } from 'src/app/service/socket.service';
import { ChatComponent } from '../chat/chat.component';
import { SoundService } from 'src/app/data/sound.service';
import { SideMenuComponent } from '../side-menu/side-menu.component';
import { Session } from 'src/app/endpoint/session';
import { LeaderboardComponent } from '../leaderboard/leaderboard.component';

enum Screen { LOGIN, LOBBY, ROOM, GAME, SETTINGS, CREDITS, LEADERBOARD }
const tempScreens = [ Screen.SETTINGS, Screen.CREDITS, Screen.LEADERBOARD ];

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    screenEnum = Screen;
    screen: Screen;
    lastScreen: Screen = Screen.LOGIN;
    roomEnteredInfo: RoomEnteredInfo;
    presetInfos: PresetInfo[];

    @ViewChild("login", {static: true}) login: LoginComponent;
    @ViewChild("lobby", {static: true}) lobby: LobbyComponent;
    @ViewChild("room", {static: true}) room: RoomComponent;
    @ViewChild("game", {static: true}) game: GameComponent;
    @ViewChild("settings", {static: true}) settings: MenuControlComponent;
    @ViewChild('chat', {static: true}) chatBox: ChatComponent;
    @ViewChild('sideMenu', {static: true}) sideMenu: SideMenuComponent;
    @ViewChild('leaderboard', {static: true}) leaderboard: LeaderboardComponent;

    showChatBox = false;

    constructor(
        private socket: SocketService,
        private control: Control,
        private cd: ChangeDetectorRef,
        public soundService: SoundService,
        private session: Session,
        private ngZone: NgZone,
    ) {
        Util.autoBind(this, socket);
    }

    ngOnInit() {
        this.screen = Screen.LOGIN;
        this.login.onLoaded();

        this.control.listenerOnChatPressed.add(() => {
            this.toggleChatbox();
        });

        // this.toggleSound();
    }

    on_presetInfos(presetInfos: PresetInfo[]) {
        this.presetInfos = presetInfos;
    }

    onLoggedIn() {
        this.ngZone.run(() => {
            this.screen = Screen.LOBBY;
            this.lobby.enterLobby();
        });
    }

    on_roomEntered(info: RoomEnteredInfo) {
        this.ngZone.run(() => {
            this.roomEnteredInfo = info;
            this.screen = Screen.ROOM;
    
            this.room.enterRoom(info);
            this.cd.markForCheck();
        });
    }

    on_gameEntered(startGameData: StartGameData) {
        this.ngZone.run(() => {
            this.screen = Screen.GAME;
    
            this.game.startGame(startGameData);
        });
    }

    onExit() {
        if (this.screen == Screen.GAME || this.screen == Screen.ROOM) {
            this.socket.send('exitRoom');
            this.screen = Screen.LOBBY;
            this.lobby.refreshRoomList();
        } else {
            this.socket.send('logout');
            this.screen = Screen.LOGIN;
            this.login.reset();
        }
    }

    onExitGame() {
        if (this.screen == Screen.GAME) {
            this.ngZone.run(() => {
                this.screen = Screen.ROOM;
                this.room.showGameResult();
            });
        }
    }

    private goToScreen(screen: Screen): boolean {
        if (this.screen != screen && tempScreens.indexOf(this.screen) == -1) {
            this.lastScreen = this.screen;
        }

        if (tempScreens.indexOf(screen) == -1) {
            this.screen = screen;
            return true;
        } else if (this.screen == screen) {
            this.screen = this.lastScreen;
            return false;
        } else {
            this.screen = screen;
            return true;
        }
    }

    onCloseScreen() {
        this.screen = this.lastScreen;
    }

    onGoToSettings() {
        if (this.goToScreen(Screen.SETTINGS)) {
            this.settings.loadSettings();
        }
    }

    onGoToCredits() {
        this.goToScreen(Screen.CREDITS);
    }

    onGoToLeaderboard() {
        if (this.goToScreen(Screen.LEADERBOARD)) {
            this.leaderboard.refresh();
        }
    }

    toggleChatbox() {
        this.showChatBox = !this.showChatBox;
        if (this.showChatBox) {
            this.chatBox.focus();
            this.sideMenu.clearNotifyChat();
        } else {
            // Give the document focus
            window.focus();
            
            // Remove focus from any focused element
            if (document.activeElement) {
                (document.activeElement as any).blur();
            }
        }
    }

    toggleSound() {
        this.soundService.toggle();
    }
}
