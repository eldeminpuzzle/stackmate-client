import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-coins',
    templateUrl: './coins.component.html',
    styleUrls: ['./coins.component.scss']
})
export class CoinsComponent implements OnInit {

    @Input() amount: number;

    constructor() { }

    ngOnInit() {
    }

}
