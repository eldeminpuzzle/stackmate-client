import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-toggle-text',
    templateUrl: './toggle-text.component.html',
    styleUrls: ['./toggle-text.component.scss']
})
export class ToggleTextComponent implements OnInit {

    @Output() change = new EventEmitter();
    @Input() initialValue: number;
    @Input() choices: { value: any, label: string }[];
    selected = 0;

    constructor() { }

    ngOnInit() {
        this.selected = this.initialValue;
    }

    handleClick() {
        this.selected = (this.selected + 1) % this.choices.length;
        this.change.emit(this.choices[this.selected].value);
    }
}
