import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/data/image.service';
import { IdbManager } from 'src/app/service/idbManager';
import { Control } from '../control';

@Component({
    selector: 'app-preloader',
    templateUrl: './preloader.component.html',
    styleUrls: ['./preloader.component.scss']
})
export class PreloaderComponent implements OnInit {

    loaded = false;
    msg = 'Loading...';

    constructor(
        private idbManager: IdbManager,
        private imageService: ImageService,
        private control: Control,
    ) { }

    ngOnInit() {
        this.idbManager.asyncInit(() => {
            this.msg += '\n IDB initialized';
            this.imageService.load();
            this.imageService.setOnLoad(() => {
                this.msg += '\n Images loaged';
                this.control.init();
                this.loaded = true;
            });
        });
    }

}
