
export class TestConfig {
    public static autoLogin = false;
    public static autoRoom = false;
    public static autoStartGame = false;
    public static renderSmall = false;
    public static useQuickTestRuleSet = false;

    // public static autoLogin = true;
    // public static autoRoom = false;
    // public static autoStartGame = false;
    // public static renderSmall = false;
    // public static useQuickTestRuleSet = false;
}
