
export class Util {
    static autoBind(object: any, socket: any) {
        let proto: any = Reflect.getPrototypeOf(object);
        for (let key of Reflect.ownKeys(proto)) {
            let keyString: string = key.toString();
            if (keyString.startsWith('on_')) {
                socket.on(keyString.slice(3), object[keyString].bind(object));
            }
        }
    }
}
