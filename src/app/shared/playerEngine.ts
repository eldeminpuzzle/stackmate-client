import { GameData, GarbageData, SideData, SideStats, Tile } from "./gameData";
import { AppendPreviewCommand, Command, Commands, DieCommand, PlacePieceCommand, SetTimeCommand, SpawnGarbageCommand, SubMoveCommand, TileType, TimeOutCommand } from "./gameIntf";
import { ComboMode, GarbageBlockingMode, HoldMode, LockComboMode, PieceGenerationMode, RotationMode, SideRule } from "./gameRule";
import { MatUtil } from "./matUtil";
import { ClosestRotationSystem, NoKickRotationSystem, RotationSystem, SuperRotationSystem } from "./rotation/rotation";
import { BagPieceGen, createPiece, MemoryPieceGen, PieceGen, RandomPieceGen } from "./pieceGen";
import { AttackRule } from "./attackRule";
import { GarbageQueue } from "./garbageQueue";
import { GameStatsController } from "./gameStats";
import { RandomGen } from "./randomGen";
import { GarbageGen } from "./garbageGen";

export interface ClearInfo { linesCleared: number[], garbageLineCount: number };

export interface MoveResult {
    pieceSize: number;
    pieceId: number;
    clearedLines: number;
    clearedGarbageLines: number;
    multiClearValue: number;

    isPerfectClear: boolean;
    isColorClear: boolean;
    // isPieceClear: boolean;
    combo: number; // first clear: combo = 1
    countsAsCombo: number; // -1 = combo break. 0 = maintain combo. 1 = increase combo.
    isImmobileSpin: boolean;
    isThreeCornersSpin: boolean;
    isMiniTSpin: boolean;

    // intermediate
    attacksUsedForBlocking?: number;
    attackPower?: number[];
    backToBack?: number; // first spin: backToBack = 1
}

export interface SubmoveInfo {
    y: number;
    x: number;
    rot: number;
    hold: boolean;
    ackFixRateSpawnGarbage: number;
    ackComboTimerBreak: boolean;
}

export interface MoveInfo {
    y: number;
    x: number;
    rot: number;
    hold: boolean;
    isTripleSpin: boolean;
    isLastMoveSpin: boolean;
    ackSpawnGarbage: number; // only if NTB
    ackFixRateSpawnGarbage: number; // only if NTB
    ackBlockGarbage: number; // only if NTB
    ackComboTimerBreak: boolean; // only if NTB - true if timer ran out before the lock down
}

export class PlayerEngine {

    rotSys: RotationSystem; 
    attackRule: AttackRule;
    garbageQueue: GarbageQueue;
    statsController: GameStatsController;

    pieceGen: PieceGen;
    garbageGen: GarbageGen;

    executor = new Map<Commands, any>();

    public listenersOnCommandExecuted = new Set<any>();
    public listenersOnLockDown = new Set<(m: MoveInfo, r: MoveResult) => any>();
    public listenersOnLockOut = new Set<() => any>();
    public listenersOnTimeChanged = new Set<(dt: number) => any>();
    public listenersOnTurnStarted = new Set<() => any>();
    public listenersOnRoundStarted = new Set<() => any>();
    public listenersOnRoundEnded = new Set<() => any>();
    public listenersOnTurnEnded = new Set<() => any>();
    public listenersOnPieceMoved = new Set<(dy: number, dx: number, drot: number, kick: boolean) => any>();
    public listenersOnGarbageReceived = new Set<(cmd: SpawnGarbageCommand) => any>();
    public listenersOnLinesCleared = new Set<(clearInfo: ClearInfo) => any>();
    
    public listenersOnLocalHold = new Set<() => any>();
    public listenersOnPieceMoveFailed = new Set<(dy: number, dx: number, drot: number) => any>();
    public listenersOnGarbageEntered = new Set<(numLines: number, isFixRate: boolean) => any>();

    // pixi only (server side wasted?)
    public listenersOnMinoUpdated = new Set<(pos: {x: number, y: number}[]) => any>();

    public sideData: SideData;

    // data only useful for local player, hence not in side data
    private lastMoveIsSpin: boolean;
    private lastMoveIsTripleSpin: boolean; // Useful for detecting T-Spin minis.

    comboEnd: number; // if combo timer is ON, combo breaks when this time is reached (ack for remote)
    private lastGarbageFixSpawn: number = 0;
    private fixRateAck = 0;
    
    private r: RandomGen;

    constructor(
        public gameData: GameData,
        public sideRule: SideRule,
        public sideNr: number,
        public sideStats: SideStats,
        public isLocal: boolean,
    ) {
        this.sideData = {
            isAlive: true,
            activePiece: null,
            hold: null,
            board: this.createEmptyBoard(),
            previews: [],
            piecesLeft: 0,
            holdUsed: false,
            garbageQueue: [],
            time: sideRule.timeInitial,
            turnStart: null,
            combo: 0,
            backToBack: 0,
            attackMultiplierSurplus: 0,

            visibleGarbage: 0,
        };

        this.rotSys = sideRule.rotationMode == RotationMode.NO_KICK ? new NoKickRotationSystem()
                    : sideRule.rotationMode == RotationMode.NEAREST ? new ClosestRotationSystem()
                    : new SuperRotationSystem();
        this.attackRule = new AttackRule(this.sideRule, this.sideData);
        this.garbageQueue = new GarbageQueue(this.sideData);
        this.statsController = new GameStatsController(this);

        this.executor.set(Commands.APPEND_PREVIEW, this.cmd_appendPreview.bind(this));
        this.executor.set(Commands.FILL_HOLD, this.cmd_fillHold.bind(this));
        this.executor.set(Commands.PLACE_PIECE, this.cmd_placePiece.bind(this));
        this.executor.set(Commands.SUB_MOVE, this.cmd_subMove.bind(this));
        this.executor.set(Commands.SPAWN_GARBAGE, this.cmd_spawnGarbage.bind(this));
        this.executor.set(Commands.SET_TIME, this.cmd_setTime.bind(this));
        this.executor.set(Commands.TIME_OUT, this.cmd_timeOut.bind(this));
        this.executor.set(Commands.DIE, this.cmd_die.bind(this));
    }

    getProgressToNextFixRateGarbage(ct: number) {
        return this.sideRule.garbageSpawnFixRate > 0 ? Math.min(1, (ct - this.lastGarbageFixSpawn) / (1000 / this.sideRule.garbageSpawnFixRate)) : 1;
    }

    tick(dt: number, ct: number) {
        // spawn garbage fix rate
        if (this.isLocal) {
            let spawnInterval = 1000 / this.sideRule.garbageSpawnFixRate;
            if (!this.garbageQueue.nextIsReady()) {
                this.lastGarbageFixSpawn = Math.max(this.lastGarbageFixSpawn, ct - spawnInterval);
            } else if (ct - this.lastGarbageFixSpawn >= spawnInterval) {
                this.lastGarbageFixSpawn += spawnInterval;
                this.advanceGarbageQueue(1, true, true);
                this.fixRateAck++;
            }
        }
    }

    isTurn() {
        return this.sideData.isAlive && (!this.sideRule.turnBased || this.gameData.turn == this.sideNr);
    }

    addTime(seconds: number) {
        this.sideData.time = Math.min(this.sideRule.timeMaxPool, this.sideData.time + seconds);
        this.listenersOnTimeChanged.forEach(l => l(seconds));
    }

    shouldClockRun() {
        return this.sideNr == this.gameData.turn;
    }

    isRunning() {
        return this.gameData.turn != null;
    }

    startRound(seed: number) {
        this.r = new RandomGen(seed);
        this.sideData.isAlive = true;
        this.sideData.activePiece = null;
        this.sideData.hold = null;
        for (let i = 0; i < this.sideRule.height; i++) {
            for (let j = 0; j < this.sideRule.width; j++) {
                this.sideData.board[i][j] = null;
            }
        }
        this.sideData.previews.length = 0;
        this.sideData.piecesLeft = 0;
        this.sideData.holdUsed = false;
        this.sideData.garbageQueue.length = 0;

        this.pieceGen =
            this.sideRule.pieceGenerationMode == PieceGenerationMode.RANDOM ? new RandomPieceGen(this.r, this.sideRule.pieceList)
            : this.sideRule.pieceGenerationMode == PieceGenerationMode.BAG ? new BagPieceGen(this.r, this.sideRule.pieceList)
            : this.sideRule.pieceGenerationMode == PieceGenerationMode.MEMORY ? new MemoryPieceGen(this.r, this.sideRule.pieceList, this.sideRule.pieceGenerationMemSize)
            : null;
        
        this.garbageGen = new GarbageGen(this.sideRule, this.sideData, this.r);

        // fill active piece, hold, preview
        if (this.sideRule.holdMode == HoldMode.INFINITE) {
            this.sideData.hold = this.pieceGen.next();
        }
        for (let i = 0; i < this.sideRule.numPreviews; i++) {
            this.sideData.previews.push(this.pieceGen.next());
        }
        this.nextPiece();

        this.listenersOnRoundStarted.forEach(l => l());
    }

    createEmptyBoard(): Tile[][] {
        let ret: Tile[][] = new Array(this.sideRule.height);
        for (let i = 0; i < this.sideRule.height; i++) {
            ret[i] = new Array(this.sideRule.width);
        }
        return ret;
    }

    startTurn() {
        this.addTime(this.sideRule.timePerTurn);
        this.sideData.piecesLeft = this.sideRule.piecesPerTurn;
        this.sideData.turnStart = Date.now();
        this.listenersOnTurnStarted.forEach(l => l());
    }
    
    attemptMove(dy: number, dx: number, drot: number, ignorePieceMoveListeners=false): boolean {
        if (!this.checkActivePieceCollision(dy, dx, drot)) {
            this.doMove(dy, dx, drot)
            if (drot != 0) {
                // this.soundManager.play(s.rotate);
            }
            this.lastMoveIsSpin = drot != 0;
            this.lastMoveIsTripleSpin = false;
            if (!ignorePieceMoveListeners) {
                this.listenersOnPieceMoved.forEach(l => l(dy, dx, drot, false));
            }
            return true;
        } else if (drot != 0) {
            // attempt kicks
            for (let kick of this.rotSys.getKickTable(this.sideData.activePiece, drot)) {
                if (!this.checkActivePieceCollision(dy + kick[0], dx + kick[1], drot)) {
                    this.doMove(dy + kick[0], dx + kick[1], drot);
                    this.lastMoveIsSpin = drot != 0;
                    this.lastMoveIsTripleSpin = kick.length > 2;
                    if (!ignorePieceMoveListeners) {
                        this.listenersOnPieceMoved.forEach(l => l(dy, dx, drot, false));
                    }
                    return true;
                }
            }
        }
        this.listenersOnPieceMoveFailed.forEach(l => l(dy, dx, drot));
        return false;
    }

    doMove(dy: number, dx: number, drot: number) {
        this.sideData.activePiece.y += dy;
        this.sideData.activePiece.x += dx;
        this.sideData.activePiece.rot = (this.sideData.activePiece.rot + 4 + drot) % 4;
        this.sideData.activePiece.isLastMoveRotation = (drot % 4) != 0;

        for (let i = 0; i < drot; i++) {
            this.sideData.activePiece.piece.tiles = MatUtil.rotate(this.sideData.activePiece.piece.tiles);
        }

        this.lastMoveIsTripleSpin = false;
        this.lastMoveIsSpin = false;
    }

    hardDrop() {
        this.sonicDrop();

        if (this.sideData.piecesLeft > 0 || !this.sideRule.turnBased) {
            this.lockDown();
        }
    }

    sonicDrop() {
        let dy = 0;
        while (this.attemptMove(1, 0, 0, true)) {
            dy++;
        }
        if (dy > 0) {
            this.listenersOnPieceMoved.forEach(l => l(dy, 0, 0, false));
        }
    }

    createSubMove(): SubmoveInfo {
        let ap = this.sideData.activePiece;
        let fra = this.fixRateAck;
        // this.fixRateAck = 0; // TODO: enable this when spawning fix rate is implemented on SubMove
        return {
            x: ap.x,
            y: ap.y,
            rot: ap.rot,
            hold: this.sideData.holdUsed,
            ackFixRateSpawnGarbage: fra,
            ackComboTimerBreak: Date.now() >= this.comboEnd,
        };
    }

    lockDown(ackSpawnGarbage: number=null, ackFixRateSpawnGarbage: number=null, ackBlockGarbage: number=null, ackComboTimerBreak: boolean=null): MoveResult {
        const ap = this.sideData.activePiece;
        const tiles = ap.piece.tiles;
        let pieceSize = ap.piece.size;
        let pieceId = ap.piece.id;
        let x = ap.x;
        let y = ap.y;
        let rot = ap.rot;

        
        // spawn remote fix rate garbage (before placing piece. Other garbage are spawned after placing piece)
        if (!this.sideRule.turnBased && !this.isLocal && ackFixRateSpawnGarbage != null && ackFixRateSpawnGarbage > 0) {
            this.advanceGarbageQueue(ackFixRateSpawnGarbage, true, true);
        }


        // detect spin bonuses
        let isImmobileSpin = this.isImmobile();
        let { isThreeCornersSpin, isMiniTSpin } = this.checkThreeCornersSpin();

        
        let pixiMinoUpdate: {x: number, y: number}[] = []; // bombs + place piece, coordinates before clearing lines

        // detonate bombs
        let detonated: Set<number> = new Set();
        for (let j = 0; j < tiles[0].length; j++) {
            for (let i = tiles.length - 1; i >= 0; i--) {
                if (tiles[i][j] != null) {
                    const tx = ap.x + j;
                    for (let k = 1; true; k++) {
                        const ty = ap.y + i + k;
                        if (ty < this.sideData.board.length && this.sideData.board[ty][tx] != null && this.sideData.board[ty][tx].type == TileType.LOCK) {
                            this.sideData.board[ty][tx].nonClearing = false;
                            this.sideData.board[ty][tx].colorId = 7;
                            detonated.add(ty);
                            if (this.listenersOnMinoUpdated.size > 0) {
                                pixiMinoUpdate.push({x: tx, y: ty});
                            }
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
        }

        // place piece on the board
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j] != null) {
                    const ty = ap.y + i;
                    const tx = ap.x + j;
                    this.sideData.board[ty][tx] = tiles[i][j];
                    
                    if (this.listenersOnMinoUpdated.size > 0) {
                        pixiMinoUpdate.push({x: tx, y: ty});
                    }
                }
            }
        }

        this.listenersOnMinoUpdated.forEach(l => l(pixiMinoUpdate));

        // clear lines and count combo
        let comboTimerWasBroken = false;
        let checkClearResult = this.checkClear(detonated);
        this.clear(checkClearResult);
        if ((this.isLocal && this.sideRule.comboMode == ComboMode.TIMER && this.comboEnd != null && Date.now() > this.comboEnd)
            || (!this.isLocal && ackComboTimerBreak)) {
            // comboTimer runs out
            this.sideData.combo = 0;
            comboTimerWasBroken = true;
        }
        let clearOrDetonate = detonated.size > 0 || checkClearResult.linesCleared.length > 0;
        let nonLockClear = checkClearResult.multiClearValue > 0;
        let countsAsCombo = 
            this.sideRule.locksComboMode == LockComboMode.DOES_NOT_COUNT ? (nonLockClear ? 1 : -1)
            : this.sideRule.locksComboMode == LockComboMode.MAINTAINS_COMBO ? (nonLockClear ? 1 : clearOrDetonate ? 0 : -1)
            : this.sideRule.locksComboMode == LockComboMode.INCREASES_COMBO ? (clearOrDetonate ? 1 : -1)
            : null;
        if (countsAsCombo == null) throw new Error('combo error');
        if (this.sideRule.comboMode != ComboMode.NONE) {
            if (countsAsCombo == -1) {
                if (this.sideRule.comboMode == ComboMode.CONSECUTIVE_CLEARS) {
                    this.sideData.combo = 0;
                }
            } else if (countsAsCombo == 0) {

            } else if (countsAsCombo == 1) {
                if (this.sideData.combo == 0 && this.sideRule.comboMode == ComboMode.TIMER) {
                    this.comboEnd = Date.now() + this.sideRule.comboTimerInitial * 1000;
                }
                this.sideData.combo++;
            }
        }

        // check perfect clear
        let isPerfectClear = true;
        let isColorClear = true;
        for (let i = 0; i < this.sideRule.height; i++) {
            for (let j = 0; j < this.sideRule.width; j++) {
                if (this.sideData.board[i][j] != null) {
                    isPerfectClear = false;
                    if (!this.sideData.board[i][j].isGarbage) {
                        isColorClear = false;
                    }
                }
                if (!isPerfectClear && !isColorClear) break;
            }
            if (!isPerfectClear && !isColorClear) break;
        }


        // time bonus per move
        this.addTime(this.sideRule.timePerMove);


        // intermediate moveResult
        let moveResult: MoveResult = {
            pieceSize,
            pieceId,
            clearedLines: checkClearResult.linesCleared.length,
            clearedGarbageLines: checkClearResult.garbageLineCount,
            multiClearValue: checkClearResult.multiClearValue,
            isPerfectClear,
            isColorClear,

            combo: this.sideData.combo,
            countsAsCombo: countsAsCombo,
            isImmobileSpin,
            isThreeCornersSpin,
            isMiniTSpin,
        };
        // update B2B
        moveResult.backToBack = 0;
        let b2bFlag = this.attackRule.isB2b(moveResult)
        if (b2bFlag == 1) {
            this.sideData.backToBack++;
            moveResult.backToBack = this.sideData.backToBack;
        } else if (b2bFlag == -1) {
            this.sideData.backToBack = 0;
        }
        // block garbage and complete moveResult
        let power = this.attackRule.apply(moveResult);
        if (this.isLocal) {
            let mode = this.sideRule.garbageBlockingMode;
            if (mode == GarbageBlockingMode.BLOCK || mode == GarbageBlockingMode.BOTH) {
                let powerSum = power.reduce((a,b) => a+b, 0)
                moveResult.attacksUsedForBlocking = this.garbageQueue.block(powerSum);
            }
            if (mode == GarbageBlockingMode.ATTACK || mode == GarbageBlockingMode.BOTH) {
                moveResult.attackPower = power;
            } else if (mode == GarbageBlockingMode.BLOCK) {
                this.subtractPower(power, moveResult.attacksUsedForBlocking);
                moveResult.attackPower = power;
            }
        } else {
            if (ackBlockGarbage != null && ackBlockGarbage != 0) {
                let check = this.garbageQueue.block(ackBlockGarbage);
                if (check != ackBlockGarbage) {
                    console.error('Invalid ackBlockGarbage received!');
                }

                this.subtractPower(power, ackBlockGarbage);
            }
            moveResult.attackPower = power;
        }

        
        // combo timer time bonus
        if (this.sideData.combo > 0) {
            let timeBonusTable = this.attackRule.isSpinBonus(moveResult) 
                    ? this.sideRule.comboTimerSpinBonusBase0
                    : this.sideRule.comboTimerMultiClearBonusBase0;
            let timeBonusMult = this.sideRule.comboTimerTimeBonusMultiplierTable[Math.min(this.sideRule.comboTimerTimeBonusMultiplierTable.length - 1, this.sideData.combo - 1)];
            let timeBonusFlat = timeBonusTable[Math.min(timeBonusTable.length - 1, moveResult.clearedLines)];
            let timeBonus = timeBonusFlat > 0 ? timeBonusFlat * timeBonusMult : timeBonusFlat;
            this.comboEnd += timeBonus * 1000;
        }


        // spawn local garbage
        let garbageLinesSpawned: number;
        if (!(this.sideRule.lineClearDelaysGarbage && checkClearResult.linesCleared.length > 0)) {
            // normally we have to spawn garbage here. However, if NTB, remote sides garbage are simulated upon acknowledgment instead of locally.
            if (this.sideRule.turnBased || this.isLocal) {
                garbageLinesSpawned = this.advanceGarbageQueue(this.sideRule.garbageSpawnCap, false, false);
            }
        }
        // spawn remote garbage
        if (!this.sideRule.turnBased && !this.isLocal && ackSpawnGarbage != null && ackSpawnGarbage > 0) {
            this.advanceGarbageQueue(ackSpawnGarbage, true, false);
        }
        
        // after all board updates are done, spawn the next piece and check for top out
        this.nextPiece();


        let moveInfo: MoveInfo = {
            x: x,
            y: y,
            rot: rot,
            hold: this.sideData.holdUsed,
            isTripleSpin: this.lastMoveIsTripleSpin,
            isLastMoveSpin: this.lastMoveIsSpin,
            ackSpawnGarbage: (!this.sideRule.turnBased && this.isLocal) ? garbageLinesSpawned : null,
            ackFixRateSpawnGarbage: (!this.sideRule.turnBased && this.isLocal) ? this.fixRateAck : null,
            ackBlockGarbage: moveResult.attacksUsedForBlocking,
            ackComboTimerBreak: comboTimerWasBroken,
        };
        this.fixRateAck = 0; // reset counter of fix-rate spawned garbage for the next move

        // update sideData
        this.sideData.piecesLeft--;
        this.sideData.holdUsed = false;


        // call listeners
        this.listenersOnLockDown.forEach(l => l(moveInfo, moveResult ));

        if (this.sideRule.turnBased && this.gameData.turn != this.sideNr) {
            this.listenersOnTurnEnded.forEach(l => l());
        }

        return moveResult;
    }

    private subtractPower(power: number[], toSubtract: number) {
        while (toSubtract > 0 && power.length > 0) {
            if (toSubtract >= power[0]) {
                toSubtract -= power[0];
                power.splice(0, 1);
            } else {
                power[0] -= toSubtract;
                break;
            }
        }
    }

    private checkThreeCornersSpin() {
        if (!this.lastMoveIsSpin) {
            return {isThreeCornersSpin: false, isMiniTSpin: false};
        }

        let isThreeCornersSpin = false;
        let isMiniTSpin = false;
        let ap = this.sideData.activePiece;
        if (ap.piece.size == 4 && ap.piece.id == 5) { // T-Piece
            let faceCorners = 0;
            let flatCorners = 0;
            const cornerPoses = [
                { faceCorners: [[0, 0], [0, 2]], flatCorners: [[2, 0], [2, 2]] },
                { faceCorners: [[0, 2], [2, 2]], flatCorners: [[0, 0], [2, 0]] },
                { faceCorners: [[2, 0], [2, 2]], flatCorners: [[0, 0], [0, 2]] },
                { faceCorners: [[0, 0], [2, 0]], flatCorners: [[0, 2], [2, 2]] },
            ];
            let cornerPos = cornerPoses[(ap.rot + 40000) % 4];
            for (let i = 0; i < 2; i++) {
                if (!this.isTileFree(cornerPos.faceCorners[i][0] + ap.y, cornerPos.faceCorners[i][1] + ap.x)) {
                    faceCorners++;
                }
                if (!this.isTileFree(cornerPos.flatCorners[i][0] + ap.y, cornerPos.flatCorners[i][1] + ap.x)) {
                    flatCorners++;
                }
            }

            isThreeCornersSpin = faceCorners + flatCorners >= 3;
            isMiniTSpin = isThreeCornersSpin && (!this.lastMoveIsTripleSpin && faceCorners < 2);
        }
        return { isThreeCornersSpin, isMiniTSpin };
    }

    isImmobile() {
        for (let dpos of [[-1,0],[0,-1],[0,1],[1,0]]) {
            if (!this.checkActivePieceCollision(dpos[0], dpos[1], 0)) {
                return false;
            }
        }
        return true;
    }

    nextPiece() {
        this.sideData.previews.push(this.pieceGen.next());
        const piece = this.sideData.previews.splice(0, 1)[0];
        if (piece != null) {
            this.sideData.activePiece = {
                piece: piece,
                rot: 0,
                x: null,
                y: null,
                isLastMoveRotation: false,
            }
            this.resetActivePiecePosition();
        } else {
            this.sideData.activePiece = null;
        }
    }

    advanceGarbageQueue(limit: number, ignoreGarbageDelay: boolean, isFixRate: boolean): number {
        let ret = 0;

        /*
         * Complicated:
         *  1. take next GarbageData[] from queue
         *  2. spawn the garbage (turns GarbageData[] into actual tiles)
         *  3. put the last spawned row back into the carry (remainder in queue needs it in case of spawning clean garbage)
         */
        let lastRow: TileType[] = null;
        let gbRes = this.garbageQueue.next(limit, ignoreGarbageDelay);
        if (gbRes.gb.length > 0 && gbRes.gb[0].heightLeft > 0) {
            gbRes.gb.forEach(g => {
                ret += g.heightLeft;
                lastRow = this.spawnGarbage(g);
            });
            if (gbRes.carry != null) {
                gbRes.carry.lastRow = lastRow;
            }
        }
        
        if (ret > 0) {
            this.listenersOnGarbageEntered.forEach(l => l(ret, isFixRate));
        }

        return ret;
    }

    spawnGarbage(garbage: GarbageData) {
        let rows = this.garbageGen.createRow(garbage);
        for (let rowData of rows) {
            let row: Tile[] = new Array(this.sideRule.width);
            for (let j = 0; j < row.length;j ++) {
                switch (rowData[j]) {
                    case TileType.EMPTY: break;
                    case TileType.GARBAGE:
                        row[j] = {
                            colorId: 7,
                            isGarbage: true,
                        }
                        break;
                    case TileType.LOCK:
                        row[j] = {
                            colorId: 8,
                            isGarbage: true,
                            nonClearing: true,
                            type: rowData[j],
                        }
                        break;

                }
            }
            this.spawnGarbageRow(row);
        }
        return rows[rows.length - 1];
    }

    hold(): boolean {
        if (this.sideRule.holdMode == HoldMode.INFINITE
            || (this.sideRule.holdMode == HoldMode.ONCE_ONLY && !this.sideData.holdUsed)
        ) {
            let piece = this.sideData.hold;
    
            this.resetActivePieceRotation();
            this.sideData.hold = this.sideData.activePiece.piece;
            
            this.sideData.holdUsed = !this.sideData.holdUsed;

            if (piece != null) {
                this.sideData.activePiece.piece = piece;
                this.resetActivePiecePosition();
            } else {
                this.nextPiece();
            }

            if (this.isLocal) {
                this.listenersOnLocalHold.forEach(l => l());
            }
            return true;
        } else {
            return false;
        }
    }

    resetActivePiecePosition() {
        let piece = this.sideData.activePiece.piece;
        this.sideData.activePiece.x = Math.floor((this.sideRule.width - piece.tiles[0].length) / 2);
        this.sideData.activePiece.y = this.sideRule.height - this.sideRule.playHeight - piece.tiles.length + MatUtil.getNumEmptyRowsBottom(piece.tiles);
        
        if (this.getProgressToNextFixRateGarbage(Date.now()) == 1) {
            this.sideData.activePiece.y++;
        }

        if (this.isLocal && this.checkActivePieceCollision(0, 0, 0)) {
            this.lockOut();
        }
    }

    resetActivePieceRotation() {
        let ap = this.sideData.activePiece;
        for (let i = ((ap.rot + 40)-1) % 4 + 1; i < 4; i++) {
            this.sideData.activePiece.piece.tiles = MatUtil.rotate(this.sideData.activePiece.piece.tiles);
        }
        ap.rot = 0;
    }

    checkActivePieceCollision(dy: number, dx: number, drot: number) {
        if (this.sideData.activePiece == null) return false;
        
        let tiles = this.sideData.activePiece.piece.tiles;
        drot = (drot + 4000000) % 4;
        if (drot != 0) {
            for (let i = 0; i < drot; i++) {
                tiles = MatUtil.rotate(tiles);
            }
        }
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j] != null) {
                    const ty = this.sideData.activePiece.y + dy + i;
                    const tx = this.sideData.activePiece.x + dx + j;
                    if (!this.isInside(ty, tx) || this.sideData.board[ty][tx] != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    isInside(ty: number, tx: number) {
        return ty >= 0
            && tx >= 0
            && ty < this.sideData.board.length
            && tx < this.sideData.board[0].length;
    }

    isTileFree(y: number, x: number): boolean {
        return this.isInside(y, x) && this.sideData.board[y][x] == null;
    }

    /**
     * lines have to be sorted!
     */
    clear(clearInfo: ClearInfo) {
        const lines = clearInfo.linesCleared;
        if (lines.length == 0) {
            return;
        }

        lines.forEach(line => {
            if (this.isRowGarbage(line)) {
                this.sideData.visibleGarbage--;
            }
        })

        let count = 1;
        for (let i = lines[lines.length - 1] - 1; i >= 0; i--) {
            if (count < lines.length && lines[lines.length - 1 - count] == i) {
                count++;
            } else {
                for (let j = 0; j < this.sideData.board[i].length; j++) {
                    this.sideData.board[i+count][j] = this.sideData.board[i][j];
                }
            }
        }

        this.listenersOnLinesCleared.forEach(l => l(clearInfo));
    }

    isRowGarbage(i: number) {
        for (let j = 0; j < this.sideData.board[i].length; j++) {
            if (this.sideData.board[i][j] != null && this.sideData.board[i][j].isGarbage) {
                return true;
            }
        };
        return false;
    }

    checkClear(detonated: Set<number>): { linesCleared: number[], garbageLineCount: number, multiClearValue: number } {
        let linesCleared = [];
        let garbageLineCount = 0;
        let lockLineCount = 0;
        let nonLockLineCount = 0;
        for (let i = 0; i < this.sideData.board.length; i++) {
            let clear = true;
            let garbage = false;;
            for (let j = 0; j < this.sideData.board[i].length; j++) {
                if (this.sideData.board[i][j] == null || this.sideData.board[i][j].nonClearing) {
                    clear = false;
                    break;
                } else if (this.sideData.board[i][j].isGarbage) {
                    garbage = true;
                }
            }
            if (clear) {
                linesCleared.push(i);
                if (garbage) {
                    garbageLineCount++;
                }
                if (detonated.has(i)) {
                    lockLineCount++;
                } else {
                    nonLockLineCount++;
                }
            }
        }
        return {
            linesCleared: linesCleared,
            garbageLineCount: garbageLineCount,
            multiClearValue: nonLockLineCount + Math.min(this.sideRule.locksMaxMultiClearValue, lockLineCount),
        };
    }

    spawnGarbageRow(row: Tile[]) {
        // if top row has tile, game over
        for (let j = 0; j < this.sideData.board[0].length; j++) {
            if (this.sideData.board[0][j] != null) {
                this.lockOut();
            }
        }

        // shift up by 1
        for (let i = 0; i < this.sideData.board.length - 1; i++) {
            for (let j = 0; j < this.sideData.board[i].length; j++) {
                this.sideData.board[i][j] = this.sideData.board[i+1][j];
            }
        }

        // place garbage
        const i = this.sideData.board.length - 1;
        this.sideData.board[i] = row;

        if ( this.sideData.activePiece.y > -MatUtil.getNumEmptyRowsTop(this.sideData.activePiece.piece.tiles)) {
            this.sideData.activePiece.y--;
        }
    }

    lockOut() {
        if (this.sideData.isAlive) {
            this.sideData.isAlive = false;
            this.listenersOnLockOut.forEach(l => l());
            this.sideStats.totalTime = this.gameData.stats.totalTime;
        }
    }

    doMoveInfo(moveInfo: MoveInfo): MoveResult {
        if (moveInfo.hold) {
            this.hold();
        }
        
        this.doMove(
            moveInfo.y - this.sideData.activePiece.y,
            moveInfo.x - this.sideData.activePiece.x,
            (moveInfo.rot + 4 - this.sideData.activePiece.rot) % 4);

        this.lastMoveIsTripleSpin = moveInfo.isTripleSpin;
        this.lastMoveIsSpin = moveInfo.isLastMoveSpin;

        return this.lockDown(moveInfo.ackSpawnGarbage, moveInfo.ackFixRateSpawnGarbage, moveInfo.ackBlockGarbage, moveInfo.ackComboTimerBreak);
    }

    doSubMoveInfo(subMoveInfo: SubmoveInfo) {
        if (this.sideData.holdUsed != subMoveInfo.hold) {
            this.hold();
        }

        let ap = this.sideData.activePiece;
        let drot = (subMoveInfo.rot + 4 - ap.rot) % 4;
        this.doMove(subMoveInfo.y - ap.y, subMoveInfo.x - ap.x, drot);
        this.listenersOnPieceMoved.forEach(l => l(null, null, drot, false));
    }


    // Combo Timer. Returns null if no combo timer is active.
    getComboTimeLeft(ct: number) {
        if (this.comboEnd != null && ct <= this.comboEnd) {
            return Math.max(0, (this.comboEnd - ct) / 1000);
        } else {
            return null;
        }
    }





    // COMMANDS
    
    executeCommand(cmd: Command) {
        this.executor.get(cmd.type)(cmd);
    }

    triggerListenerOnCommandExecuted() {
        this.listenersOnCommandExecuted.forEach(l => l());
    }

    cmd_appendPreview(cmd: AppendPreviewCommand) {
        let piece = createPiece(cmd.pieceSize, cmd.pieceId);
        if (this.sideData.activePiece == null) {
            this.sideData.activePiece = {
                piece,
                rot: 0,
                x: Math.floor((this.sideRule.width - piece.tiles[0].length) / 2),
                y: this.sideRule.height - this.sideRule.playHeight - piece.tiles.length + MatUtil.getNumEmptyRowsBottom(piece.tiles),
                isLastMoveRotation: false,
            };
        } else {
            this.sideData.previews.push(piece);
        }
    }

    cmd_fillHold(cmd: AppendPreviewCommand) {
        this.sideData.hold = createPiece(cmd.pieceSize, cmd.pieceId);
    }

    cmd_placePiece(cmd: PlacePieceCommand) {
        if (this.isLocal) return;
        
        this.doMoveInfo(cmd.moveInfo);
    }

    cmd_subMove(cmd: SubMoveCommand) {
        if (this.isLocal) return;
        
        this.doSubMoveInfo(cmd.subMoveInfo);
    }

    cmd_spawnGarbage(cmd: SpawnGarbageCommand) {
        this.garbageQueue.addGarbage({ holes: cmd.holes, totalHeight: cmd.height, delayTime: this.sideRule.garbageEnterDelay, queuedAt: Date.now(), heightLeft: cmd.height, lastRow: null });
        this.sideData.visibleGarbage += cmd.height;
        this.listenersOnGarbageReceived.forEach(l => l(cmd));
    }

    cmd_setTime(cmd: SetTimeCommand) {
        this.sideData.time = cmd.time;
    }

    cmd_timeOut(cmd: TimeOutCommand) {
        this.sideData.time = 0;
        this.lockOut();
    }
    
    cmd_die(cmd: DieCommand) {
        this.lockOut();
    }
}
