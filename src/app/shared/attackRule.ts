
import { SideData } from "./gameData";
import { ComboMode, SideRule, SpinBonusMode } from "./gameRule";
import { MoveResult } from "./playerEngine";

export class AttackRule {
    constructor(private sideRule: SideRule, private sideData: SideData) {}

    isSpinBonus(moveResult: MoveResult) {
        return this.sideRule.spinBonusMode == SpinBonusMode.IMMOBILE && moveResult.isImmobileSpin
        || this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && moveResult.isMiniTSpin
        || this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && moveResult.isThreeCornersSpin;
    }

    isB2b(moveResult: MoveResult) {
        if (this.isSpinBonus(moveResult)) {
            return this.readTable(this.sideRule.b2bSpinTruthTableBase0, moveResult.multiClearValue + 1);
        } else {
            return this.readTable(this.sideRule.b2bTruthTable, moveResult.multiClearValue);
        }
            
    }

    apply(moveResult: MoveResult): number[] {
        return this.applyCanContainZeroes(moveResult).filter(n => n != 0);
    }

    private applyCanContainZeroes(moveResult: MoveResult): number[] {
        // clear bonus from multiClearTable / spinBonusTable
        let clearBonus = 0;
        if (moveResult.multiClearValue > 0) {
            if (this.sideRule.spinBonusMode == SpinBonusMode.IMMOBILE && moveResult.isImmobileSpin) {
                // immobile spin
                clearBonus = this.readTable(this.sideRule.spinBonusTableBase0, moveResult.multiClearValue + 1);
            } else if (this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && moveResult.isMiniTSpin) {
                // t-spin mini
                clearBonus = 0;
            } else if (this.sideRule.spinBonusMode == SpinBonusMode.T_SPIN_ONLY && moveResult.isThreeCornersSpin) {
                // t-spin regular
                clearBonus = this.readTable(this.sideRule.spinBonusTableBase0, moveResult.multiClearValue + 1);
            } else {
                // no spin
                clearBonus = this.readTable(this.sideRule.multiClearTable, moveResult.multiClearValue);
            }
        }

        // B2B multiplier on clear bonus and additive
        let b2bMultiplierBonus = 0;
        let b2bAdditiveBonus = 0;
        if (moveResult.backToBack > 1) {
            b2bMultiplierBonus = clearBonus * (this.readTable(this.sideRule.b2bMultiplierTable, moveResult.backToBack - 1) - 1);
            b2bAdditiveBonus = this.readTable(this.sideRule.b2bAdditiveTable, moveResult.backToBack - 1);
        }
        
        let comboBonus = 0;
        if (this.sideRule.comboMode != ComboMode.NONE && moveResult.combo > 1 && moveResult.countsAsCombo == 1) {
            comboBonus = this.readTable(this.sideRule.comboTable, moveResult.combo - 1);
        }
        

        let boardClearBonus = moveResult.isPerfectClear ? this.sideRule.perfectClearBonus : moveResult.isColorClear ? this.sideRule.colorClearBonus : 0;
        let rawPower = clearBonus + comboBonus + boardClearBonus + b2bMultiplierBonus + b2bAdditiveBonus;
        if (this.sideRule.attackMultiplier != 1) {
            if (this.sideRule.useAttackMultiplierBalancer) {

                let ret: number[] = [];
                this.sideData.attackMultiplierSurplus += rawPower * this.sideRule.attackMultiplier;

                // guaranteed spawns (for attackMultiplier > 1)
                let guaranteedSpawn = Math.floor(this.sideRule.attackMultiplier);
                let spawnPower = Math.round(rawPower);
                for (let i = 0; i < guaranteedSpawn; i++) {
                    ret.push(spawnPower);
                    this.sideData.attackMultiplierSurplus -= spawnPower;
                }

                // spawn chance from surplus
                if (this.sideData.attackMultiplierSurplus >= rawPower) {
                    // lots of surplus --> always spawn garbage
                    ret.push(spawnPower);
                    this.sideData.attackMultiplierSurplus -= spawnPower;
                } else if (this.sideData.attackMultiplierSurplus > 0) {
                    // small surplus --> random chance of spawning garbage
                    if (this.sideData.attackMultiplierSurplus / rawPower >= 0.5) {
                        ret.push(spawnPower);
                        this.sideData.attackMultiplierSurplus -= spawnPower;
                    }
                }

                return ret;
            } else {
                let multPower = Math.round(rawPower * this.sideRule.attackMultiplier);
                return multPower > 0 ? [multPower] : [];
            }
        } else {
            this.sideData.attackMultiplierSurplus += rawPower;
            let spawnPower = Math.floor(this.sideData.attackMultiplierSurplus);
            this.sideData.attackMultiplierSurplus -= spawnPower;
            return spawnPower > 0 ? [spawnPower] : [];
        }
    }

    private readTable(table: number[], indexBase1: number): number {
        return table[Math.min(table.length, indexBase1) - 1];
    }
}
