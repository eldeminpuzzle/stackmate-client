import { TileType } from "./gameIntf";
import { GarbageHoleData } from "./gameRule";

export interface GameData {
    winner: number;
    sides: SideData[];
    turn: number; // must be null if the game is over
    
    stats: GameStats;
}

export interface SideData {
    isAlive: boolean;
    board: Tile[][];
    previews: Piece[];
    hold: Piece;
    activePiece: ActivePiece;
    piecesLeft: number;
    holdUsed: boolean;
    garbageQueue: GarbageData[];
    time: number; // seconds
    turnStart: number; // Date.now() at turn start
    combo: number;
    
    backToBack: number;

    attackMultiplierSurplus: number;

    // intermediates
    visibleGarbage: number;
}

export interface GarbageData {
    holes: GarbageHoleData[];
    totalHeight: number;
    delayTime: number;
    queuedAt: number; // when Date.now() - queuedAt >= delayTime, the garbage can spawn into the field
    
    heightLeft?: number;
    lastRow: TileType[];
}

export interface Tile {
    colorId: number;
    //connectivity: boolean[];
    isGarbage: boolean;
    nonClearing?: boolean;
    type?: TileType;
}

export interface Piece {
    tiles: Tile[][];
    size: number;
    id: number;
}

export interface ActivePiece {
    piece: Piece;
    x: number;
    y: number;
    rot: number;
    isLastMoveRotation: boolean;
}

export class SideStats {
    totalPieces: number = 0;
    totalAttack: number = 0;
    totalBlocked: number = 0;
    totalReceived: number = 0;
    totalTimeReceived: number = 0;
    totalTime: number = 0;
    rank: number = null;
}

export class GameStats {
    totalTime: number = 0;
    maxRank: number = -1; // e.g. number of teams

    sides: SideStats[] = [];
}
