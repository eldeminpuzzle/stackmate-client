import * as seedrandom from "seedrandom";

export class RandomGen {
    r: seedrandom.prng;
    constructor(seed?: number) {
        this.r = seedrandom(seed == null ? undefined : seed.toString());
    }

    int(max: number=Number.MAX_SAFE_INTEGER) {
        return Math.abs(this.r.int32()) % max;
    }

    chance(chance: number) {
        return this.r.double() < chance;
    }

    range(min: number, max: number) {
        return this.r.double() * (max - min) + min;
    }

    pick<T>(arr: T[]): T {
        return arr.length == 0 ? null : arr[this.int(arr.length)];
    }
}
