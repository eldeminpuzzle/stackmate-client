

import { GameRule } from './gameRule';
import { GameRuleInput, RuleField, sideFields } from './gameRuleFields';

export class GameRuleUtils {
    getGameRule(data: GameRuleInput): GameRule {
        let gameRule: GameRule = {
            sides: []
        };
        for (let sideNr = 0; sideNr < data.sideRules.length; sideNr++) {

            // inp = coalesce(side, global)
            let inp: Map<string, any> = new Map();
            for (let field of sideFields) {
                inp.set(field.key, this.getEffectiveValue(data, sideNr, field));
            }

            let sideRule: any = {};
            inp.forEach((value, key) => {
                sideRule[key] = value;
            });
            sideRule.height = sideRule.playHeight + 10;
            gameRule.sides.push(sideRule);
        }
        return gameRule;
    }

    getEffectiveValue(data: GameRuleInput, sideNr: number, field: RuleField) {
        let inpSide = data.sideRules[sideNr];
        let inpGlobal = data.globalSideRules;

        let val = inpSide.get(field.key);
        if (val == null) {
            val = inpGlobal.get(field.key);
        }
        if (field.valueConverter != null) {
            val = field.valueConverter(val);
        }
        return val;
    }
    
    getRuleJson(data: GameRuleInput) {
        let gameRules = this.mapToObj(data.gameRules);

        let globalSideRules: any = {};
        for (let field of sideFields) {
            globalSideRules[field.key] = data.globalSideRules.get(field.key);
        }

        let sideRules: any[] = [];
        for (let sideNr = 0; sideNr < data.sideRules.length; sideNr++) {
            let sideObj: any = {};
            for (let field of sideFields) {
                let sideValue = data.sideRules[sideNr].get(field.key);
                if (sideValue != null) {
                    sideObj[field.key] = sideValue;
                }
            }
            sideRules.push(sideObj);
        }

        let obj: any = {
            gameRules,
            globalSideRules,
            sideRules
        };
        return JSON.stringify(obj);
    }
    
    loadRuleJson(json: string): GameRuleInput {
        let data: any = null;
        try {
            data = JSON.parse(json);
        } catch(e) {
            console.error(e);
            return null;
        }

        if (data == null) {
            return null;
        }
        
        let ret: GameRuleInput = {
            gameRules: new Map(),
            globalSideRules: new Map(),
            sideRules: [],
        };

        if (data.globalSideRules == null) {
            return null;
        } else {

            for (let field of sideFields) {
                if (data.globalSideRules[field.key] != null) {
                    ret.globalSideRules.set(field.key, data.globalSideRules[field.key]);
                } else {
                    ret.globalSideRules.set(field.key, field.defaultValue);
                }
            }
        }

        if (data.sideRules == null) {
            return null;
        } else {
            for (let sideNr = 0; sideNr < data.sideRules.length; sideNr++) {
                let sideMap = new Map();
                for (let field of sideFields) {
                    if (data.sideRules[sideNr][field.key] != null) {
                        sideMap.set(field.key, data.sideRules[sideNr][field.key]);
                    }
                }
                ret.sideRules.push(sideMap);
            }

            return ret;
        }
    }

    getGameRuleFromJson(json: string) {
        let gri = this.loadRuleJson(json);
        return this.getGameRule(gri);   
    }
      
    private mapToObj(map: Map<string, any>) {
        let ret: any = {};
        map.forEach((value, key) => ret[key] = value);
        return ret;
    }
}
export default new GameRuleUtils();
