
import { ComboMode, GarbageBlockingMode, GarbageHoleData, HoldMode, LockComboMode, PieceGenerationMode, RotationMode, SpinBonusMode, TargetingMode } from './gameRule';
import { PieceList, PIECES } from './pieceGen';

export enum RuleFieldType { INTEGER_BOX, DECIMAL_BOX, STRING, ON_OFF, CHOICE };
export enum RuleFieldDetailLevel { BASIC, ADVANCED, DETAILED, HIDDEN };

export interface GameRuleInput {
    gameRules: Map<string, any>;
    globalSideRules: Map<string, any>;
    sideRules: Map<string, any>[];
}

export interface RuleField {
    key: string;
    path: string;
    desc?: string;
    type: RuleFieldType;
    defaultValue: any;
    nonOverridable?: boolean,
    choices?: {value: number, label: string}[];
    minValue?: number;
    maxValue?: number;
    detailLevel?: RuleFieldDetailLevel;
    requirement?: (game: (string: string) => any, side: (string: string) => any) => boolean;
    validator?: (v: any) => boolean;
    valueConverter?: (v: any) => any;
}

function intListValidator(minValue: number, maxValue: number) {
    return (a: string) => {
        for (let v of a.split(',')) {
            var value = parseInt(v);
            if (!(!isNaN(value) && value >= minValue && maxValue <= 20)) {
                return false;
            }
        }
        return true;
    }
}

function floatListValidator(minValue: number, maxValue: number) {
    return (a: string) => {
        for (let v of a.split(',')) {
            var value = parseFloat(v);
            if (!(!isNaN(value) && value >= minValue && maxValue <= 20)) {
                return false;
            }
        }
        return true;
    }
}

function pieceListValidator(list: string) {
    let pieceList: PieceList[] = parsePieceList(list);
    let bagSize = 0;
    for (let item of pieceList) {
        if (item.multiplier == null || isNaN(item.multiplier) || item.multiplier <= 0) {
            return false;
        }

        if (item.size < 1 || item.size > PIECES.length) {
            return false;
        }

        if (item.pieceId != null && item.pieceId >= PIECES[item.size - 1].length) {
            return false;
        }

        bagSize += Math.round((item.pieceId == null ? PIECES[item.size - 1].length : 1) * item.multiplier);
    }
    if (bagSize > 1000) {
        console.log('bagSize ' + bagSize);
        return false;
    }
    return true;
}

function parsePieceList(list: string) {
    let ret: PieceList[] = [];
    for (let entry of list.split(',')) {
        let multiplier = 1;
        if (entry.indexOf('*') != -1) {
            let split = entry.split('*');
            entry = split[0];
            multiplier = parseInt(split[1]);
        }

        let pieceId = null;
        if (entry.indexOf('-') != -1) {
            let split = entry.split('-');
            entry = split[0];
            pieceId = parseInt(split[1]);
        }

        let size = parseInt(entry);

        ret.push({
            multiplier, pieceId, size
        })
    }
    return ret;
}

function parseGarbageHoles(list: string) {
    let ret: GarbageHoleData[] = [];
    for (let entry of list.split(',')) {
        ret.push({
            width: parseInt(entry.substr(0, 1)),
            type: parseInt(entry.substr(1)),
        });
    }
    return ret;
}

let sideFields: RuleField[] = [{
    key: 'width',
    path: 'Board/Width',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 10,
    minValue: 4,
    maxValue: 20
}, {
    key: 'playHeight', // sets height=playHeight+10
    path: 'Board/Height (+1)',
    desc: 'The actual visible board height will be one less than this number (however an extra row will be partially visible during garbage-entry animation).',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 21,
    minValue: 2,
    maxValue: 100
}, {
    key: 'team',
    path: 'Player/Team',
    type: RuleFieldType.CHOICE,
    defaultValue: 0,
    choices: [
        { value: 0, label: 'Free' },
        { value: 1, label: 'Blue' },
        { value: 2, label: 'Green' },
        { value: 3, label: 'Red' },
        { value: 4, label: 'Yellow' },
    ],
    desc: '0 = no team (free for all)',
}, {
    key: 'targetingMode',
    path: 'Player/Targeting mode',
    type: RuleFieldType.CHOICE,
    choices: [
        // { value: TargetingMode.EVEN, label: 'Even' }, // not implemented
        { value: TargetingMode.LEAST, label: 'Least' },
        { value: TargetingMode.NEXT, label: 'Next' },
        { value: TargetingMode.LEAST_ON_SCREEN, label: 'Least uncleared' },
    ],
    nonOverridable: true,
    defaultValue: TargetingMode.LEAST,
    desc: 'Determines who will receive the lines sent by a player. Even: spread garbage evenly to all other players. Least: send all to the player who received the least so far. Next: always send to the next position player.',
}, {
    key: 'rotationMode',
    path: 'Movement/Rotation system (kick table)',
    desc: 'Determines whether a piece can be shifted upon collision after a rotation in order to fit in the board',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: RotationMode.NO_KICK, label: 'No kick (classic)' },
        { value: RotationMode.SRS, label: 'SRS' },
        { value: RotationMode.NEAREST, label: 'Nearest' },
    ],
    defaultValue: RotationMode.SRS
}, {
    key: 'allow180Rotation',
    path: 'Movement/Allow 180° degrees rotation',
    type: RuleFieldType.ON_OFF,
    defaultValue: true,
}, {
    key: 'gravity',
    path: 'Gravity/Gravity',
    desc: 'Piece falling speed in squares per second. If -1, infinite gravity will be applied. If 0, pieces will never fall nor auto lock.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 2,
    minValue: -1,
    maxValue: 1000,
    validator: g => (g == -1 || g >= 0)
}, {
    key: 'lockDelay',
    path: 'Gravity/Lock delay',
    desc: 'Time in seconds before a piece automatically locks down if it touches the ground. This time resets everytime the piece is moved.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 0.5,
    minValue: 0,
    maxValue: 10,
    requirement: (game, side) => side('gravity') != 0,
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'lockDelayResetLimit',
    path: 'Gravity/Lock delay reset limit',
    desc: 'Time in seconds before a piece automatically locks down if it touches the ground. This time is NOT reset if the piece is moved. If the piece is no longer touching the ground, this time is paused.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 5,
    minValue: 0,
    maxValue: 30,
    requirement: (game, side) => side('gravity') != 0,
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'pieceList',
    path: 'Piece Generation/Allowed pieces',
    desc: 'Comma separated values to specify which pieces can spawn. E.g. "5" = pentomino mode. "4*2,5" = two of each tetromino and one of each pentominoes. "4-1,4-2,4-3,4-4,4-5,4-6" = no I pieces. Discord Kevin#2642 for more details.',
    type: RuleFieldType.STRING,
    validator: pieceListValidator,
    valueConverter: parsePieceList,
    defaultValue: '4',
    detailLevel: RuleFieldDetailLevel.BASIC,
}, {
    key: 'pieceGenerationMode',
    path: 'Piece Generation/Piece generator',
    desc: 'Random generator for pieces. Random: total random. Bags: spawn every piece once in random order. Memory: remembers the last n pieces and avoid spawning them.',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: PieceGenerationMode.RANDOM, label: 'Random' },
        { value: PieceGenerationMode.BAG, label: 'Bags' },
        { value: PieceGenerationMode.MEMORY, label: 'Memory' },
    ],
    defaultValue: PieceGenerationMode.BAG
}, {
    key: 'pieceGenerationMemSize',
    path: 'Piece Generation/Piece generator memory size',
    desc: 'If the Memory piece generator is used, this is the number of pieces before the same piece can spawn again. This should never be set larger than the bag size.',
    type: RuleFieldType.INTEGER_BOX,
    minValue: 1,
    maxValue: 10000,
    defaultValue: 3,
}, {
    key: 'pieceGenerationShareSeed',
    path: 'Piece Generation/Same pieces',
    desc: 'If checked, all players will receive the same random seed. This gives the same pieces to every player unless they have different set of allowed pieces.',
    type: RuleFieldType.ON_OFF,
    defaultValue: false,
}, {
    key: 'generatePieceLocally',
    path: 'Piece Generation/Generate pieces locally',
    desc: 'If turned off, pieces will be generated on the server. Players will see slight delay before new pieces appear, but makes it impossible to cheat / predict the pieces before they appear.',
    type: RuleFieldType.ON_OFF,
    defaultValue: true,
    detailLevel: RuleFieldDetailLevel.HIDDEN,
}, {
    key: 'numPreviews',
    path: 'Piece Generation/Number of previews',
    desc: 'Number of pieces visible in the queue (right side of the board)',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 5,
    minValue: 0,
    maxValue: 13
}, {
    key: 'holdMode',
    path: 'Piece Generation/Hold mode',
    desc: 'With hold, a player can choose which piece he wants to play.',
    type: RuleFieldType.CHOICE,
    defaultValue: HoldMode.ONCE_ONLY,
    choices: [
        { value: HoldMode.NO_HOLD, label: "No hold" },
        { value: HoldMode.ONCE_ONLY, label: "Once per piece" },
        { value: HoldMode.INFINITE, label: "Always allowed" },
    ],
}, {
    key: 'turnBased',
    path: 'Turn Control/Turn based',
    desc: 'EXPERIMENTAL. The game was designed to be turn-based. If you turn this off, be prepared to defend against a few mega bugs.',
    type: RuleFieldType.ON_OFF,
    defaultValue: false,
    nonOverridable: true,
}, {
    key: 'piecesPerTurn',
    path: 'Turn Control/Pieces per turn',
    desc: 'How many pieces a player have to play before switching turns to the opponents',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 7,
    minValue: 1,
    maxValue: 50,
}, {
    key: 'timeInitial',
    path: 'Turn Control/Starting time',
    desc: 'Time limit per player (seconds). A player loses the game if his time run out.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 30,
    minValue: 3,
    maxValue: 7200,
}, {
    key: 'timePerTurn',
    path: 'Turn Control/Time per turn',
    desc: 'Time bonus (seconds) added to a player\'s time pool at the beginning of every turn. Can contain decimal point (e.g. 2.5 seconds).',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 0,
    minValue: 0,
    maxValue: 600,
}, {
    key: 'timePerMove',
    path: 'Turn Control/Time bonus per move',
    desc: 'Time bonus (seconds) added to a player\'s time pool at the beginning of every turn. Can contain decimal point (e.g. 0.75 seconds).',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 2,
    minValue: 0,
    maxValue: 60,
}, {
    key: 'timeMaxPool',
    path: 'Turn Control/Max time pool',
    desc: 'The maximum amount of time a player can save up. If the limit is reached, time bonus per turn/piece placed will be ignored.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 60,
    minValue: 0,
    maxValue: 7200,
}, {
    key: 'garbageHoles',
    path: 'Garbage/Garbage holes',
    desc: '"10" = normal garbage. "30" = 3-wide garbage hole. "13" = locks garbage. "10,10,10" = super cheese. "23,20" = ?',
    type: RuleFieldType.STRING,
    defaultValue: '10',
    valueConverter: parseGarbageHoles
}, {
    key: 'garbageCleanlinessWithin',
    path: 'Garbage/Garbage cleanliness within',
    desc: 'The % chance of each garbage line to have a hole in the same column to the previous garbage line within an attack. This controls whether large attacks will send swiss cheese or straight line garbage.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 100,
    minValue: 0,
    maxValue: 100,
}, {
    key: 'garbageCleanlinessBetween',
    path: 'Garbage/Garbage cleanliness between',
    desc: 'The % chance of a garbage line from an attack to have a hole in the same column to the previous attack. This controls whether several small attacks will send swiss cheese or straight line garbage.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 0,
    minValue: 0,
    maxValue: 100,
}, {
    key: 'garbageEdgeLimitation',
    path: 'Garbage/Garbage edge zone',
    desc: 'The number of left-most and right-most columns in which garbage holes are not allowed to spawn. E.g. 1 will prevent garbage holes on the left and right most columns. A large number will force the garbage holes to only spawn in the center.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 0,
    minValue: 0,
    maxValue: 10,
}, {
    key: 'locksComboMode',
    path: 'Garbage/Locks combo mode',
    desc: 'Determines whether placing pieces on top of locks count towards the combo. The setting to maintain combo without increasing the combo count is only useful if there are multiple locks per row or the lock max multi-clear-value is 0.',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: LockComboMode.INCREASES_COMBO, label: 'Counts as combo' },
        { value: LockComboMode.INCREASES_COMBO, label: 'Only prevents combo break' },
        { value: LockComboMode.INCREASES_COMBO, label: 'Does not count' },
    ],
    defaultValue: LockComboMode.INCREASES_COMBO,
}, {
    key: 'locksMaxMultiClearValue',
    path: 'Garbage/Locks max multi-clear-value',
    desc: 'Garbage lines cleared by locks are counted separately from normal line clears. For example, a move clears two normal lines, but also clears two garbage lines by putting the piece on top of stacked locks. If this setting is 0, then the move counts as a double. But if this settings is 2 or higher, the move is counted as 2+2=quadruple, and will send the corresponding amount of lines.',
    type: RuleFieldType.INTEGER_BOX,
    minValue: 0,
    maxValue: 40,
    defaultValue: 1,
}, {
    key: 'lineClearDelaysGarbage',
    path: 'Garbage/Line clear delays garbage',
    desc: 'If on, incoming garbage lines will only enter the field if a piece is locked down without clearing a line.',
    type: RuleFieldType.ON_OFF,
    defaultValue: true,
}, {
    key: 'garbageBlockingMode',
    path: 'Garbage/Garbage cancellation mode',
    desc: 'Whether it is possible to cancel incoming garbage lines by sending.',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: GarbageBlockingMode.ATTACK, label: 'No cancelling' },
        { value: GarbageBlockingMode.BLOCK, label: 'Cancel garbage' },
        { value: GarbageBlockingMode.BOTH, label: 'Cancel and send lines' },
    ],
    defaultValue: GarbageBlockingMode.BLOCK,
}, {
    key: 'garbageEnterDelay',
    path: 'Garbage/Garbage delay',
    desc: 'Time in seconds before the garbage enters the garbage queue. This gives more chance to react to incoming garbage lines.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 0.25,
    minValue: 0,
    maxValue: 5,
}, {
    key: 'garbageSpawnCap',
    path: 'Garbage/Max garbage spawned at once',
    desc: 'This limits number of garbage lines that can enter the field at once (per piece locked down). -1 means infinite. 0 should only be used if "Garbage spawn continuous rate" is used instead.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 6,
    minValue: -1,
    maxValue: 100,
}, {
    key: 'garbageSpawnFixRate',
    path: 'Garbage/Garbage spawn continuous rate',
    desc: 'Garbage lines spawned per second from the garbage queue regardless of placing pieces.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 0,
    minValue: 0,
    maxValue: 100,
}, {
    key: 'comboMode',
    path: 'Attack/Combo bonus',
    desc: 'Determines the attack power from consecutively clearing lines (number of cleared lines has no effect) starting from the first clear.',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: ComboMode.NONE, label: 'No combo' },
        { value: ComboMode.CONSECUTIVE_CLEARS, label: 'Consecutive clears' },
        { value: ComboMode.TIMER, label: 'Timer' },
    ],
    defaultValue: ComboMode.CONSECUTIVE_CLEARS,
}, {
    key: 'comboTable',
    path: 'Attack/Combo attack table',
    type: RuleFieldType.STRING,
    defaultValue: '0,1,1,1,2,2,3',
    validator: floatListValidator(0, 20),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    // comboTimerTimeBonusTable: number[][];
    // comboTimerTimeBonusDampening: number; // multiplier is not applied if time bonus is negative (e.g. penalty for not clearing lines)
    key: 'comboTimerInitial',
    path: 'Attack/Combo timer/Base combo time',
    desc: 'How much time the player has initially when starting a combo. During the combo, the player may get extra time bonuses based on the other settings.',
    type: RuleFieldType.DECIMAL_BOX,
    minValue: 0.1,
    maxValue: 10,
    defaultValue: 1.5,
    requirement: (g, s) => s('comboMode') == ComboMode.TIMER,
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'comboTimerMultiClearBonusBase0',
    path: 'Attack/Combo timer/Multi clear time bonus table',
    desc: 'How much time will be added to the combo when clearing 0, 1, 2, and so on lines at once. Can be negative to give time penalty for non/small clears.',
    type: RuleFieldType.STRING,
    defaultValue: '-0.25,0.25,0.5,0.75,1',
    requirement: (g, s) => s('comboMode') == ComboMode.TIMER,
    validator: floatListValidator(-100, 3),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'comboTimerSpinBonusBase0',
    path: 'Attack/Combo timer/Spin clear time bonus table',
    desc: 'How much time will be added to the combo when clearing 0, 1, 2, and so on lines with a spin bonus. Can be negative to give time penalty for non/small clears.',
    type: RuleFieldType.STRING,
    defaultValue: '0,0.5,1,1.5',
    requirement: (g, s) => s('comboMode') == ComboMode.TIMER,
    validator: floatListValidator(-100, 3),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'comboTimerTimeBonusMultiplierTable',
    path: 'Attack/Combo timer/Time bonus multiplier per combo',
    desc: 'Normally, use this to give diminishing time bonus at higher combos to avoid infinite combos and give emphasis on getting large clears early in a combo.',
    type: RuleFieldType.STRING,
    defaultValue: '1,0.8,0.6,0.4,0.3,0.2,0.1,0',
    requirement: (g, s) => s('comboMode') == ComboMode.TIMER,
    validator: floatListValidator(0, 1),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'multiClearTable',
    path: 'Attack/Multi clear attack table',
    desc: 'Determines the attack power from clearing multiple lines at once (for the standard pieces, maximum 4 lines can be cleared at once)',
    type: RuleFieldType.STRING,
    defaultValue: '0,1,2,4,6,8,10,12',
    validator: floatListValidator(0, 20),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'spinBonusTableBase0',
    path: 'Attack/Spin clear attack table (start at 0)',
    desc: 'Determines the attack power from clearing (multiple) lines with a spin bonus. Not additive with the Multi clear attack table',
    type: RuleFieldType.STRING,
    defaultValue: '0,2,4,6,8,10,12',
    requirement: (game, side) => side('spinBonusMode') != SpinBonusMode.NO_BONUS,
    validator: floatListValidator(0, 20),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'b2bMultiplierTable',
    path: 'Attack/B2B multiplier table',
    desc: 'In case of back-to-back, The "Multi clear" and "Spin clear" attack tables will be multiplied by the B2B multiplier. This can be a list of values to reward longer B2B chains. Attack power from combo table is not affected.',
    type: RuleFieldType.STRING,
    defaultValue: '1.5',
    validator: floatListValidator(0, 20),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'b2bAdditiveTable',
    path: 'Attack/B2B additive table',
    desc: 'Back-to-back clears will have this amount added to its attack power. This can be a list of values to reward longer B2B chains. This value will not be multiplied by the B2B multiplier table.',
    type: RuleFieldType.STRING,
    defaultValue: '0',
    validator: floatListValidator(0, 20),
    valueConverter: (a: string) => (a).split(',').map(str => parseFloat(str)),
    detailLevel: RuleFieldDetailLevel.ADVANCED,
}, {
    key: 'b2bTruthTable',
    path: 'Attack/B2B truth table',
    desc: 'The n-th value in this table refers to clearing n lines at once (so the fourth value is for quadruple clear). A \'1\' means the move will count as B2B, a \'0\' means it will keep the B2B chain, while a \'-1\' means it will break (reset) the B2B bonus.',
    type: RuleFieldType.STRING,
    defaultValue: '-1,-1,-1,1',
    validator: intListValidator(-1, 1),
    valueConverter: (a: string) => (a).split(',').map(str => parseInt(str)),
    detailLevel: RuleFieldDetailLevel.DETAILED,
}, {
    key: 'b2bSpinTruthTableBase0',
    path: 'Attack/B2B spin truth table (start at 0)',
    desc: 'The n-th value in this table refers to clearning (n-1) lines at once with a spin bonus (so the fourth value is for *-Spin Triples). A \'1\' means the move will count as B2B, a \'0\' means it will keep the B2B chain, while a \'-1\' means it will break (reset) the B2B bonus. If a clear is bigger than the table, the last value of the table will be used.',
    type: RuleFieldType.STRING,
    defaultValue: '0,1,1,1',
    requirement: (game, side) => side('spinBonusMode') != SpinBonusMode.NO_BONUS,
    validator: intListValidator(-1, 1),
    valueConverter: (a: string) => (a).split(',').map(str => parseInt(str)),
    detailLevel: RuleFieldDetailLevel.DETAILED,
}, {
    key: 'spinBonusMode',
    path: 'Attack/Spin bonus',
    desc: 'Determines whether rotating a piece into tight positions sends attacks to the opponent.',
    type: RuleFieldType.CHOICE,
    choices: [
        { value: SpinBonusMode.NO_BONUS, label: 'No bonus' },
        { value: SpinBonusMode.T_SPIN_ONLY, label: 'T-Spin only' },
        { value: SpinBonusMode.IMMOBILE, label: 'Immobile' }
    ],
    defaultValue: SpinBonusMode.T_SPIN_ONLY
}, {
    key: 'perfectClearBonus',
    path: 'Attack/All clear bonus',
    desc: 'Determines the attack power from emptying the board completely.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 10,
    minValue: 0,
    maxValue: 20,
}, {
    key: 'colorClearBonus',
    path: 'Attack/Color clear bonus',
    desc: 'Determines the attack power from clearing everything in the board except garbage lines.',
    requirement: (game, side) => side('perfectClearBonus') > 0,
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 0,
    minValue: 0,
    maxValue: 20,
}, {
    key: 'attackMultiplier',
    path: 'Attack/Attack multiplier',
    desc: 'Scales the attack strength from all sources including multi-clears, spin-clears, B2B, combo and board clear bonuses.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 1,
    minValue: 0,
    maxValue: 20,
}, {
    key: 'receiveMultiplier',
    path: 'Attack/Receive multiplier',
    desc: 'Scales the received attacks from all sources.',
    type: RuleFieldType.DECIMAL_BOX,
    defaultValue: 1,
    minValue: 0,
    maxValue: 20,
}, {
    key: 'useAttackMultiplierBalancer',
    path: 'Attack/Use attack multiplier balancer',
    desc: 'Uses a rigged random mechanic to cancel or duplicate attacks rather than just multiplying the number of lines sent to avoid spawning only cheese or only clean garbage lines when using attack multiplier.',
    type: RuleFieldType.ON_OFF,
    defaultValue: true,
    detailLevel: RuleFieldDetailLevel.ADVANCED,

    

/* ------------------ NETWORK ------------------ */
}, {
    key: 'lagTolerance',
    path: 'Network/Lag tolerance (ms)',
    desc: 'If the server does not receive any packet from a client for this amount of time during a game, the player will be dropped.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 5000,
    minValue: 500,
    maxValue: 60000,



/* ------------------ ANIMATION ------------------ */
}, {
    key: 'garbageEntryAnimationRate',
    path: 'Animation/Garbage entry animation rate',
    desc: 'How fast the garbage appears in the field. Does not affect top out detection.',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 100,
    minValue: 5,
    maxValue: 1000
}, {
    key: 'lineClearAnimationRate',
    path: 'Animation/Line clear animation rate',
    desc: 'How fast the board falls down when lines are cleared',
    type: RuleFieldType.INTEGER_BOX,
    defaultValue: 100,
    minValue: 5,
    maxValue: 1000,
}];
export { sideFields };
