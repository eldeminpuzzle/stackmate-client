import { GameRule, GarbageHoleData } from "./gameRule";
import { MoveInfo, SubmoveInfo } from "./playerEngine";

export enum Commands { START_ROUND, APPEND_PREVIEW, FILL_HOLD, PLACE_PIECE, SUB_MOVE, SPAWN_GARBAGE, SET_TIME, TIME_OUT, DIE }
export enum TileType { EMPTY, GARBAGE, BOMB, LOCK, GLASS }

export interface Command {
    type: Commands;
    sideNr?: number;
}

export interface StartGameData {
    gameRule: GameRule;
    localSideNr: number;
}

export interface StartRoundCommand extends Command {
    turn: number;
    randomSeeds: number[];
}

export interface AppendPreviewCommand extends Command {
    sideNr: number;
    pieceSize: number;
    pieceId: number;
}

export interface FillHoldCommand extends Command {
    sideNr: number;
    pieceSize: number;
    pieceId: number;
}

export interface PlacePieceCommand extends Command {
    sideNr: number;
    moveInfo: MoveInfo;
}

export interface SubMoveCommand extends Command {
    sideNr: number;
    subMoveInfo: SubmoveInfo;
}

export interface SpawnGarbageCommand extends Command {
    sideNr: number;
    holes: GarbageHoleData[];
    height: number;
}

export interface SetTimeCommand extends Command {
    sideNr: number;
    time: number; // seconds
}

export interface TimeOutCommand extends Command {
    sideNr: number;
}

export interface DieCommand extends Command {
    sideNr: number;
}
