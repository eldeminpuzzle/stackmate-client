import { RandomGen } from "./randomGen";

interface RecvInfo {
    name: string;
    id: number;
    args: any;
}

interface SendInfo extends RecvInfo {
    resendTimeout: any;
    retryCount: number;
}

export class SocketWrapper {
    private socket: any;
    private callbacks = new Map<string, (...args: any) => any>();

    private retryLimit = 10;
    private resendInterval = 500;

    private sendNextId = 0;
    private sendBuffer = new Map<number, SendInfo>();
    
    private recvNextId = 0;
    private recvBuffer = new Map<number, RecvInfo>();

    private r = new RandomGen();
    private testDelayMsgMin = 0;
    private testDelayMsgMax = 0;
    private testDropMsgChance = 0;

    count = 0;

    getRandomDelay() {
        return this.r.range(this.testDelayMsgMin, this.testDelayMsgMax);
    }

    constructor(socket: any) {
        this.socket = socket;
        if (this.testDelayMsgMax == 0) {
            this.socket.on('ack', this.recvAck.bind(this));
            this.socket.on('msg', this.recvMsg.bind(this));
        } else {
            this.socket.on('ack', (id: number) => setTimeout(() => this.recvAck(id), this.getRandomDelay())); // TEST DELAY
            this.socket.on('msg', (id: number, name: string, argsStr: string) => setTimeout(() => this.recvMsg(id, name, argsStr), this.getRandomDelay())); // TEST DELAY
        }
        this.socket.on('disconnect', this.handleDisconnect.bind(this));

        this.socket.on('connect', () => {
            console.log('Connected!');
        });

        this.on('testHeartbeat', () => {
            console.log('testHeartbeat ' + this.count++);
        });

        // this.testHeartbeat();
    }

    testHeartbeat() {
        this.send('testHeartbeat');
        setTimeout(() => {
            this.testHeartbeat();
        }, 30000);
    }

    close() {
        this.socket.close();
    }

    send(name: string, ...args: any) {
        let id = this.sendNextId;
        this.sendNextId++;

        let info: SendInfo = {
            resendTimeout: setTimeout(() => this.resendOrDc(id), this.resendInterval),
            retryCount: 0,
            name,
            id,
            args: JSON.stringify(args)
        };

        this.sendBuffer.set(id, info);
        this.emit(info);
    }

    private recvAck(id: number) {
        if (this.testDropMsgChance > 0 && this.r.chance(this.testDropMsgChance)) return;
        if (this.sendBuffer.has(id)) {
            clearTimeout(this.sendBuffer.get(id).resendTimeout);
            this.sendBuffer.delete(id);
        }
    }

    private resendOrDc(id: number) {
        let info = this.sendBuffer.get(id);
        info.retryCount++;
        if (info.retryCount > this.retryLimit) {
            this.socket.disconnect();
        } else {
            this.emit(info);
            info.resendTimeout = setTimeout(() => this.resendOrDc(id), this.resendInterval);
        }
    }

    private emit(info: SendInfo) {
        this.socket.emit('msg', info.id, info.name, info.args);
    }

    on(name: string, callback: (...args: any) => any) {
        this.callbacks.set(name, callback);
    }

    private recvMsg(id: number, name: string, argsStr: string) {
        let args: any[] = JSON.parse(argsStr);
        if (this.testDropMsgChance > 0 && this.r.chance(this.testDropMsgChance)) return;
        this.sendAck(id);
        if (this.recvNextId == id) {
            let callback = this.callbacks.get(name);
            if (callback == null) {
                console.error('Invalid message ' + name + ' received from client. Disconnecting.');
                this.socket.disconnect();
            } else {
                // console.log('executing ' + id + ' - ' + name);
                callback(...args);
                this.recvNextId++;

                // execute previously buffered msgs (due to out of order receive)
                while (this.recvBuffer.has(this.recvNextId)) {
                    let info = this.recvBuffer.get(this.recvNextId);
                    // console.log('executing ' + this.recvNextId + ' - ' + info.name);
                    this.callbacks.get(info.name)(...info.args);
                    this.recvBuffer.delete(this.recvNextId);
                    this.recvNextId++;
                }
            }
        } else if (id < this.recvNextId) {
            // ignore old message - the other side resent it because our ack has not been received
        } else {
            // msg is good but an earlier msg has been lost. Buffer this msg for later, but ack it already
            this.recvBuffer.set(id, {id, name, args});
        }
    }

    sendAck(id: number) {
        this.socket.emit('ack', id);
    }

    handleDisconnect() {
        console.log("Disconnected!");
        // this.socket.disconnect();
        // this.sendBuffer.forEach((value, key) => {
        //     clearTimeout(this.sendBuffer.get(value.id).resendTimeout);
        // });
        this.callbacks.get('disconnect')();
    }

    forceDisconnect() {
        this.socket.disconnect();
    }
}
