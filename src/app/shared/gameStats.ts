import { SideStats } from "./gameData";
import { MoveInfo, MoveResult, PlayerEngine } from "./playerEngine";

export class GameStatsController {
    private stats: SideStats;

    constructor(private playerEngine: PlayerEngine) {
        this.stats = this.playerEngine.sideStats;

        this.stats.totalTimeReceived = this.playerEngine.sideRule.timeInitial;

        this.playerEngine.listenersOnLockDown.add((m: MoveInfo, r: MoveResult) => {
            this.stats.totalAttack += r.attackPower.reduce((a,b)=>a+b, 0);
            this.stats.totalBlocked += r.attacksUsedForBlocking;
            this.stats.totalPieces++;
        });

        this.playerEngine.listenersOnGarbageReceived.add(cmd => this.stats.totalReceived += cmd.height);
        this.playerEngine.listenersOnTimeChanged.add(dt => this.stats.totalTimeReceived += Math.max(0, dt));
    }
}
