import { Injectable, NgZone } from '@angular/core';
import { SocketService } from '../service/socket.service';
import { GameRule } from '../shared/gameRule';
import { RoomInfo } from '../shared/intf';

@Injectable({
    providedIn: 'root'
})
export class LobbyService {

    constructor(private socket: SocketService, private ngZone: NgZone) { }

    getRoomInfos() {
        this.socket.send("getRooms");

        // TODO: prevent multiple requests if one is already under way
    }

    createRoom(name: string, custom: boolean, presetId: number) {
        this.socket.send('createRoom', name, presetId, custom);
        // TODO: send update game rule
        // this.socket.send("")
    }

    joinRoom(room: RoomInfo, spectateMode: boolean) {
        this.socket.send('joinRoom', room.id, spectateMode);
    }
}
