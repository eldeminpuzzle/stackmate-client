import { TestBed } from '@angular/core/testing';

import { Session } from './session';

describe('SessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Session = TestBed.get(Session);
    expect(service).toBeTruthy();
  });
});
