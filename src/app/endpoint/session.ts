import { Injectable } from '@angular/core';
import { AccountInfo } from '../shared/intf';

@Injectable({
    providedIn: 'root'
})
export class Session {

    account: AccountInfo;

    constructor() {
        
    }
}
