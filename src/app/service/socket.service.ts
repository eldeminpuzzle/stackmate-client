import { Injectable } from '@angular/core';
import { SocketWrapper } from '../shared/socketWrapper';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';

const socket = io(environment.ws);

@Injectable({
    providedIn: 'root' 
})
export class SocketService extends SocketWrapper {
    constructor(
        
    ) {
        super(socket);
    }
}
