import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class IdbManager {

	static DB_NAME = 'tetromania';

    private db: any;

	private resetEverytime = false;

	asyncInit(callback: any) {
		let request = window.indexedDB.open(IdbManager.DB_NAME, 1);
		request.onupgradeneeded = function(e) {
			let db = (e.target as any).result;
			let objectStore = db.createObjectStore(IdbManager.DB_NAME, { keyPath: 'key' });
		}
		request.onsuccess = (e: any) => {
            this.db = (e.target as any).result;
            if (this.resetEverytime) {
                console.log('RESETTING IDB');
                this.clear();
            }
            callback();
        }
    }

	clear() {
        // reset idb
        this.db.transaction([IdbManager.DB_NAME], "readwrite")
            .objectStore(IdbManager.DB_NAME)
            .clear();
	}

	get(key: string, onsuccess: any, defaultValue: any=null) {
        let request = this.db.transaction([IdbManager.DB_NAME])
            .objectStore(IdbManager.DB_NAME)
            .get(key);
        request.onerror = function (event: any) {
            console.log('Failed to load ' + key);
			console.log(event);
			onsuccess(null); // TODO: error handling
        }
        request.onsuccess = function () {
            let data = request.result == null ? defaultValue : request.result.value;
			onsuccess(data);
		}
	}

	put(key: string, value: any, onsuccess=()=>{}) {
		let transaction = this.db.transaction([IdbManager.DB_NAME], "readwrite");
        let request = transaction.objectStore(IdbManager.DB_NAME).put({key: key, value: value});
        request.onerror = function (event: any) {
            console.log('Failed to save ' + key);
            console.log(event);
		}
		transaction.oncomplete = function() {
			onsuccess();
		};
	}

}
